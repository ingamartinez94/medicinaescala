<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MiniMental extends Model
{
    protected $table = "miniMental";

    protected $fillable = ['item1','respuesta1','item2','respuesta2','item3','respuesta3',
        'item4','respuesta4','item5','respuesta5','porcentajeFinal','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
