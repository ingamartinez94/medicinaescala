<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HoehnyYahr extends Model
{
    protected $table = "hoehnyYahr";

    protected $fillable = ['observacion','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
