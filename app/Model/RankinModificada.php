<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RankinModificada extends Model
{
    protected $table = "rankinModificada";

    protected $fillable = ['observacion','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
