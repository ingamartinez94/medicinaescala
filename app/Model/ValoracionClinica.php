<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class ValoracionClinica extends Model
{
    protected $table = "valoracionClinica";

    protected $fillable = ['item','respuesta','porcentajeFinal','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
