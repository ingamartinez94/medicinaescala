<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TestMental extends Model
{
    protected $table = "testMental";

    protected $fillable = ['item1','respuesta1','item2','respuesta2','item3','respuesta3',
        'item4','respuesta4','item5','respuesta5','item6','respuesta6','item7','respuesta7',
        'item8','item9','item10','item11','item12','item13','item14','item15','item16','item17','item18',
        'item19','item20','item21','item22','item23','item24',
        'respuesta1','respuesta2','respuesta3','respuesta4','respuesta5','respuesta6','respuesta7',
        'respuesta8','respuesta9','respuesta10','respuesta11','respuesta12','respuesta13','respuesta14',
        'respuesta15','respuesta16','respuesta17','respuesta18','respuesta19','respuesta20','respuesta21',
        'respuesta22','respuesta23','respuesta24','porcentajeFinal','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion','valoracion_id');
    }
}
