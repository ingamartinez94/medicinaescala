<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Barthel extends Model
{
    protected $table = "barthel";

    protected $fillable = ['item1','respuesta1','item2','respuesta2','item3','respuesta3',
        'item4','respuesta4','item5','respuesta5','item6','respuesta6','item7','respuesta7',
        'item8','respuesta8','item9','respuesta9','item10','respuesta10','porcentajeFinal','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
