<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Cincinnati extends Model
{

    protected $table = "cincinnati";

    protected $fillable = ['item1','respuesta1','item2','respuesta2','item3','respuesta3',
        'porcentajeFinal','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
