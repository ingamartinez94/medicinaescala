<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Paciente extends Authenticatable
{
    protected $table = "paciente";

    protected $fillable = ['nombre','apellido','cedula','direccion','email','password','medicos_id'];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value, 'UTF-8');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function valoraciones()
    {
        return $this->hasMany('App\model\Valoracion');
    }

    public function medico()
    {
        return $this->belongsTo('App\model\Medico','medicos_id');
    }

    public function canadiense()
    {
        return $this->hasOne('App\model\Canadiense');
    }

    public function cincinnati()
    {
        return $this->hasOne('App\model\Cincinnati');
    }
}
