<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Updrs extends Model
{
    protected $table = "updrs";

    protected $fillable = [
        'item1','item2','item3','item4','item5','item6','item7','item8','item9','item10','item11',
        'item12','item13','item14','item15','item16','item17','item18','item19','item20','item21',
        'item22','item23','item24','item25','item26','item27','item28','item29','item30','item31',
        'item32','item33','item34','item35','item36','item37','item38','item39','item40','item41',
        'item42','item43','item44','item45','item46','item47','item48','item49','item50','item51',
        'item52','item53','item54','item55','item56',

        'respuesta_item1','respuesta_item2','respuesta_item3','respuesta_item4','respuesta_item5','respuesta_item6','respuesta_item7','respuesta_item8','respuesta_item9','respuesta_item10','respuesta_item11',
        'respuesta_item12','respuesta_item13','respuesta_item14','respuesta_item15','respuesta_item16','respuesta_item17','respuesta_item18','respuesta_item19','respuesta_item20','respuesta_item21',
        'respuesta_item22','respuesta_item23','respuesta_item24','respuesta_item25','respuesta_item26','respuesta_item27','respuesta_item28','respuesta_item29','respuesta_item30','respuesta_item31',
        'respuesta_item32','respuesta_item33','respuesta_item34','respuesta_item35','respuesta_item36','respuesta_item37','respuesta_item38','respuesta_item39','respuesta_item40','respuesta_item41',
        'respuesta_item42','respuesta_item43','respuesta_item44','respuesta_item45','respuesta_item46','respuesta_item47','respuesta_item48','respuesta_item49','respuesta_item50','respuesta_item51',
        'respuesta_item52','respuesta_item53','respuesta_item54','respuesta_item55','respuesta_item56',

        'porcentajeFinal','valoracion_id'
    ];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
