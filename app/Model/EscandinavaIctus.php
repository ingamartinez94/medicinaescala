<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EscandinavaIctus extends Model
{
    protected $table = "escandinavaictus";

    protected $fillable = ['item1','respuesta1','item2','respuesta2','item3','respuesta3',
        'item4','respuesta4','item5','respuesta5','item6','respuesta6','item7','respuesta7',
        'item8','respuesta8','porcentajeFinal','valoracion_id'];

    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
