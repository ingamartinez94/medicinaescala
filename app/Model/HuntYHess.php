<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HuntYHess extends Model
{
    protected $table = "huntyhess";

    protected $fillable = ['observacion','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
