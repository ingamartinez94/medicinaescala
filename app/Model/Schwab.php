<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Schwab extends Model
{
    protected $table = "schwab";

    protected $fillable = ['observacion','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion');
    }
}
