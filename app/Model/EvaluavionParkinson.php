<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class EvaluavionParkinson extends Model
{
    protected $table = "evalucacionparkinson";

    protected $fillable = ['item1','respuesta1','item2','respuesta2','item3','respuesta3',
        'item4','respuesta4','item5','respuesta5','item6','respuesta6','item7','respuesta7',
        'item8','item9','item10','item11','item12','item13','item14','item15','item16','item17','item18',
        'item19','item20','item21','item22','item23','item24','item25','item26','item27','item28','item29','item30',

        'gravedad1','gravedad2','gravedad3','gravedad4','gravedad5','gravedad6','gravedad7',
        'gravedad8','gravedad9','gravedad10','gravedad11','gravedad12','gravedad13','gravedad14',
        'gravedad15','gravedad16','gravedad17','gravedad18','gravedad19','gravedad20','gravedad21',
        'gravedad22','gravedad23','gravedad24','gravedad25','gravedad26','gravedad27','gravedad28',
        'gravedad29','gravedad30',

        'frecuencia1','frecuencia2','frecuencia3','frecuencia4','frecuencia5','frecuencia6','frecuencia7',
        'frecuencia8','frecuencia9','frecuencia10','frecuencia11','frecuencia12','frecuencia13','frecuencia14',
        'frecuencia15','frecuencia16','frecuencia17','frecuencia18','frecuencia19','frecuencia20','frecuencia21',
        'frecuencia22','frecuencia23','frecuencia24','frecuencia25','frecuencia26','frecuencia27','frecuencia28',
        'frecuencia29','frecuencia30',

        'porcentajeFinal','valoracion_id'];


    public function valoracion()
    {
        return $this->belongsTo('App\model\Valoracion','valoracion_id');
    }
}
