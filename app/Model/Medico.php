<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Medico extends Authenticatable
{
    protected $table = "medicos";

    protected $fillable = ['nombre','apellido','especialidad','email','password'];


    protected $hidden = ['password', 'remember_token'];


    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value, 'UTF-8');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }


    public function valoraciones()
    {
        return $this->hasMany('App\model\Valoracion');
    }

    public function pacientes()
    {
        return $this->hasMany('App\model\Paciente');
    }

}
