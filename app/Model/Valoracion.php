<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Valoracion extends Model
{
    protected $table = "valoracion";

    protected $fillable = ['fecha','paciente_id','medicos_id','escala'];


    public function medico()
    {
        return $this->belongsTo('App\model\Medico');
    }

    public function paciente()
    {
        return $this->belongsTo('App\model\Paciente');
    }

    public function canadiense()
    {
        return $this->hasOne('App\model\Canadiense');
    }
    public function cincinnati()
    {
        return $this->hasOne('App\model\Cincinnati');
    }
    public function escandinavaIctus()
    {
        return $this->hasOne('App\model\EscandinavaIctus');
    }

    public function mentalAbreviado()
    {
        return $this->hasOne('App\model\MentalAbreviado');
    }
}
