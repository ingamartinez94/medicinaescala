<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('log');
});

Route::resource('medico','MedicoController');
Route::resource('paciente','PacienteController');
Route::resource('admin','AdminController');

/*escalas*/
Route::resource('cincinnati','CincinnatiController');
Route::post('cincinnati/{pacienteId}', 'CincinnatiController@store');

Route::resource('neuro-canadiense','CanadienseController');
Route::post('neuro-canadiense/{pacienteId}', 'CanadienseController@store');

Route::resource('escandinava-ictus','EscandinavaIctusController');
Route::post('escandinava-ictus/{pacienteId}', 'EscandinavaIctusController@store');

Route::resource('indice-barthel','IndiceBarthelController');
Route::post('indice-barthel/{pacienteId}', 'IndiceBarthelController@store');

Route::resource('rankin','RankinController');
Route::post('rankin/{pacienteId}', 'RankinController@store');

Route::resource('hunt-hess','HuntHessController');
Route::post('hunt-hess/{pacienteId}', 'HuntHessController@store');

Route::resource('mini-mental','MiniMentalController');
Route::post('mini-mental/{pacienteId}', 'MiniMentalController@store');

Route::resource('test-mental','TestMentalController');
Route::post('test-mental/{pacienteId}', 'TestMentalController@store');

Route::resource('test-mental-abreviado','TestMentalAbreviadoController');
Route::post('test-mental-abreviado/{pacienteId}', 'TestMentalAbreviadoController@store');

Route::resource('hoehn-yahr','HoehnYahrController');
Route::post('hoehn-yahr/{pacienteId}', 'HoehnYahrController@store');

Route::resource('schwab','SchwabController');
Route::post('schwab/{pacienteId}', 'SchwabController@store');

Route::resource('hachinski','HachinskiController');
Route::post('hachinski/{pacienteId}', 'HachinskiController@store');

Route::resource('nihss','NIHSSController');
Route::post('nihss/{pacienteId}', 'NIHSSController@store');

Route::resource('valoracion-clinica','ValoracionClinicaController');
Route::post('valoracion-clinica/{pacienteId}', 'ValoracionClinicaController@store');

Route::resource('updrs','UPDRSController');
Route::post('updrs/{pacienteId}', 'UPDRSController@store');

Route::resource('valoracion-parkinson','EvaluavionParkinsonController');
Route::post('valoracion-parkinson/{pacienteId}', 'EvaluavionParkinsonController@store');


Route::get('paciente/{pacienteId}/listado', 'MedicoListadoController@index');

Route::get('graficos/{pacienteId}', 'MedicoListadoController@grafico');

Route::resource('log','LogController');
Route::get('logout','LogController@logout');

//Graficos de Escalas
Route::group(['prefix' => 'grafico'], function () {
    Route::get('mental-abreviado/{paciente_id?}', [
        'as'   => 'grafico.mental-abreviado',
        'uses' => 'GraficoEscala@mentalAbreviadoIndex'
    ]);
    Route::get('test-mental/{paciente_id?}', [
        'as'   => 'grafico.test-mental',
        'uses' => 'GraficoEscala@testMentalIndex'
    ]);

    Route::get('mini-mental/{paciente_id?}', [
        'as'   => 'grafico.mini-mental',
        'uses' => 'GraficoEscala@miniMentalIndex'
    ]);

    Route::get('barthel/{paciente_id?}', [
        'as'   => 'grafico.barthel',
        'uses' => 'GraficoEscala@barthelIndex'
    ]);

    Route::get('escandinava/{paciente_id?}', [
        'as'   => 'grafico.escandinava',
        'uses' => 'GraficoEscala@escandinavaIctusIndex'
    ]);

    Route::get('cincinnati/{paciente_id?}', [
        'as'   => 'grafico.cincinnati',
        'uses' => 'GraficoEscala@cincinnatiIndex'
    ]);

    Route::get('hunt-hess/{paciente_id?}', [
        'as'   => 'grafico.hunt-hess',
        'uses' => 'GraficoEscala@huntHessIndex'
    ]);

    Route::get('hoehn-yahr/{paciente_id?}', [
        'as'   => 'grafico.hoehn-yahr',
        'uses' => 'GraficoEscala@hoehnYahrIndex'
    ]);

    Route::get('rankin-modificado/{paciente_id?}', [
        'as'   => 'grafico.rankin-modificado',
        'uses' => 'GraficoEscala@rankinIndex'
    ]);

    Route::get('schwab/{paciente_id?}', [
        'as'   => 'grafico.schwab',
        'uses' => 'GraficoEscala@schwabIndex'
    ]);

    Route::get('hachinski/{paciente_id?}', [
        'as'   => 'grafico.hachinski',
        'uses' => 'GraficoEscala@hachinskiIndex'
    ]);

    Route::get('nihss/{paciente_id?}', [
        'as'   => 'grafico.nihss',
        'uses' => 'GraficoEscala@nihssIndex'
    ]);

    Route::get('valoracion/{paciente_id?}', [
        'as'   => 'grafico.valoracion',
        'uses' => 'GraficoEscala@valoracionClinicaIndex'
    ]);

    Route::get('canadiense/{paciente_id?}', [
        'as'   => 'grafico.canadiense',
        'uses' => 'GraficoEscala@canadienseIndex'
    ]);

    Route::get('evaluacion-parkinson/{paciente_id?}', [
        'as'   => 'grafico.evaluacion-parkinson',
        'uses' => 'GraficoEscala@evalucacionparkinsonIndex'
    ]);
    Route::get('updrs/{paciente_id?}', [
        'as'   => 'grafico.updrs',
        'uses' => 'GraficoEscala@updrsIndex'
    ]);

});

Route::get('crear-registros', function () {
    $admin= new \App\Model\Admin();
    $admin->nombre='admin_nombre';
    $admin->apellido='admin_apellido';
    $admin->email='admin@admin.com';
    $admin->password='1234';
    $admin->save();

    $medico= new \App\Model\Medico();
    $medico->nombre='medico_nombre';
    $medico->apellido='medico_apellido';
    $medico->especialidad='internista';
    $medico->email='medico@medico.com';
    $medico->password='1234';
    $medico->save();

    $paciente= new \App\Model\Paciente();
    $paciente->nombre='paciente_nombre';
    $paciente->apellido='paciente_apellido';
    $paciente->cedula='123456789';
    $paciente->direccion='paciente_direccion';
    $paciente->email='paciente@paciente.com';
    $paciente->password='1234';
    $paciente->medicos_id=1;
    $paciente->save();

});