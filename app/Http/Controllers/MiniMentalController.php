<?php

namespace App\Http\Controllers;

use App\Model\MiniMental;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\model\Valoracion;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class MiniMentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

        //59-46-33-23-20
        $date = Carbon::now();
        $date = $date->format('d-m-Y');

        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"mini-mental",
        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;


        $valor1=$request['11']+$request['12']+$request['13']+$request['14']+$request['15']+
                $request['16']+$request['17']+$request['18']+$request['19']+$request['20'];

        $valor2=$request['21']+$request['22']+$request['23'];

        $valor3=$request['31']+$request['32']+$request['33'];

        if ($request['46']==5){
            $valor4 = 5;

        }else{
            $valor4=$request['41']+$request['42']+$request['43']+$request['44']+$request['45'];

        }


        $valor5=$request['11']+$request['12']+$request['13']+$request['14']+$request['15']+
            $request['16']+$request['17']+$request['18']+$request['19'];

        $valorFormulario = $valor1+$valor2+$valor3+$valor4+$valor5;
            
       // dd("pregunta 1 ".$valor1." Registro= ".$valor2." Recuerdo ".$valor3." Atención y Cálculo ".$valor4." Lenguaje ".$valor5);
   
        MiniMental::create([
            'item1'=>"Orientacion",
            'respuesta1' => $valor1,
            'item2'=>"Registro",
            'respuesta2'=>$valor2,
            'item3'=>"Recuerdo",
            'respuesta3'=>$valor3,
            'item4'=>"Atención y Cálculo",
            'respuesta4'=>$valor4,
            'item5'=>"Lenguaje",
            'respuesta5'=>$valor5,
            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' => $idValoracion,
            
        ]);

        return redirect()->route('grafico.mini-mental',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
