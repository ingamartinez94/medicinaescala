<?php

namespace App\Http\Controllers;

use App\Model\Hachinski;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class HachinskiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');

        $valorFormulario = 0.0;

        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"hachinski",

        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;


        //validacion de las preguntas segun la escala
       $valorFormulario = $request['opcion_brusco']+$request['opcion_deterioro']+$request['opcion_fluctuante']+
        $request['opcion_desorientacion']+$request['opcion_preservación']+$request['opcion_depresion']+
    $request['opcion_somatización']+$request['opcion_labilidad']+$request['opcion_historia']+
           $request['opcion_historia_ictus']+$request['opcion_evidencia']+$request['opcion_sintomas']+
           $request['opcion_signos'];


        //Registrar Los resultados de la escala
        Hachinski::create([
            'item1'=>"Comienzo brusco",
            'respuesta1'=>$request['opcion_brusco'],
            'item2'=>"Deterioro escalonado",
            'respuesta2'=>$request['opcion_deterioro'],
            'item3'=>"Curso fluctuante.",
            'respuesta3'=>$request['opcion_fluctuante'],
            'item4'=>"Desorientación nocturna.",
            'respuesta4'=>$request['opcion_desorientacion'],
            'item5'=>"Preservación relativa de la personalidad.",
            'respuesta5'=>$request['opcion_preservación'],
            'item6'=>"Depresión.",
            'respuesta6'=>$request['opcion_depresion'],
            'item7'=>"Somatización.",
            'respuesta7'=>$request['opcion_somatización'],
            'item8'=>"Labilidad emocional.",
            'respuesta8'=>$request['opcion_labilidad'],
            'item9'=>"Historia de hipertensión arterial.",
            'respuesta9'=>$request['opcion_historia'],
            'item10'=>"Historia de ictus previos.",
            'respuesta10'=>$request['opcion_historia_ictus'],
            'item11'=>"Evidencia de arteriosclerosis asociada.",
            'respuesta11'=>$request['opcion_evidencia'],
            'item12'=>"Síntomas neurológicos focales.",
            'respuesta12'=>$request['opcion_sintomas'],
            'item13'=>"Signos neurológicos focales.",
            'respuesta13'=>$request['opcion_signos'],
            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,

        ]);

        return redirect()->route('grafico.hachinski',$id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
