<?php

namespace App\Http\Controllers;

use App\model\Canadiense;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class CanadienseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {

        $date = Carbon::now();
        $date = $date->format('d-m-Y');


        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"canadiense",

        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        
        $valorFormulario = $request['opcion_nivel_conciencia'] + $request['opcion_orientacion']+$request['opcion_habla']+
            $request['opcion_fun_motora_sin_cara'] +$request['opcion_fun_motora_sin_extremidad_superior']+
            $request['opcion_fun_motora_sin_extremidad_superior_distal'] +
            $request['opcion_fun_motora_sin_extremidad_inferior'] +$request['opcion_fun_motora_con_cara']+
            $request['opcion_fun_motora_con_extremidad_superior']+$request['opcion_fun_motora_con_extremidad_inferior'];

//        dd($valorFormulario);

        Canadiense::create([
            'item1'=>"Nivel de conciencia",
            'item2'=>"Orientación",
            'item3'=>"Habla",
            'item4'=>"Funciones motoras (sin problemas en la comprensión verbal)",
            'item5'=>"Extremidad superior porción proximal.",
            'item6'=>"Extremidad superior porción distal.",
            'item7'=>"Extremidad inferior.",
            'item8'=>"Funciones motoras(sin problemas en la comprensión verbal)",
            'item9'=>"Extremidades superiores.",
            'item10'=>"Extremidades inferiores.",

            'respuesta2'=>$request['opcion_nivel_conciencia'],
            'respuesta1'=>$request['opcion_orientacion'],
            'respuesta3'=>$request['opcion_habla'],
            'respuesta4'=>$request['opcion_fun_motora_sin_cara'],
            'respuesta5'=>$request['opcion_fun_motora_sin_extremidad_superior'],
            'respuesta6'=>$request['opcion_fun_motora_sin_extremidad_superior_distal'],
            'respuesta7'=>$request['opcion_fun_motora_sin_extremidad_inferior'],
            'respuesta8'=>$request['opcion_fun_motora_con_cara'],
            'respuesta9'=>$request['opcion_fun_motora_con_extremidad_superior'],
            'respuesta10'=>$request['opcion_fun_motora_con_extremidad_inferior'],


            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,


        ]);

        return redirect()->route('grafico.canadiense',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
