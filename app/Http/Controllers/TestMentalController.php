<?php

namespace App\Http\Controllers;


use App\Model\TestMental;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class TestMentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');



        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"test-mental",
        ]);

        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        $valor5=$request['opcion_nombre_direccion1']+$request['opcion_nombre_direccion2']+$request['opcion_nombre_direccion3'];

        $valor10=$request['check_hospital']+$request['check_nombre_hospital']+$request['check_sala']+$request['check_pueblo'];

        $valorFormulario = $request['check_nombre']+$request['check_edad']+$request['check_hora']+
            $request['check_tiempo']+$valor5+$request['check_dia']+$request['check_fecha_mes']+
            $request['check_mes']+$request['check_anio']+$valor10+$request['opcion_doctor_enfermera']+
            $request['check_fecha_nacimiento']+$request['check_lugar_nacimiento']+
            $request['check_colegio']+$request['check_asistir']+$request['check_ocupacion']+
            $request['check_esposa']+$request['check_guerra_p']+$request['check_guerra_s']+
            $request['check_monarca']+$request['check_ministro']+$request['check_meses_anio']+
            $request['opcion_contar']+$request['opcion_contar_rever'];

        TestMental::create([
            'item1'=>"Nombre",
            'item2'=>"Edad",
            'item3'=>"Hora",
            'item4'=>"Tiempo del dia",
            'item5'=>"Nombre y direccion",
            'item6'=>"Día de la semana",
            'item7'=>"Fecha (correcto día del mes)",
            'item8'=>"Mes",
            'item9'=>"Año",
            'item10'=>"Lugar donde se encuentra",
            'item11'=>"Reconocimiento de 2 personas",
            'item12'=>"Fecha de nacimiento (día y mes)",
            'item13'=>"Lugar de nacimiento",
            'item14'=>"Colegio al que asistió",
            'item15'=>"Asistió a la escuela",
            'item16'=>"Ocupación formal",
            'item17'=>"Nombre de la esposa, hermano o pariente cercano",
            'item18'=>"Dato de la Primera Guerra Mundial (año)",
            'item19'=>"Dato de la Segunda Guerra Mundial (año)",
            'item20'=>"Nombre del presente Monarca",
            'item21'=>"Nombre del Primer Ministro",
            'item22'=>"Meses del año dichos hacia atrás",
            'item23'=>"Contar del 1 al 20 (1 ó 2)",
            'item24'=>"Contar del 20 al 1 (1 ó 2)",

            'respuesta1'=>$request['check_nombre'],
            'respuesta2'=>$request['check_edad'],
            'respuesta3'=>$request['check_hora'],
            'respuesta4'=>$request['check_tiempo'],
            'respuesta5'=>$valor5,
            'respuesta6'=>$request['check_dia'],
            'respuesta7'=>$request['check_fecha_mes'],
            'respuesta8'=>$request['check_mes'],
            'respuesta9'=>$request['check_anio'],
            'respuesta10'=>$valor10,
            'respuesta11'=>$request['opcion_doctor_enfermera'],
            'respuesta12'=>$request['check_fecha_nacimiento'],
            'respuesta13'=>$request['check_lugar_nacimiento'],
            'respuesta14'=>$request['check_colegio'],
            'respuesta15'=>$request['check_asistir'],
            'respuesta16'=>$request['check_ocupacion'],
            'respuesta17'=>$request['check_esposa'],
            'respuesta18'=>$request['check_guerra_p'],
            'respuesta19'=>$request['check_guerra_s'],
            'respuesta20'=>$request['check_monarca'],
            'respuesta21'=>$request['check_ministro'],
            'respuesta22'=>$request['check_meses_anio'],
            'respuesta23'=>$request['opcion_contar'],
            'respuesta24'=>$request['opcion_contar_rever'],

            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,
        ]);

        return redirect()->route('grafico.test-mental',$id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
