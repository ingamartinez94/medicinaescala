<?php
/**
 * Created by PhpStorm.
 * User: darkm
 * Date: 1/05/2016
 * Time: 2:18 AM
 */

namespace App\Http\Controllers\Traits;

use App\Model\Admin;
use App\model\Valoracion;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;


trait TraitMiniMental
{
    //minimeental
    public function miniMentalIndex($paciente_id=null)
    {
        $paciente_id=$this->checkPaciente($paciente_id);
        return $this->getGraficoMiniMental($paciente_id);

    }

    public function getGraficoMiniMental($paciente_id)
    {
        $escala="mini-mental";
        $dataEscalas = DB::table('valoracion')
            ->join('minimental', 'valoracion.id', '=', 'minimental.valoracion_id')
            ->select('minimental.*')
            ->where('valoracion.paciente_id','=',$paciente_id)
            ->get();

        //Estabas retornando todas las fechas que tenia el paciente
        $valoraciones=Valoracion::select('valoracion.fecha')
            ->join('minimental', 'valoracion.id', '=', 'minimental.valoracion_id')
            ->where('paciente_id',$paciente_id)->get();


        if ($valoraciones->isEmpty()){

            $mes_formateado=0;
            $porcentajeFinal_Formateado=0;

            foreach ($dataEscalas as $dataEscala){
                $porcentajeFinal_Formateado[]=$dataEscala->porcentajeFinal;
            }
            
            $tipos = Valoracion::where('paciente_id', $paciente_id)
                ->groupBy('escala')
                ->get();

            return view('graficos.index',compact('dataEscalas','tipos'))
                ->with('mes_formateado', $mes_formateado)
                ->with('porcentajeFinal_Formateado', $porcentajeFinal_Formateado)
                ->with('escala', $escala)
                ->with('id', $paciente_id)
                ;

        }else{

            foreach ($valoraciones as $valoracion){
                $mes = Date::parse($valoracion->fecha);
                $mes_formateado[] = $mes->format('j F');
            }

            foreach ($dataEscalas as $dataEscala){
                $porcentajeFinal_Formateado[]=$dataEscala->porcentajeFinal;
            }

            $tipos = Valoracion::where('paciente_id', $paciente_id)
                ->groupBy('escala')
                ->get();

            return view('graficos.index',compact('dataEscalas','tipos'))
                ->with('mes_formateado', $mes_formateado)
                ->with('porcentajeFinal_Formateado', $porcentajeFinal_Formateado)
                ->with('escala', $escala)
                ->with('id', $paciente_id)
                ;
        }
    }
}