<?php
/**
 * Created by PhpStorm.
 * User: darkm
 * Date: 1/05/2016
 * Time: 2:18 AM
 */

namespace App\Http\Controllers\Traits;

use App\Model\Admin;
use App\model\Valoracion;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;


trait TraitBarthel
{
    //barthel
    public function barthelIndex($paciente_id=null)
    {
        $paciente_id=$this->checkPaciente($paciente_id);
        return $this->getGraficoBarthel($paciente_id);

    }

    public function getGraficoBarthel($paciente_id)
    {
        $escala="barthel";
        $dataEscalas = DB::table('valoracion')
            ->join('barthel', 'valoracion.id', '=', 'barthel.valoracion_id')
            ->select('barthel.*')
            ->where('valoracion.paciente_id','=',$paciente_id)
            ->get();

        //Estabas retornando todas las fechas que tenia el paciente
        $valoraciones=Valoracion::select('valoracion.fecha')
            ->join('barthel', 'valoracion.id', '=', 'barthel.valoracion_id')
            ->where('paciente_id',$paciente_id)->get();


        if ($valoraciones->isEmpty()){

            $mes_formateado=0;
            $porcentajeFinal_Formateado=0;

            foreach ($dataEscalas as $dataEscala){
                $porcentajeFinal_Formateado[]=$dataEscala->porcentajeFinal;
            }

            $tipos = Valoracion::where('paciente_id', $paciente_id)
                ->groupBy('escala')
                ->get();

            return view('graficos.index',compact('dataEscalas','tipos'))
                ->with('mes_formateado', $mes_formateado)
                ->with('porcentajeFinal_Formateado', $porcentajeFinal_Formateado)
                ->with('escala', $escala)
                ->with('id', $paciente_id)
                ;

        }else{

            foreach ($valoraciones as $valoracion){
                $mes = Date::parse($valoracion->fecha);
                $mes_formateado[] = $mes->format('j F');
            }

            foreach ($dataEscalas as $dataEscala){
                $porcentajeFinal_Formateado[]=$dataEscala->porcentajeFinal;
            }


            $tipos = Valoracion::where('paciente_id', $paciente_id)
                ->groupBy('escala')
                ->get();

            return view('graficos.index',compact('dataEscalas','tipos'))
                ->with('mes_formateado', $mes_formateado)
                ->with('porcentajeFinal_Formateado', $porcentajeFinal_Formateado)
                ->with('escala', $escala)
                ->with('id', $paciente_id)
                ->with('id', $paciente_id)
                ;
        }
    }
}