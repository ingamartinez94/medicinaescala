<?php

namespace App\Http\Controllers\Traits;

use App\Model\Admin;
use App\model\Valoracion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;


trait TraitSchwab
{
    //cincinnati
    public function schwabIndex($paciente_id=null)
    {
        $paciente_id=$this->checkPaciente($paciente_id);
        return $this->getGragicoSchwab($paciente_id);

    }

    public function getGragicoSchwab($paciente_id)
    {
        $escala="schwab";
        
        $dataEscalas = DB::table('valoracion')
            ->join('schwab', 'valoracion.id', '=', 'schwab.valoracion_id')
            ->select('schwab.*')
            ->where('valoracion.paciente_id','=',$paciente_id)
            ->get();

        //Estabas retornando todas las fechas que tenia el paciente
        $valoraciones=Valoracion::select('valoracion.fecha')
            ->join('schwab', 'valoracion.id', '=', 'schwab.valoracion_id')
            ->where('paciente_id',$paciente_id)->get();


        if ($valoraciones->isEmpty()){

            $mes_formateado=0;
            $porcentajeFinal_Formateado=0;

            foreach ($dataEscalas as $dataEscala){
                $porcentajeFinal_Formateado[]=$dataEscala->porcentajeFinal;
            }

            $tipos = Valoracion::where('paciente_id', $paciente_id)
                ->groupBy('escala')
                ->get();

            return view('graficos.index',compact('dataEscalas','tipos'))
                ->with('mes_formateado', $mes_formateado)
                ->with('porcentajeFinal_Formateado', $porcentajeFinal_Formateado)
                ->with('escala', $escala)
                ->with('id', $paciente_id)
                ;

        }else{

            foreach ($valoraciones as $valoracion){
                $mes = Date::parse($valoracion->fecha);
                $mes_formateado[] = $mes->format('j F');
            }

            $porcentajeFinal_Formateado[]=0;

            $tipos = Valoracion::where('paciente_id', $paciente_id)
                ->groupBy('escala')
                ->get();

            return view('graficos.index',compact('dataEscalas','tipos'))
                ->with('mes_formateado', $mes_formateado)
                ->with('porcentajeFinal_Formateado', $porcentajeFinal_Formateado)
                ->with('escala', $escala)
                ->with('id', $paciente_id)
                ;
        }
    }
}