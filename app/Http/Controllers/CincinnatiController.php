<?php

namespace App\Http\Controllers;

use App\model\Cincinnati;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class CincinnatiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');

        $valorFormulario = 0.0;

        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"cincinnati",

        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;


        //validacion de las preguntas segun la escala
        if ($request['opcion_facial'] == 'anormal' || $request['opcion_brazos'] == 'anormal' ||
                                            $request['opcion_lenguaje']== 'anormal'){
            
            $valorFormulario = 10.0;

        }

        //Registrar Los resultados de la escala
        Cincinnati::create([
            'item1'=>"Asimetría Facial",
            'item2'=>"Fuerza en los brazos",
            'item3'=>"Lenguaje",
            'respuesta1' =>$request['opcion_facial'],
            'respuesta2' =>$request['opcion_brazos'],
            'respuesta3' => $request['opcion_lenguaje'],
            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,
        ]);

        return redirect()->route('grafico.cincinnati',$id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
