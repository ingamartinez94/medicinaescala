<?php

namespace App\Http\Controllers;

use App\Model\Paciente;
use App\model\Valoracion;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Response;

class PacienteController extends Controller
{
    public function __construct()
    {
        if (Auth::guard('web_medico')->check()) {
            $this->middleware('auth-medico',['except' => ['index']]);
        }else{
            $this->middleware('auth-paciente');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::guard('web_paciente')->user()->id;
        $valoraciones = Valoracion::where('paciente_id', $id)
            ->groupBy('escala')
            ->get();

        return view('paciente.index', compact('valoraciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Paciente::create([

            'nombre' => $request['nombrePaciente'],
            'apellido' => $request['apellidoPaciente'],
            'cedula' => $request['cedula'],
            'direccion' => $request['direccion'],
            'email' => $request['emailPaciente'],
            'password' => $request['passworoPaciente'],
            'medicos_id' => Auth::guard('web_medico')->user()->id,
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::findOrFail($id);

        return Response::json($paciente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Auth::guard('web_paciente')->user()->id;
        $valoraciones = Valoracion::where('paciente_id', $id)
            ->groupBy('escala')
            ->get();

        $loginId = Auth::guard('web_paciente')->user()->id;
        if ($loginId == $id){
            $objPaciente = Paciente::findOrFail($id);

            return view('paciente.edit',compact('objPaciente','valoraciones'));

        }else{
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paciente = Paciente::findOrFail($id);
        $idMedico= $paciente->medico;

        if($request->ajax()){
            $paciente->nombre =$request['nombre'];
            $paciente->apellido = $request['apellido'];
            $paciente->cedula = $request['cedula'];
            $paciente->direccion = $request['direccion'];
            $paciente->email = $request['correo'];
            $paciente->medicos_id = $idMedico->id;

            $paciente->save();
        }else{
//            $paciente->nombre =$request['nombre'];
//            $paciente->apellido = $request['apellido'];
//            $paciente->cedula = $request['cedula'];
            $paciente->direccion = $request['direccion'];
            $paciente->email = $request['correo'];
            $paciente->password = $request['contra'];
            $paciente->medicos_id = $idMedico->id;

            $paciente->save();

            return back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Paciente::destroy($id);
    }
}
