<?php

namespace App\Http\Controllers;

use App\model\EvaluavionParkinson;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class EvaluavionParkinsonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');



        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"evaluacion-parkinson",

        ]);

        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;


        $valorGravedad =$request['opcion_gravedad_p1']+$request['opcion_gravedad_p2']+
            $request['opcion_gravedad_p3']+$request['opcion_gravedad_p4']+$request['opcion_gravedad_p5']+
            $request['opcion_gravedad_p6']+$request['opcion_gravedad_p7']+$request['opcion_gravedad_p8']+
            $request['opcion_gravedad_p9']+$request['opcion_gravedad_p10']+$request['opcion_gravedad_p11']+
            $request['opcion_gravedad_p12']+$request['opcion_gravedad_p13']+$request['opcion_gravedad_p14']+
            $request['opcion_gravedad_p15']+$request['opcion_gravedad_p16']+$request['opcion_gravedad_p17']+
            $request['opcion_gravedad_p18']+$request['opcion_gravedad_p19']+$request['opcion_gravedad_p20']+
            $request['opcion_gravedad_p21']+$request['opcion_gravedad_p22']+$request['opcion_gravedad_p23']+
            $request['opcion_gravedad_p24']+$request['opcion_gravedad_p25']+$request['opcion_gravedad_p26']+
            $request['opcion_gravedad_p27']+$request['opcion_gravedad_p28']+$request['opcion_gravedad_p29']+
           $request['opcion_gravedad_p30'];

        $valorFrecuencia = $request['frecuencia1']+$request['frecuencia2']+
            $request['frecuencia3']+$request['frecuencia4']+$request['frecuencia5']+
            $request['frecuencia6']+$request['frecuencia7']+$request['frecuencia8']+
            $request['frecuencia9']+$request['frecuencia10']+$request['frecuencia11']+
            $request['frecuencia12']+$request['frecuencia13']+$request['frecuencia14']+
            $request['frecuencia15']+$request['frecuencia16']+$request['frecuencia17']+
            $request['frecuencia18']+$request['frecuencia19']+$request['frecuencia20']+
            $request['frecuencia21']+$request['frecuencia22']+
            $request['frecuencia24']+$request['frecuencia25']+$request['frecuencia26']+
            $request['frecuencia27']+$request['frecuencia28']+$request['frecuencia29']+
            $request['frecuencia30'];
;

        $valorFormulario = $valorGravedad + $valorFrecuencia;

        EvaluavionParkinson::create([
            'item1'=>"Mareo,aturdimiento",
            'item2'=>"Caer por desmayo",
            'item3'=>"Adormiado o dormido",
            'item4'=>"Limitacion por fatiga",
            'item5'=>"Dificultad para dormir",
            'item6'=>"Inquietud de piernas mientras se esta sentado",
            'item7'=>"perdida de interes",
            'item8'=>"Falta de motivacion para nuevas actividades",
            'item9'=>"Nervioso o preocupado sin razon",
            'item10'=>"Tristeza o deprecion",
            'item11'=>"Estado de animo plano, sin altibajos",
            'item12'=>"Dificultad para sentir placer con sus actividades",
            'item13'=>"Ve cosas que no estan",
            'item14'=>"Cree cosas que no son verdad(robo,infidelidad)",
            'item15'=>"Ve doble",
            'item16'=>"Problemas para mantener la concentracion",
            'item17'=>"Olvidar cosas recientes",
            'item18'=>"Olvidar hacer cosas(tomar pastillas, tareas diarias)",
            'item19'=>"Babea durante el dia",
            'item20'=>"Dificultad para tragar",
            'item21'=>"Sufre de estreñimiento",
            'item22'=>"Dificultad para retener orina",
            'item23'=>"Frecuencia con la que orina 2h despues de la ultima vez",
            'item24'=>"Levantarse a orinar en la noche",
            'item25'=>"Interes alterado por el sexo",
            'item26'=>"Problemas para tener sexo",
            'item27'=>"Dolor  inexplicable( por medicamentos)",
            'item28'=>"Cambio para percibir olores y sabores",
            'item29'=>"Cambio de peso(sin dieta)",
            'item30'=>"Sudor excesivo",

            'gravedad1'=>$request['opcion_gravedad_p1'],
            'gravedad2'=>$request['opcion_gravedad_p2'],
            'gravedad3'=>$request['opcion_gravedad_p3'],
            'gravedad4'=>$request['opcion_gravedad_p4'],
            'gravedad5'=>$request['opcion_gravedad_p5'],
            'gravedad6'=>$request['opcion_gravedad_p6'],
            'gravedad7'=>$request['opcion_gravedad_p7'],
            'gravedad8'=>$request['opcion_gravedad_p8'],
            'gravedad9'=>$request['opcion_gravedad_p9'],
            'gravedad10'=>$request['opcion_gravedad_p10'],
            'gravedad11'=>$request['opcion_gravedad_p11'],
            'gravedad12'=>$request['opcion_gravedad_p12'],
            'gravedad13'=>$request['opcion_gravedad_p13'],
            'gravedad14'=>$request['opcion_gravedad_p14'],
            'gravedad15'=>$request['opcion_gravedad_p15'],
            'gravedad16'=>$request['opcion_gravedad_p16'],
            'gravedad17'=>$request['opcion_gravedad_p17'],
            'gravedad18'=>$request['opcion_gravedad_p18'],
            'gravedad19'=>$request['opcion_gravedad_p19'],
            'gravedad20'=>$request['opcion_gravedad_p20'],
            'gravedad21'=>$request['opcion_gravedad_p21'],
            'gravedad22'=>$request['opcion_gravedad_p22'],
            'gravedad23'=>$request['opcion_gravedad_p23'],
            'gravedad24'=>$request['opcion_gravedad_p24'],
            'gravedad25'=>$request['opcion_gravedad_p25'],
            'gravedad26'=>$request['opcion_gravedad_p26'],
            'gravedad27'=>$request['opcion_gravedad_p27'],
            'gravedad28'=>$request['opcion_gravedad_p28'],
            'gravedad29'=>$request['opcion_gravedad_p29'],
            'gravedad30'=>$request['opcion_gravedad_p30'],

            'frecuencia1'=>$request['frecuencia1'],
            'frecuencia2'=>$request['frecuencia2'],
            'frecuencia3'=>$request['frecuencia3'],
            'frecuencia4'=>$request['frecuencia4'],
            'frecuencia5'=>$request['frecuencia5'],
            'frecuencia6'=>$request['frecuencia6'],
            'frecuencia7'=>$request['frecuencia7'],
            'frecuencia8'=>$request['frecuencia8'],
            'frecuencia9'=>$request['frecuencia9'],
            'frecuencia10'=>$request['frecuencia10'],
            'frecuencia11'=>$request['frecuencia11'],
            'frecuencia12'=>$request['frecuencia12'],
            'frecuencia13'=>$request['frecuencia13'],
            'frecuencia14'=>$request['frecuencia14'],
            'frecuencia15'=>$request['frecuencia15'],
            'frecuencia16'=>$request['frecuencia16'],
            'frecuencia17'=>$request['frecuencia17'],
            'frecuencia18'=>$request['frecuencia18'],
            'frecuencia19'=>$request['frecuencia19'],
            'frecuencia20'=>$request['frecuencia20'],
            'frecuencia21'=>$request['frecuencia21'],
            'frecuencia22'=>$request['frecuencia22'],
            'frecuencia23'=>0,
            'frecuencia24'=>$request['frecuencia24'],
            'frecuencia25'=>$request['frecuencia25'],
            'frecuencia26'=>$request['frecuencia26'],
            'frecuencia27'=>$request['frecuencia27'],
            'frecuencia28'=>$request['frecuencia28'],
            'frecuencia29'=>$request['frecuencia29'],
            'frecuencia30'=>$request['frecuencia30'],

            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,
        ]);

        return redirect()->route('grafico.evaluacion-parkinson',$id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
