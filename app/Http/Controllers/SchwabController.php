<?php

namespace App\Http\Controllers;

use App\Model\Schwab;
use Illuminate\Http\Request;
use App\model\Valoracion;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class SchwabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');

        $valorFormulario = 0.0;

        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"schwab",
        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        Schwab::create([

            'observacion' =>$request['observaciones'],
            'valoracion_id' =>$idValoracion,

        ]);

        return redirect()->route('grafico.mental-abreviado',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
