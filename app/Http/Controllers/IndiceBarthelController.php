<?php

namespace App\Http\Controllers;

use App\Model\Barthel;
use Illuminate\Http\Request;
use App\model\Valoracion;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class IndiceBarthelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');

        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"barthel",
        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        $valorFormulario=$request['opcion_comer']+$request['opcion_lavarse']+$request['opcion_vestirse']+
            $request['opcion_arreglarse']+$request['opcion_deposicion']+$request['opcion_miccion']+
            $request['opcion_retrete']+$request['opcion_trasladarse']+$request['opcion_deambular']+
            $request['opcion_escaleras'];

        Barthel::create([
            'item1'=>"Comer",
            'respuesta1'=>$request['opcion_comer'],
            'item2'=>"Lavarse",
            'respuesta2'=>$request['opcion_lavarse'],
            'item3'=>"Vestirse",
            'respuesta3'=>$request['opcion_vestirse'],
            'item4'=>"Arreglarse",
            'respuesta4'=>$request['opcion_arreglarse'],
            'item5'=>"Deposición ",
            'respuesta5'=>$request['opcion_deposicion'],
            'item6'=>"Micción ",
            'respuesta6'=>$request['opcion_miccion'],
            'item7'=>"Usar el retrete",
            'respuesta7'=>$request['opcion_retrete'],
            'item8'=>"Trasladarse ",
            'respuesta8'=>$request['opcion_trasladarse'],
            'item9'=>"Deambular",
            'respuesta9'=>$request['opcion_deambular'],
            'item10'=>"Subir escaleras",
            'respuesta10'=>$request['opcion_escaleras'],
            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,

        ]);

        return redirect()->route('grafico.barthel',$id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
