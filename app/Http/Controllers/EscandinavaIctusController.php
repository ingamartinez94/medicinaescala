<?php

namespace App\Http\Controllers;

use App\Model\EscandinavaIctus;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class EscandinavaIctusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');

        $valorFormulario = 0.0;

        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"escandinava",

        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        $valoracionTotal = 0;

        $valoracionTotal = $request['opcion_conciencia'] + $request['opcion_mov_ocular']+$request['opcion_mov_musc_brazo']+
            $request['opcion_mov_musc_mano'] +$request['opcion_mov_musc_pierna'] +$request['opcion_orientacion'] +
            $request['opcion_lenguaje'] +$request['opcion_marcha'];


        EscandinavaIctus::create([

            'item1' =>"Conciencia",
            'respuesta1'=>$request['opcion_conciencia'] ,
            'item2'=>"Movimientos Oculares",
            'respuesta2'=>$request['opcion_mov_ocular'],
            'item3'=>"Fuerza Muscular en el brazo",
            'respuesta3'=>$request['opcion_mov_musc_brazo'],
            'item4' =>"Fuerza Muscular en la mano",
            'respuesta4'=> $request['opcion_mov_musc_mano'],
            'item5'=>"Fuerza Muscular en la pierna",
            'respuesta5'=>$request['opcion_mov_musc_pierna'],
            'item6' =>"Orientación",
            'respuesta6'=>$request['opcion_orientacion'],
            'item7' =>"Lenguaje",
            'respuesta7'=>$request['opcion_lenguaje'],
            'item8' =>"Marcha",
            'respuesta8'=>$request['opcion_marcha'],
            'porcentajeFinal' => $valoracionTotal,
            'valoracion_id' => $idValoracion,

        ]);

        return redirect()->route('grafico.escandinava',$id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
