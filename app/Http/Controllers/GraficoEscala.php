<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\TraitBarthel;
use App\Http\Controllers\Traits\TraitCanadiense;
use App\Http\Controllers\Traits\TraitCincinnati;
use App\Http\Controllers\Traits\TraitEscandinavaIctus;
use App\Http\Controllers\Traits\TraitEvaluacionParkinson;
use App\Http\Controllers\Traits\TraitHachinski;
use App\Http\Controllers\Traits\TraitHoehnYahr;
use App\Http\Controllers\Traits\TraitHuntHess;
use App\Http\Controllers\Traits\TraitMentalAbreviado;
use App\Http\Controllers\Traits\TraitMiniMental;
use App\Http\Controllers\Traits\TraitNihss;
use App\Http\Controllers\Traits\TraitRankin;
use App\Http\Controllers\Traits\TraitSchwab;
use App\Http\Controllers\Traits\TraitTestMental;
use App\Http\Controllers\Traits\TraitUpdrs;
use App\Http\Controllers\Traits\TraitValoracionClinica;
use App\Model\Admin;
use App\model\Valoracion;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;

class GraficoEscala extends Controller
{
    use TraitMentalAbreviado,TraitBarthel,TraitCincinnati,
        TraitEscandinavaIctus,TraitMiniMental,TraitTestMental,TraitHuntHess,TraitHoehnYahr,
        TraitRankin,TraitSchwab,TraitHachinski,TraitNihss,TraitValoracionClinica,TraitCanadiense,
        TraitEvaluacionParkinson, TraitUpdrs;
    
    public function __construct()
    {
        Date::setLocale('es');
//        $this->middleware('auth-admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkPaciente($paciente_id)
    {
        if ($paciente_id==null){
            $paciente_id=Auth::guard('web_paciente')->user()->id;
        }
        return $paciente_id;
    }

}
