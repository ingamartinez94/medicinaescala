<?php

namespace App\Http\Controllers;

use App\Model\MentalAbreviado;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class TestMentalAbreviadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');


        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"mental-abreviado",

        ]);


        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        $valorFormulario = $request['check_edad']+$request['check_hora_dia']+$request['check_direccion']+
            $request['check_año']+ $request['check_nombre_hospital']+$request['check_reconocer']+
            $request['check_fecha']+$request['check_año_guerra']+ $request['check_monarca']+ $request['check_contar'];

        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;

        MentalAbreviado::create([
            'item1'=>"Edad",
            'item2'=>"Hora del día",
            'item3'=>"Dirección",
            'item4'=>"Año",
            'item5'=>"Nombre del hospital",
            'item6'=>"Reconocer 2 personas",
            'item7'=>"Fecha de nacimiento",
            'item8'=>"Año de la Primera Guerra Mundial",
            'item9'=>"Nombre del presente Monarca",
            'item10'=>"Contar del 20 al 1",
            'respuesta1'=>$request['check_edad'],
            'respuesta2'=>$request['check_hora_dia'],
            'respuesta3'=>$request['check_direccion'],
            'respuesta4'=>$request['check_año'],
            'respuesta5'=>$request['check_nombre_hospital'],
            'respuesta6'=>$request['check_reconocer'],
            'respuesta7'=>$request['check_fecha'],
            'respuesta8'=>$request['check_año_guerra'],
            'respuesta9'=>$request['check_monarca'],
            'respuesta10'=>$request['check_contar'],
            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,
        ]);
        return redirect()->route('grafico.mental-abreviado',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
