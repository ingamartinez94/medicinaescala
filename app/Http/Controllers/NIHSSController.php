<?php

namespace App\Http\Controllers;

use App\model\Nihss;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;


class NIHSSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');



        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"nihss",

        ]);

        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;



        $valorFormulario = $request['opcion_nivel_conciencia']+$request['opcion_preguntas_LOC']+$request['opcion_ordenes_LOC']+
            $request['opcion_mirada']+$request['opcion_vision']+$request['opcion_paralisis_facial']+
            $request['opcion_brazo_izquierdo']+$request['opcion_brazo_derecho']+$request['opcion_pierna_izquierdo']+
            $request['opcion_piernas_derecha']+$request['opcion_ataxia']+
            $request['opcion_sensibilidad']+$request['opcion_lenguaje']+$request['opcion_disartria']+
            $request['opcion_negligencia'];

        Nihss::create([
            'item1'=>"1a Nivel de conciencia",
            'item2'=>"1b Preguntas LOC",
            'item3'=>"1c Órdenes LOC",
            'item4'=>"Mirada",
            'item5'=>"Visión",
            'item6'=>"Parálisis facial",
            'item7'=>"5a. Motor – Brazo Izquierdo",
            'item8'=>"5b. Motor – Brazo Derecho",
            'item9'=>"6a. Motor – Pierna Izquierda",
            'item10'=>"6b. Motor – Pierna Derecha",
            'item11'=>"Ataxia de miembros",
            'item12'=>"Sensibilidad",
            'item13'=>"Lenguaje",
            'item14'=>"Disartria",
            'item15'=>"Extinción e inatención (negligencia)",


            'respuesta1'=>$request['opcion_nivel_conciencia'],
            'respuesta2'=>$request['opcion_preguntas_LOC'],
            'respuesta3'=>$request['opcion_ordenes_LOC'],
            'respuesta4'=>$request['opcion_mirada'],
            'respuesta5'=>$request['opcion_vision'],
            'respuesta6'=>$request['opcion_paralisis_facial'],
            'respuesta7'=>$request['opcion_brazo_izquierdo'],
            'respuesta8'=>$request['opcion_brazo_derecho'],
            'respuesta9'=>$request['opcion_pierna_izquierdo'],
            'respuesta10'=>$request['opcion_piernas_derecha'],
            'respuesta11'=>$request['opcion_ataxia'],
            'respuesta12'=>$request['opcion_sensibilidad'],
            'respuesta13'=>$request['opcion_lenguaje'],
            'respuesta14'=>$request['opcion_disartria'],
            'respuesta15'=>$request['opcion_negligencia'],


            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,
        ]);

        return redirect()->route('grafico.nihss',$id);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
