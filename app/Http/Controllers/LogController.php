<?php

namespace App\Http\Controllers;

use App\Paciente;
use Illuminate\Http\Request;

use Auth;
use Session;
use Redirect;
use Config;
use Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function __construct()
    {
        $this->middleware('guest',['except' => ['logout']]);
    }
    
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
//        dd(Auth::guard('web_medico')->attempt(['email'=>$request['email'],'password'=>$request['password']]));

        if(Auth::guard('web_admin')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {
            return Redirect::to('admin');
        }

        if(Auth::guard('web_medico')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {
            return Redirect::to('medico');
        }

        if(Auth::guard('web_paciente')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {
            return Redirect::to('paciente');
        }

        return Redirect::to('/');
    }

    public function logout()
    {
        if (Auth::guard('web_medico')->check()) {
            Auth::guard('web_medico')->logout();
        }

        if (Auth::guard('web_admin')->check()) {
            Auth::guard('web_admin')->logout();
        }

        if (Auth::guard('web_paciente')->check()) {
            Auth::guard('web_paciente')->logout();
        }
        return Redirect::to('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request,$id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
