<?php

namespace App\Http\Controllers;

use App\Model\Medico;
use App\Model\Paciente;
use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Response;
use Redirect;

class MedicoController extends Controller
{
    public function __construct()
    {
        if (Auth::guard('web_admin')->guest()) {
            $this->middleware('auth-medico');
        }else{
            $this->middleware('auth-admin');
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacientes = Paciente::all();
        return view('medico.index', compact('pacientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Medico::create([

            'nombre' => $request['nombreMedico'],
            'apellido' => $request['nombreMedico'],
            'especialidad' => $request['nombreMedico'],
            'email' => $request['nombreMedico'],
            'password' => $request['nombreMedico'],


        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medico = Medico::findOrFail($id);

        return Response::json($medico);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loginId = Auth::guard('web_medico')->user()->id;
        if ($loginId == $id){
            $objMedico = Medico::findOrFail($id);

            return view('medico.edit',compact('objMedico'));

        }else{
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->ajax()){
            $medico = Medico::find($id);

            $medico->nombre =$request['nombre'];
            $medico->apellido = $request['apellido'];
            $medico->especialidad = $request['especialidad'];
            $medico->email = $request['correo'];

            $medico->save();
        }else{
            $medico = Medico::find($id);

//            $medico->nombre =$request['nombre'];
//            $medico->apellido = $request['apellido'];
//            $medico->especialidad = $request['especialidad'];
            $medico->email = $request['correo'];
            $medico->password = $request['contra'];

            $medico->save();

            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Medico::destroy($id);
    }
}
