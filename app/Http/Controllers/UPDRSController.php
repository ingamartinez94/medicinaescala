<?php

namespace App\Http\Controllers;

use App\model\Updrs;
use App\model\Valoracion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Auth;
use Session;
use Redirect;
use Config;
use Response;
use DB;

class UPDRSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $date = Carbon::now();
        $date = $date->format('d-m-Y');



        //Registrar Valoracion para despues usar su id
        $valoracionID = Valoracion::create([

            'fecha'=>$date,
            'paciente_id'=>$id,
            'medicos_id'=>Auth::guard('web_medico')->user()->id,
            'escala'=>"updrs",
        ]);

        //ID del ultimo insert en la tabla Valoracion
        $idValoracion = $valoracionID->id;



        $valorFormulario = $request['opcion_deterioro_congnitivo']+
            $request['opcion_alucinaciones']+
            $request['opcion_animo_depresivo']+
            $request['opcion_ansiedad']+
            $request['opcion_apatia']+
            $request['opcion_disregulacion']+
            $request['opcion_sueño']+
            $request['opcion_somnolencia']+
            $request['opcion_dolor']+
            $request['opcion_urinario']+
            $request['opcion_estreñimiento']+
            $request['opcion_mareo']+
            $request['opcion_habla']+
            $request['opcion_saliva']+
            $request['opcion_marticacion']+
            $request['opcion_act_comer']+
            $request['opcion_vestirse']+
            $request['opcion_higiene']+
            $request['opcion_escritura']+
            $request['opcion_hobbies']+
            $request['opcion_cama']+
            $request['opcion_temblor']+
            $request['opcion_levantarse']+
            $request['opcion_caminar']+
            $request['opcion_congelacion']+
            $request['opcion_lenguaje']+
            $request['opcion_expresion_facial']+
            $request['opcion_rigidez']+
            $request['opcion_golpeteo']+
            $request['opcion_mov_manos']+
            $request['opcion_pronacion_manos']+
            $request['opcion_golpeteo_dedos']+
            $request['opcion_agilidad_piernas']+
            $request['opcion_levantarse_silla']+
            $request['opcion_marcha']+
            $request['opcion_congelacion_marcha']+
            $request['opcion_estabilidad_postural']+
            $request['opcion_postura']+
            $request['opcion_espontaneidad_global']+
            $request['opcion_temblor_postural_manos']+
            $request['opcion_temblor_accion_manos']+
            $request['opcion_amplitud_temblor_reposo_extremidades']+
            $request['opcion_amplitud_temblor_reposo_labio']+
            $request['opcion_persistencia_temblor']+
            $request['opcion_tiempo_discinesias']+
            $request['opcion_impacti_funcional_discinesias']+
            $request['opcion_estado_off']+
            $request['opcion_impacto_funcional_fluctuaciones']+
            $request['opcion_fluctuaciones_motoras']+
            $request['opcion_distonia_dolorosa'];

        Updrs::create([
            'item1'=>"1.1. Deterioro cognitivo:",
            'item2'=>"1.2. Alucinaiones y psicosis:",
            'item3'=>"1.3. Estado de ánimo depresivo:",
            'item4'=>"1.4.	Ansiedad:",
            'item5'=>"1.5.	Apatía:",
            'item6'=>"1.6.	Manifestaciones del síndrome de disregulación dopaminérgica:",
            'item7'=>"1.7.	Problemas de sueño:",
            'item8'=>"1.8.	Somnolencia diurna:",
            'item9'=>"1.9.	Dolor y otras sensaciones:",
            'item10'=>"1.10.	Problemas urinarios:",
            'item11'=>"1.11.	Problemas de estreñimiento:",
            'item12'=>"1.12.	Sensación de mareo al ponerse de pie:",
            'item13'=>"2.1.	Hablar: durante los últimos siete días ¿ha tenido problemas para hablar?",
            'item14'=>"2.2.	Saliva y babeo",
            'item15'=>"2.3.	Marticación y deglución",
            'item16'=>"2.4.	Actividades para comer",
            'item17'=>"2.5.	Vestirse",
            'item18'=>"2.6.	Higiene",
            'item19'=>"2.7.	Escritura",
            'item20'=>"2.8.	Hobbies y otras actividades",
            'item21'=>"2.9.	Darse vuelta en la cama",
            'item22'=>"2.10	Temblor",
            'item23'=>"2.11.	Levantarse de la cama, del asiento de un coche o de un sillón",
            'item24'=>"2.12.	Caminar y equilibrio",
            'item25'=>"2.13.	Congelaciones o bloqueos (al caminar)",
            'item26'=>"3a. ¿Está recibiendo el paciente mediación para tratar los síntomas de la enfermedad de Parkinson?",
            'item27'=>"3b. Estado clinico del paciente",
            'item28'=>"3c. ¿Está el paciente tomando levodopa?",
            'item29'=>"3c.1. ¿En caso afirmativo, minutos transcurridos desde la últim dosis de levodopa",
            'item30'=>"3.1.	Lenguaje",
            'item31'=>"3.2.	Expresión facial",
            'item32'=>"3.3.	Rigidez",
            'item33'=>"3.4.	Golpeteo de dedos (finger tapping)",
            'item34'=>"3.5.	Movimientos con las manos",
            'item35'=>"3.6.	Movimientos de pronación-supinación de las manos",
            'item36'=>"3.7.	Golpeteo con los dedos de los pies (toe tapping):",
            'item37'=>"3.8.	Agilidad de las piernas",
            'item38'=>"3.9.	Levantarse de la silla",
            'item39'=>"3.10. Marcha",
            'item40'=>"3.11.	Congelación de la marcha",
            'item41'=>"3.12.	Estabilidad postural",
            'item42'=>"3.13.	Postura",
            'item43'=>"3.14.	Espontaniedad global del movimiento (bradicinesia corporal)",
            'item44'=>"3.15.	Temblor postural de las manos",
            'item45'=>"3.16.	Temblor de acción de las manos",
            'item46'=>"3.17.	Amplitud del temblor de reposo (extremidades)",
            'item47'=>"3.17.	Amplitud del temblor de reposo (labio/mandibula)",
            'item48'=>"3.18.	Persistencia del temblor de reposo",
            'item49'=>"A. ¿Hubo durante la exploración discinesias (corea o distonía)?",
            'item50'=>"B. En caso afirmativo, ¿interfirieron estos movimientos con la puntuación?",
            'item51'=>"4.1. Tiempo con discinesias",
            'item52'=>"4.2. Impacto funcional de las discinesia",
            'item53'=>"4.3. Tiempo en estado OFF",
            'item54'=>"4.4. Impacto funcional de las fluctuaciones",
            'item55'=>"4.5. Complejidad de las fluctuaciones motoras",
            'item56'=>"4.6. Distonía dolorosa en estado OFF",


            'respuesta_item1'=>$request['opcion_deterioro_congnitivo'],
            'respuesta_item2'=>$request['opcion_alucinaciones'],
            'respuesta_item3'=>$request['opcion_animo_depresivo'],
            'respuesta_item4'=>$request['opcion_ansiedad'],
            'respuesta_item5'=>$request['opcion_apatia'],
            'respuesta_item6'=>$request['opcion_disregulacion'],
            'respuesta_item7'=>$request['opcion_sueño'],
            'respuesta_item8'=>$request['opcion_somnolencia'],
            'respuesta_item9'=>$request['opcion_dolor'],
            'respuesta_item10'=>$request['opcion_urinario'],
            'respuesta_item11'=>$request['opcion_estreñimiento'],
            'respuesta_item21'=>$request['opcion_mareo'],
            'respuesta_item13'=>$request['opcion_habla'],
            'respuesta_item14'=>$request['opcion_saliva'],
            'respuesta_item15'=>$request['opcion_marticacion'],
            'respuesta_item16'=>$request['opcion_act_comer'],
            'respuesta_item17'=>$request['opcion_vestirse'],
            'respuesta_item18'=>$request['opcion_higiene'],
            'respuesta_item19'=>$request['opcion_escritura'],
            'respuesta_item20'=>$request['opcion_hobbies'],
            'respuesta_item21'=>$request['opcion_cama'],
            'respuesta_item22'=>$request['opcion_temblor'],
            'respuesta_item23'=>$request['opcion_levantarse'],
            'respuesta_item24'=>$request['opcion_caminar'],
            'respuesta_item25'=>$request['opcion_congelacion'],
            'respuesta_item26'=>$request['opcion_medicacion'],
            'respuesta_item27'=>$request['opcion_medicacion2'],
            'respuesta_item28'=>$request['opcion_levodoga'],
            'respuesta_item29'=>$request['dosis'],
            'respuesta_item30'=>$request['opcion_lenguaje'],
            'respuesta_item31'=>$request['opcion_expresion_facial'],
            'respuesta_item32'=>$request['opcion_rigidez'],
            'respuesta_item33'=>$request['opcion_golpeteo'],
            'respuesta_item34'=>$request['opcion_mov_manos'],
            'respuesta_item35'=>$request['opcion_pronacion_manos'],
            'respuesta_item36'=>$request['opcion_golpeteo_dedos'],
            'respuesta_item37'=>$request['opcion_agilidad_piernas'],
            'respuesta_item38'=>$request['opcion_levantarse_silla'],
            'respuesta_item39'=>$request['opcion_marcha'],
            'respuesta_item40'=>$request['opcion_congelacion_marcha'],
            'respuesta_item41'=>$request['opcion_estabilidad_postural'],
            'respuesta_item42'=>$request['opcion_postura'],
            'respuesta_item43'=>$request['opcion_espontaneidad_global'],
            'respuesta_item44'=>$request['opcion_temblor_postural_manos'],
            'respuesta_item45'=>$request['opcion_temblor_accion_manos'],
            'respuesta_item46'=>$request['opcion_amplitud_temblor_reposo_extremidades'],
            'respuesta_item47'=>$request['opcion_amplitud_temblor_reposo_labio'],
            'respuesta_item48'=>$request['opcion_persistencia_temblor'],
            'respuesta_item49'=>$request['opcion_discinesias'],
            'respuesta_item50'=>$request['opcion_interferir'],
            'respuesta_item51'=>$request['opcion_tiempo_discinesias'],
            'respuesta_item52'=>$request['opcion_impacti_funcional_discinesias'],
            'respuesta_item53'=>$request['opcion_estado_off'],
            'respuesta_item54'=>$request['opcion_impacto_funcional_fluctuaciones'],
            'respuesta_item55'=>$request['opcion_fluctuaciones_motoras'],
            'respuesta_item56'=>$request['opcion_distonia_dolorosa'],


            'porcentajeFinal' => $valorFormulario,
            'valoracion_id' =>$idValoracion,
        ]);

        return redirect()->route('grafico.updrs',$id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
