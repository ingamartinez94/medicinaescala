<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">

    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href={{URL::asset('css/login-style.css')}}>

</head>
<body>
<div class="login-body">
    <article class="container-login center-block">
        <section>

            <div class="tab-content tabs-login col-lg-12 col-md-12 col-sm-12 cols-xs-12">
                <div id="login-access" class="tab-pane fade active in">
                    <h2><i class="glyphicon glyphicon-log-in"></i> Accesso</h2>
                    {!! Form::open(['route'=>'log.store','method'=> 'POST','autocomplete'=> 'off','id'=>'formLogin','class'=>'form-horizontal']) !!}
                        <div class="form-group ">
                            <label for="login" class="sr-only">Email</label>
                            <input type="text" class="form-control" name="email" id="login_value" placeholder="Correo electrónico"
                                   tabindex="1" value="">
                        </div>
                        <div class="form-group ">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                   placeholder="Contraseña" value="" tabindex="2">
                        </div>

                        <br>
                        <div class="form-group ">
                            <button type="submit" name="log-me-in" id="submit" tabindex="5"
                                    class="btn btn-lg btn-primary">Entrar
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </article>
</div>

</body>
</html>