@extends('layouts.app')

@section('includes')
    @include('medico.modals.agregar-usuario')
    @include('paciente.modals.modal_escala_cincinnati')
    @include('paciente.modals.modal_escala_neurologica_canadiense')
    @include('paciente.modals.modal_escala_escandinava_ictus')
    @include('paciente.modals.modal_escala_indice_barthel')
    @include('paciente.modals.modal_escala_rankin')
    @include('paciente.modals.modal_escala_hunt_hess')
    @include('paciente.modals.modal_escala_mini_mental')
    @include('paciente.modals.modal_escala_test_mental')
    @include('paciente.modals.modal_escala_test_mental_abreviado')
    @include('paciente.modals.modal_escala_HoehnyYahr')
    @include('paciente.modals.modal_escala_Schwab')
    @include('paciente.modals.modal_escala_hachinski')
    @include('paciente.modals.modal_escala_nihss')
    @include('paciente.modals.modal_escala_updrs')
    @include('paciente.modals.modal_escala_motor_parkinson')
    @include('paciente.modals.modal_escala_valoracion_clinica')
@endsection

@section('opciones')


    <li><a href="#" data-toggle="modal" data-target="#modalAgregarPaciente">
            <svg class="glyph stroked pencil">
                <use xlink:href="#stroked-pencil"></use>
            </svg>
            Agregar Paciente</a></li>
@endsection


@section('panel-tabla')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{Auth::guard('web_medico')->user()->nombre}}
                    {{--{{Auth::guard('web_paciente')->user()->nombre}}--}}
                </div>
                <div class="panel-body">
                    <table id="table-listado" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Escala</th>
                            <th align="center">Descripción</th>
                            <th align="center">Opcion</th>

                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td> <strong> Escala de Cincinnati  </strong> </td>
                            <td><p>
                                    Es una escala ampliamente utilizada en el área pre-hospitalaria para el diagnóstico de un evento cerebrovascular.
                                    Debido a su fácil interpretación y reproductibilidad puede incluso ser realizada por personas ajenas al área de la salud

                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_cincinnati" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala Neurológica Canadiense  </strong> </td>
                            <td><p>
                                    Es una de las escalas más clásicas en la valoración de la capacidad funcional del ictus en pacientes que no estén
                                    en estupor o coma. Sirve para monitorizar la evolución del paciente en las primeras fases del ictus y en el
                                    seguimiento en su proceso de recuperación (11-13). Valora el nivel de conciencia, lenguaje, orientación y la respuesta motora,
                                    además puede aplicarse en pacientes afásicos o con imposibilidad para la comunicación

                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_neurologica_canadiense" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala Escandinava para el ictus  </strong> </td>
                            <td><p>
                                    La escala Escandinava para el ictus (SSS) ha sido utilizada en varios ensayos clínicos como una medida de la disfunción
                                    neurológica en el ictus. El contenido de la SSS incluye la evaluación del nivel de conciencia, función motora,
                                    trastorno de los nervios craneales, sensibilidad cutánea, marcha y lenguaje. <br>
                                    La puntuación máxima es de 46 puntos, excluyendo la evaluación de la marcha,
                                    que en cuyo caso añadiría 12 puntos más (muchos de los pacientes con ictus han de estar en cama los días
                                    iniciales de su evolución) para un total de 58 puntos, la cual indicaría normalidad y 0 puntos el máximo grado de discapacidad.

                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_escandinava_ictus" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala del Instituto Nacional de Salud (NIHSS).  </strong> </td>
                            <td><p>
                                    Esta escala permite la evaluación inicial del paciente en el ictus agudo y la monitorización neurológica en las unidades de ictus.
                                    Es utilizada ampliamente en estudios experimentales, ensayos clínicos y para la valoración de los pacientes candidatos a tratamiento
                                    trombolítico en la fase hiperaguda del ictus isquémico (11, 19, 20).<br>
                                    La escala valora el nivel de conciencia, visión, movimientos extra-oculares, paresia facial, fuerza en las extremidades, ataxia,
                                    sensibilidad, lenguaje y habla, lo cual permite una excelente estimación global de la función neurológica en el ictus agudo (11, 19, 20).<br>
                                    En cuanto al pronóstico, la escala ha mostrado utilidad en la medida que permite predecir el riesgo que tiene el paciente de requerir ingreso a la
                                    UCI después del uso de la terapia de trombólisis. Se ha visto que pacientes con un puntaje superior a 10 en esta escala, tienen un riesgo 7
                                    veces más riesgo de requerir UCI a comparación de aquellos pacientes con un puntaje inferior a 10 (19).



                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_nihss" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Índice de Barthel  </strong> </td>
                            <td><p>
                                    Es útil en la evaluación a largo plazo del pronóstico y evolución del paciente con ictus, ya
                                    que permite la valoración de la independencia del paciente en las actividades de la vida diaria (11).
                                    Consta de 10 ítems en los que se valora de 0 a 10 o de 0 a 15 puntos las respectivas discapacidades en
                                    los dominios propuestos y su puntuación alcanza 100 puntos (11, 21, 22).<br>
                                    Las actividades de la vida diaria incluidas en la tabla son diez: comer, trasladarse entre la silla y la cama,
                                    aseo personal, uso del retrete, bañarse/ducharse, desplazarse (andar en superficie lisa o en silla de ruedas),
                                    subir/bajar escaleras, vestirse/desvestirse, control de las heces y control de la orina (11, 21, 22).


                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_indice_barthel" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala de Rankin modificada  </strong> </td>
                            <td><p>

                                    Es una de las escalas más utilizadas en la literatura para cuantificar la discapacidad de
                                    los supervivientes a un ictus. Su puntuación va desde asintomático a discapacidad grave o muerte.
                                    Se utiliza ampliamente en el ámbito epidemiológico y para el seguimiento de los pacientes. Su validez
                                    y correlación con otras escalas, como el índice de Barthel, ha sido demostrada en múltiples estudios
                                    (11, 21-23).

                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_rankin" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala de Hunt y Hess para la hemorragia subaracnoidea </strong> </td>
                            <td><p>

                                    En la evaluación del cuadro clínico y gravedad de la hemorragia subaracnoidea (HSA) se emplea,
                                    más o menos modificada, una escala propuesta por Hunt y Hess (1968), que además posee indudable
                                    interés pronóstico, siendo clásico ya su empleo en ámbitos médicos y quirúrgicos. Ha mostrado mayor
                                    valor predictivo que la escala de coma de Glasgow y la WFNS-Scale (World Federation of Neurological Surgeons Scale)
                                    en el pronóstico de la rotura de aneurismas intracraneales (11).<br>
                                    Consta de cinco sencillos ítems excluyentes, entre los cuales hay que escoger el que
                                    mejor define la situación clínica del paciente (11).<br>
                                    Se ha visto que los pacientes con grado III o IV en esta escala, tienen un mayor riesgo de sufrir un vasoespasmo angiográfico,
                                    sintomático o un infarto cerebral (11, 24, 25).


                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_hunt_hess" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala Mini Mental </strong> </td>
                            <td><p>

                                    Es el test estándar para la evaluación rápida de la alteración cognitiva y la demencia, especialmente
                                    en la población geriátrica.  Su realización toma aproximadamente 10 minutos, por lo que es ideal para
                                    aplicarse en forma repetida y rutinaria (32-34).<br>
                                    Es una herramienta de tamizaje, lo cual no permite detallar el dominio alterado ni conocer la causa de
                                    la alteración y su utilidad está limitada en las siguientes situaciones: depresión, alcoholismo,
                                    ingestión de etanol, problemas metabólicos, deprivaciones ambientales, infecciones del sistema nervioso
                                    central, problemas nutricionales como el déficit de vitamina B6 y B12, hemorragia subaracnoidea y
                                    poblaciones sin habilidad lecto-escritora (analfabetismo, ceguera o afonía)(34, 35).


                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_mini_mental" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Test Mental  </strong> </td>
                            <td><p>

                                    Esta prueba y su versión abreviada, son cuestionarios que miden el grado de función cognitiva,
                                    particularmente la memoria y la orientación. Toma aproximadamente 10 minutos en ser realizado y es ampliamente utilizado (36).

                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_test_mental" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Test Mental Abreviado  </strong> </td>
                            <td><p>

                                    Esta prueba es la versión abreviada del Test Mental, es un cuestionario que mide el grado de función cognitiva, particularmente
                                    la memoria y la orientación. Toma aproximadamente 3 minutos en ser realizada, no ésta limitada por la capacidad visual, de lectura,
                                    escritura o de dibujo del paciente, y su validez ha sido evaluada en pacientes geriátricos hospitalizados con cognición normal,
                                    demencia y delirio (36).<br>
                                    El mejor punto de corte es 8/10 para diferenciar la condición normal de la anormal, incluyendo delirio, con una sensibilidad del
                                    91% pero, una especificidad baja de 75%.  Esta prueba no evalúa efectivamente la función frontal y ejecutiva (33).


                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal-escala-test-mental-abreviado" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Valoración clínica de la demencia (Versión en español de la CDR).  </strong> </td>
                            <td><p>
                                    Esta escala fue desarrollara en la Escuela de Medicina de la Universidad de Washington en St. Louis, Missouri, fue publicada
                                    inicialmente por Hughes y colaboradores en 1982. Originalmente se desarrolló para medir el estado de la demencia en los pacientes
                                    con enfermedad de Alzheimer, se ha extendido a otras demencias y es usada, alrededor del mundo, por clínicas que evalúan la memoria,
                                    estudios de investigación y ensayos clínicos de potenciales medicamentos terapéuticos (43, 44).<br>
                                    La CDR consiste en entrevistas independientes semi-estructuradas que usualmente toman lugar en el consultorio y realizadas cara a
                                    cara con el paciente. Antes de la entrevista con el paciente, una información confiable, usualmente dada por un familiar cercano,
                                    sobre las habilidades cognitivas del paciente debe ser asegurada. Esta es considerada como una escala clínica global que mide cambios
                                    sociales, en el comportamiento y la funcionalidad en sus actividades diarias. Tiene varias ventajas: es independiente de otras
                                    pruebas psicométricas, no requiere una línea de base y el individuo sirve como su propio control. Se ha visto que se correlaciona
                                    con marcadores de la severidad en la demencia, tiene valor predictivo en estudios longitudinales en la Enfermedad de Alzheimer,
                                    la aplicación de la escala requiere de un personal entenado que pueda obtener información confiable del paciente (43, 44).



                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_enica" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala de Hachinski  </strong> </td>
                            <td><p>

                                    La escala de isquemia de Hachinski fue diseñada de forma empírica para diferenciar demencia vascular multinfarto y enfermedad de
                                    Alzheimer.  Consta de 13 ítems, de los cuales se aclara que el deterioro escalonado hace referencia a la evolución en brotes y la
                                    hipertensión arterial implica el antecedente de uso de hipotensores o una tensión arterial en el momento de la exploración de
                                    170/110 o superior (45, 46).<br>
                                    Aunque es útil para diferenciar demencia vascular de la enfermedad de Alzheimer, es poco sensible en caso de afección de pequeños
                                    vasos (demencia subcortical) y en las formas mixtas.  No existe una relación lineal entre la puntuación y la certeza del diagnóstico
                                    (47, 48).


                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_hachinski" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala de Hoehn y Yahr  </strong> </td>
                            <td><p>
                                    Son utilizados para evaluar la intensidad de la
                                    enfermedad de manera sencilla en cinco grados, fundamentalmente síntomas y signos.
                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_HoehnyYahr" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        <tr>
                            <td> <strong> Escala de Actividades de la Vida Diaria de Schwab &  England </strong> </td>
                            <td><p>
                                    Se utiliza para evaluar, seguir y comparar las actividades de la vida diaria en esta enfermedad.
                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_Schwab" class="btn btn-danger">Realizar</a></td>

                        </tr>
                        <tr>
                            <td> <strong> Unified Parkinson's Disease Rating Scale (UPDRS) </strong> </td>
                            <td><p>
                                    Evalúa  los síntomas en la enfermedad de Parkinson para los estudios farmacológicos, quirúrgicos y de investigación clínica; dispone de numerosos estudios de fiabilidad y de análisis factorial de su estructura.
                                    <br>
                                    Recomendaciones iniciales:<br>
                                    1.	Seleccione inicialmente la fuente de la información.<br>
                                    2.	Evite leer las respuestas al paciente.<br>
                                    3.	Si el ítem no es aplicable, como por ejemplo evaluar la marcha en un paciente amputado, márquelo como NV.

                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_updrs" class="btn btn-danger">Realizar</a></td>

                        </tr>
                        <tr>
                            <td> <strong> Escala de evaluación de síntomas no motores en la enfermedad de Parkinson </strong> </td>
                            <td><p>
                                    La escala permite el seguimiento, tipificación de la gravedad y respuesta a los tratamientos de los síntomas no motores de la enfermedad de Parkinson.
                                </p>
                            </td>
                            <td><a data-toggle="modal" data-target="#modal_escala_motor_parkinson" class="btn btn-danger">Realizar</a></td>

                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!--/.row-->
@endsection

@push('script')

    <script>
        $(document).ready(function () {
            $('#table-listado').DataTable({
                "aaSorting": []
            });

        });
    </script>
@endpush