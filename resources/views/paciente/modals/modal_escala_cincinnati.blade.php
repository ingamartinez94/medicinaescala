<div class="modal fade" id="modal_escala_cincinnati" tabindex="-1" role="dialog" aria-labelledby="modal_escala_cincinnati">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_cincinnati">Escala Cincinnati</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'cincinnati/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaCincinnati']) !!}

                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('asimetria-facial','Haga que el paciente sonría o muestre los dientes') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_facial" value="normal">Normal</label>
                                <label class="radio-inline"><input type="radio" name="opcion_facial" value="anormal">Anormal</label>
                            </label>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('asimetria-facial','Haga que el paciente cierre los ojos y mantenga los brazos estirados durante 10 segundos') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_brazos" value="normal">Normal</label>
                                <label class="radio-inline"><input type="radio" name="opcion_brazos" value="anormal">Anormal</label>
                            </label>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('asimetria-facial','Lenguaje') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_lenguaje" value="normal">Normal</label>
                                <label class="radio-inline"><input type="radio" name="opcion_lenguaje" value="anormal">Anormal</label>
                            </label>
                        </div>

                    </div>
                </div>

            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaCincinnati').validate({
        rules: {
            opcion_facial: {
                required: true
            },
            opcion_brazos: {
                required: true
            },
            opcion_lenguaje: {
                required: true
            }
        }
    });

</script>
@endpush