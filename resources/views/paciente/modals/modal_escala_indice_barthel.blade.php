<div class="modal fade" id="modal_escala_indice_barthel" tabindex="-1" role="dialog" aria-labelledby="modal_escala_indice_barthel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_indice_barthel">Escala Indice de Barthel</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'indice-barthel/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaIndiceBarthel']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Comer') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_comer" value="10">
                                Independiente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_comer" value="5">
                                Necesita ayuda para cortar la carne o el pan, extender la mantequilla, etc.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_comer" value="0">
                                Dependiente.
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Lavarse') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_lavarse" value="5">
                                Independiente: es capaz de lavarse entero usando la ducha o el baño
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lavarse" value="0">
                                Dependiente
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Vestirse') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_vestirse" value="10">
                                Independiente: es capaz de ponerse y quitarse toda la ropa sin ayuda
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_vestirse" value="5">
                                Necesita ayuda, pero realiza solo, al menos la mitad de la tarea en un tiempo razonable
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_vestirse" value="0">
                                Dependiente
                            </label>

                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Arreglarse') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_arreglarse" value="5">
                                Independiente: incluye lavarse la cara y las manos, peinarse, maquillarse, afeitarse, etc
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_arreglarse" value="0">
                                Dependiente
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Deposición (valorar la semana previa)') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_deposicion" value="10">
                                Continente: ningún episodio de incontinencia
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_deposicion" value="5">
                                Ocasional: un episodio de incontinencia, necesita ayuda para administrarse enemas o supositorios
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_deposicion" value="0">
                                Incontinente.
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('','Micción (valorar la semana previa)') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_miccion" value="10">
                                Continente: ningún episodio de incontinencia
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_miccion" value="5">
                                Ocasional: como máximo un episodio de incontinencia en 24 horas; necesita ayuda para el cuidado de la sonda o el colector
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_miccion" value="0">
                                Incontinente
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Usar el retrete') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_retrete" value="10">
                                Independiente: usa el retrete, bacinilla, o cuña sin ayuda y sin manchar o mancharse
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_retrete" value="5">
                                Necesita una pequeña ayuda para quitarse y ponerse la ropa, pero se limpia solo.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_retrete" value="0">
                                Dependiente.
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('','Trasladarse (sillón/cama)') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_trasladarse" value="15">
                                Independiente
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_trasladarse" value="10">
                                Mínima ayuda física o supervisión verbal
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_trasladarse" value="5">
                                Necesita una gran ayuda (persona entrenada), pero se sienta sin ayuda
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_trasladarse" value="0">
                                Dependiente: necesita grúa o ayuda de dos personas, no puede permanecer sentado
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Deambular') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_deambular" value="15">
                                Independiente: camina solo 50 metros, se puede ayudar con bastón, muletas o andador sin ruedas; si utiliza prótesis es capaz de quitársela y ponérsela
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_deambular" value="10">
                                Necesita ayuda física o supervisión para andar 50 metros
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_deambular" value="5">
                                Independiente en silla de ruedas sin ayuda ni supervisión
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_deambular" value="0">
                                Dependiente.
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('','Subir escaleras') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_escaleras" value="10">
                                Independiente para subir y bajar un piso sin supervisión ni ayuda de otra persona
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_escaleras" value="5">
                                Necesita ayuda física de otra persona o supervisión
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_escaleras" value="0">
                                Dependiente
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>


@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaIndiceBarthel').validate({
        rules: {

            opcion_comer: {
                required: true
            },
            opcion_lavarse: {
                required: true
            },
            opcion_vestirse: {
                required: true
            },
            opcion_arreglarse: {
                required: true
            },
            opcion_deposicion: {
                required: true
            },
            opcion_miccion: {
                required: true
            },
            opcion_retrete: {
                required: true
            },
            opcion_trasladarse: {
                required: true
            },
            opcion_deambular: {
                required: true
            },
            opcion_escaleras: {
                required: true
            }
        }
    });

</script>
@endpush