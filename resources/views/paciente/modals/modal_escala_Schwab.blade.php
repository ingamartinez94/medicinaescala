<div class="modal fade" id="modal_escala_Schwab" tabindex="-1" role="dialog" aria-labelledby="modal_escala_Schwab">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_hunt_hess">Escala de Actividades de la Vida Diaria de Schwab &  England</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(['url'=>'schwab/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaSchwab']) !!}
                <table id="table-rankin" class="table table-striped table-bordered dataTable" cellspacing="0"
                       width="100%">

                    <thead>

                    <tr>
                        <th colspan="2"> Actividades de la vida diaria de schwab y england</th>
                    </tr>

                    <tr>

                        <th>Descripcion</th>
                        <th>Porcentaje</th>

                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Completamente independiente. Capaz de realizar cualquier tarea con
                            o sin lentitud o dificultad
                        </td>
                        <td>100</td>
                    </tr>

                    <tr>
                        <td>Completamente independiente. Puede tardar el doble de lo normal en realizar una
                            Tarea
                        </td>
                        <td>90</td>
                    </tr>

                    <tr>
                        <td>Independiente en la mayoría de tareas. Tarda el doble. Consciente de su dificultad
                            y enlentecimiento
                        </td>
                        <td>80</td>
                    </tr>

                    <tr>
                        <td>No completamente independiente. En algunas tareas tarda tres o cuatro veces más
                            de lo normal
                        </td>
                        <td>70</td>
                    </tr>

                    <tr>
                        <td>Alguna dependencia. Puede hacer la mayoría de tareas, pero muy lentamente
                            y con mucho esfuerzo
                        </td>
                        <td>60</td>
                    </tr>

                    <tr>
                        <td>Más dependiente. Necesita ayuda en la mitad de las tareas cotidianas. Dificultad
                            para todo

                        </td>
                        <td>50</td>
                    </tr>

                    <tr>
                        <td>Muy dependiente. Sólo puede realizar algunas tareas sin ayuda.
                            Con mucho esfuerzo puede realizar alguna tarea. Necesita mucha ayuda

                        </td>
                        <td>40</td>
                    </tr>

                    <tr>
                        <td>Ninguna tarea solo. Grave invalidez

                        </td>
                        <td>30</td>
                    </tr>

                    <tr>
                        <td>Totalmente dependiente. Puede ayudar algo en algunas actividades

                        </td>
                        <td>20</td>
                    </tr>

                    <tr>
                        <td>Dependiente. Inválido

                        </td>
                        <td>10</td>
                    </tr>

                    <tr>
                        <td>Postrado en cama. Ciertas funciones vegetativas (esfínteres) alteradas

                        </td>
                        <td>0</td>
                    </tr>

                    </tbody>
                </table>

                <div class="form-group">
                    <label>Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="2"></textarea>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}


            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaSchwab').validate({
        rules: {
            observaciones: {
                required: true
            }
        }
    });

</script>
@endpush