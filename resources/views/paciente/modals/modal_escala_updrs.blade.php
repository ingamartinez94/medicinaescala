<div class="modal fade bs-example-modal-lg" id="modal_escala_updrs" tabindex="-1" role="dialog" aria-labelledby="modal_escala_updrs">
    <div class="modal-dialog modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_updrs">Unified Parkinson's Disease Rating Scale </h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'updrs/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaUpdrs']) !!}
                {{--1.1.	Deterioro cognitivo: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.1.	Deterioro cognitivo: durante los últimos siete días ¿ha tenido problemas para recordar cosas, seguir conversaciones, prestar atención, pensar con claridad u orientarse en los alrededores de su casa o en su ciudad?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_deterioro_congnitivo" value="0">
                                Sin deterioro cognitivo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_deterioro_congnitivo" value="1">
                                El paciente o el cuidador perciben deterioro sin interferencias concretas en la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_deterioro_congnitivo" value="2">
                                Disfunción cognitiva clínicamente evidente, pero solamente causa unamínima interferencia en la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_deterioro_congnitivo" value="3">
                                Los déficits cognitivos interfieren en la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales, pero no las impiden.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_deterioro_congnitivo" value="4">
                                La disfunción cognitiva impide al paciente llevar a cabo actividades normales e interacciones sociales.
                            </label>
                        </div>

                    </div>
                </div>
                {{--1.2.	Alucinaiones y psicosis:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.2.	Alucinaiones y psicosis: durante los últimos siete días ¿ha visto, ído, olido o sentido cosas que realmente no estaban presentes?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_alucinaciones" value="0">
                                Sin alucinaciones o conducta psicótica.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_alucinaciones" value="1">
                                Ilusiones o laucinaciones no formes, pero el paciente las reconoce sin pérdida de instrospección.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_alucinaciones" value="2">
                                Alucinaciones formes independientes de los estíulos ambientales. Sin pérdida de intrspección.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_alucinaciones" value="3">
                                Alucinaciones formes con pérdida de introspección.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_alucinaciones" value="4">
                                El paciente tiene delirios o paranoia.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.3.	Estado de ánimo depresivo: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.3.	Estado de ánimo depresivo: durante los últimos siete días ¿se ha sentido depriido, triste, desesperanzado o incapaz de disfrutar? En caso afirmativo, ¿esta sensación ha durado más de un día? ¿Se le hizo difícil llevar a cabo sus actividades cotidianas o estar con gente?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_animo_depresivo" value="0">
                                Sin estado de ánimo depresivo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_animo_depresivo" value="1">
                                Episodios de estado de ánimo depresivo que no duran más de un día cada vez que ocurren. No interfieren con la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_animo_depresivo" value="2">
                                Estado de ánimo depresivo que se mantiene durante días, pero sin interferir con las actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_animo_depresivo" value="3">
                                Estado de ánimo depresivo que interfiere, pero no anula, la capacidad del paciente para desempreñar actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_animo_depresivo" value="4">
                                Estado de ánimo depresivo que impide al paciente llevar a cabo las actividades normales o interacciones sociales.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.4.	Ansiedad: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.4.	Ansiedad: durante los últimos siete días ¿se ha sentido nervioso, preocupado o tenso? En caso afirmativo, ¿esta sensación ha durado más de un día? ¿Se le hizo difícil seguir con sus actividades habituales o estar con gente?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_ansiedad" value="0">
                                Sin sensación de ansiedad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ansiedad" value="1">
                                Sensación de ansiedad presente, pero que no dura más de un día. Sin interferencia en la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ansiedad" value="2">
                                Sensación de ansiedad que dura más de un día, pero sin interferir en la capacidad del paciente para llevar a cabo actividades normales e interacaciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ansiedad" value="3">
                                Sensación de ansiedad que interfiere, péro no anula, la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ansiedad" value="4">
                                Sesación de ansiedad que anula la capacidad del paciente para llevar a cabo actividades normales e interacciones sociales.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.5.	Apatía: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.5.	Apatía: durante los últimos siete días ¿ha pérdido el interés para realizar actividades o para estar con gente?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_apatia" value="0">
                                Sin apatía.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_apatia" value="1">
                                Apatía percibida por el paciente y/o el cuidador, pero sin interferencia en las actividades diarias y las interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_apatia" value="2">
                                La apatía interfiere en actividades e interacciones sociales aisladas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_apatia" value="3">
                                La apatía interfiere con la mayoría de actividades e interacciones sociales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_apatia" value="4">
                                Pasivo y retraido, pérdida completa de la iniciativa.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.6.	Manifestaciones del síndrome de disregulación dopaminérgica: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.6.	Manifestaciones del síndrome de disregulación dopaminérgica: durante los últimos siete días ¿Se ha sentido impulsado a hacer o pensar en algo y le ha resultado difícil interrumpirlo? ¿Se ha sentido impulsado a hacer o pensar en algo y le haresultado difícil interrumpirlo?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_disregulacion" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disregulacion" value="1">
                                Existen problemas, pero habitualmente no causan dificultades al paciente o familia/cuidador.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disregulacion" value="2">
                                Existen problemas y habitualmente causan algunas dificultades en la vida personal y familiar del paciente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disregulacion" value="3">
                                Existen problemas y habitualmente causan muchas dificultades en la vida personal y familiar del paciente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disregulacion" value="4">
                                Existen problemas e impiden al paciente llevar a cabo actividades normales o interacciones sociales o mantener los niveles previos en su vida personal y familiar.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.7.	Problemas de sueño:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.7.	Problemas de sueño: durante los últimos siete días ¿ha tenido problemas para dormirse o permanecer dormido durante la noche? Considere hasta qué punto se ha sentido descansado al despertarse por la mañana.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_sueño" value="0">
                                No tengo problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_sueño" value="1">
                                Tengo problemas de sueño, pero habitualmente no me molestan para dormir toda la noche.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_sueño" value="2">
                                Los problemas de sueño me causas habitaulmente algunas dificultades para dormir toda la noche.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_sueño" value="3">
                                Los problemas de sueño me causas muchas dificultades para dormir toda la noche.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_sueño" value="4">
                                Habitualmente no duermo durante la mayor parte de la noche.
                            </label>

                        </div>
                    </div>
                </div>
                {{--1.8.	Somnolencia diurna:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.8.	Somnolencia diurna: durante los últimos siete días ¿ha tenido problemas para permanecer despierto durante el día?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_somnolencia" value="0">
                                No tengo somnolencia diurna durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_somnolencia" value="1">
                                Tengo somnolencia durante el día, pero puedo resistirla y permanecer despierto.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_somnolencia" value="2">
                                Algunas veces me quedo dormido cuadno estoy solo y relajado. Por ejemplo, cuando estoy leyendo o viendo la televisión.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_somnolencia" value="3">
                                Algunas veces me quedo dormido cuadno no debería. Por ejemplo, mientras estoy comiendo o conversando con otras personas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_somnolencia" value="4">
                                Con frecuencia me quedo dormido cuadno no debería. Por ejemplo, mientras esto comiendo o conversando con otras personas.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.9.	Dolor y otras sensaciones:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.9.	Dolor y otras sensaciones: durante los últimos siete días ¿hatenido molestias como dolor, hormigueos o calambres?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_dolor" value="0">
                                Sin molestias.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_dolor" value="1">
                                Tengo estas molestias. Sin embargo, puedo hacer cosas y estar con otras personas sin dificultad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_dolor" value="2">
                                Estas molestias me causas algunos problemas cuando hago cosas o estoy con otras personas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_dolor" value="3">
                                Estas molestias me causan muchos problemas, pero no me impiden hacer cosas o estar con otras personas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_dolor" value="4">
                                Estas molestias me impiden hacer cosas o estar con otras personas.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.10.	Problemas urinarios: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.10.	Problemas urinarios: durante los últimos siete días ¿ha tenido problemas para controlar la orina? Por ejemplo, necesidad urgente de orinar, necesidad de orinar con mucha frecuencia, o pérdidas de orina.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_urinario" value="0">
                                Sin problemas para controlar la orina.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_urinario" value="1">
                                Necesito orinar con frecuencia o urgentemente. Sin embargo, este problema no me causa dificultades en mis actividades diarias.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_urinario" value="2">
                                Los problemas urinarios me causan algunas dificultades en mis actividades diarias. Sin embargo, no tengo pérdidas de orina.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_urinario" value="3">
                                Los problemas urinarios, incluyendo pérdidas de orina, me causan muchas dificultades en mis actividades diarias.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_urinario" value="4">
                                No puedo controlar mi orina y uso pañales o tengo sonda (catéter).
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.11.	Problemas de estreñimiento: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.11.	Problemas de estreñimiento: durante los últimos siete días ¿ha tenido problemas de estreñimiento que le causen dificultad para defecar?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_estreñimiento" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estreñimiento" value="1">
                                He estado estreñido. Debo hacer esfuerzo para defecar. Sin embargo, este problema no altera mis actividades ni me molesta.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estreñimiento" value="2">
                                El estreñimiento me causa algunos problemas para hacer cosas o para sentirme cómodo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estreñimiento" value="3">
                                El estreñimiento me causa muchos problemas para hacer cosas o para sentimer cómodo. Sin embargo, no me impide hacer cualquier cosa.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estreñimiento" value="4">
                                Normalmente necesito ayuda física de otra persona para vaciar mi intestino.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1.12.	Sensación de mareo al ponerse de pie:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        1.12.	Sensación de mareo al ponerse de pie: durante los últimos siete días ¿se ha sentido desfallecer, mareado, o aturdido cuando se ha puesto de pie después de estar sentado o tumbado?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_mareo" value="0">
                                Sin sensación de mareo o aturdimiento.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mareo" value="1">
                                Tengo sensaciones de mareo o aturdimiento. Sin embargo, no me causan problemas para hacer las cosas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mareo" value="2">
                                El mareo o aturdimienot me obliga a apoyarme en algo, pero no necesito vover a sentarme o tumbarme.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mareo" value="3">
                                El mareo o aturdimiento me obliga a sentarme o tumbarme para no desmayarme o caerme.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mareo" value="4">
                                El mareo o aturdimiento me hace caer o desmayarme.
                            </label>
                        </div>
                    </div>
                </div>
                {{--2.1.	Hablar: durante los últimos siete días ¿ha tenido problemas para hablar?--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.1.	Hablar: durante los últimos siete días ¿ha tenido problemas para hablar?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_habla" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_habla" value="1">
                                Hablo bajo, pronuncio mal o irregular, pero no tanto como para que otras personanas me pidan que repita.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_habla" value="2">
                                Mi forma de hablar hace que otras personas me pidan a veces que repita, pero no todos los días.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_habla" value="3">
                                Hablo con tantos problemas que la gentes me pide todos los días que repita, aunque la mayor parte de lo que hablo puede entenderse.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_habla" value="4">
                                No puede entenderse la mayor parte o nada de lo que hablo.
                            </label>
                        </div>
                    </div>
                </div>
                {{--10. 2.2.	Saliva y babeo:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.2.	Saliva y babeo: durante los últimos siete días ¿ha tenido habitualmente demasiada saliva cuando est despierto o durmiendo?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_saliva" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_saliva" value="1">
                                Tengo demasiada saliva, pero no babeo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_saliva" value="2">
                                Tengo cierto babeo mientras duermo, pero no cuando estoy despierto.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_saliva" value="3">
                                Tengo cierto babeo cuadno estoy despierto, pero normalmente no necesito pañuelos o babero.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_saliva" value="4">
                                Tengo tanto babeo que necesito usar habitualmente pañuelos o un babero para proteger la ropa.
                            </label>
                        </div>
                    </div>
                </div>
                {{--2.3.	Marticación y deglución: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.3.	Marticación y deglución: durante los últimos siete días ¿ha tenido habitualmente problemas para tragar las pastillas o comer? ¿Necesita cortar o aplastar las pastillas o ablandar, picar o triturar las comidas para evitar atragantarse?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_marticacion" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marticacion" value="1">
                                Hablo bajo, pronuncio mal o irregular, pero no tanto como para que otras personanas me pidan que repita.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marticacion" value="2">
                                Mi forma de hablar hace que otras personas me pidan a veces que repita, pero no todos los días.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marticacion" value="3">
                                Hablo con tantos problemas que la gentes me pide todos los días que repita, aunque la mayor parte de lo que hablo puede entenderse.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marticacion" value="4">
                                No puede entenderse la mayor parte o nada de lo que hablo.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.4.	Actividades para comer: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.4.	Actividades para comer: durante los últimos siete días ¿ha tenido habitualmente problemas para manipular los alimentos y usar los cubiertos? Por ejemplo, ¿ha tenido problemas para comer con las manor o para usarl el tendor, el cuchillo, la cuchara o los palillos?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_act_comer" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_act_comer" value="1">
                                Soy lento, pero no necesito ayuda para comer y no se me caen los alimentos mientras como.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_act_comer" value="2">
                                Soy lento para comer y ocasionalmente se me cae la comida. Puedo necesitar ayuda para algunas tareas, como cortar la carne.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_act_comer" value="3">
                                Necesito ayuda en muchas de las actividades para comer, aunque puedo hacer otras actividades solo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_act_comer" value="4">
                                Necesito ayuda en todo o casi todo para comer.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.5.	Vestirse: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.5.	Vestirse: durante los últimos siete días ¿ha tenido habitualmente problemas para vestirse? Por ejemplo, ¿es usted lento o necesita ayuda para abotonarse, usar cremalleras, ponerse o quitarse la ropa o joyas?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_vestirse" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vestirse" value="1">
                                Soy lento, pero no necesito ayuda.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vestirse" value="2">
                                Soy lento y necesito ayuda para algunas cosas al vestirme (botones, pulseras).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vestirse" value="3">
                                Necesito ayuda para muchas cosas al vestirme.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vestirse" value="4">
                                Necesito ayuda en todo o casi todo para vestirme.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.6.	Higiene: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.6.	Higiene: durante los últimos siete días ¿ha sido habitualmente lento o ha necesitado ayuda al lavarse, bañarse, afeitarse, cepillarse los dientes, peinarse o con otras actividades de higiene personal?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_higiene" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_higiene" value="1">
                                Soy lento, pero no necesito ayuda.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_higiene" value="2">
                                Necesito que alguien me ayude con algunas actividades de higiene.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_higiene" value="3">
                                Necesito ayuda para muchas actividades de higiene.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_higiene" value="4">
                                Necesito ayuda en todo o casi todo para mi higiene.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.7.	Escritura:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.7.	Escritura: durante los últimos siete días, ¿han tenido otras personas problemas para leer su escritura?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_escritura" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_escritura" value="1">
                                Mi escritura es lenta, torpe o irregular, pero todas las palabras se leen claramente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_escritura" value="2">
                                Algunas palabras no son claras y se leen con dificultad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_escritura" value="3">
                                Muchas palabras no son claras y se leen con dificultad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_escritura" value="4">
                                No se pueden leer la mayoría de las palabras o ninguna.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.8.	Hobbies y otras actividades:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.8.	Hobbies y otras actividades: durante los últimos siete días ¿ha tenido habitualmente problemas para practicar sus hobbies (aficiones) o realizar otras actividades que le gusta hacer?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_hobbies" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_hobbies" value="1">
                                Soy un poco lento, pero hago esas actividades fácilmente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_hobbies" value="2">
                                Tengo algunas dificultades para hacer esas actividades.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_hobbies" value="3">
                                Tengo grandes problemas para esas actividades, pero todavía hago la mayoría.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_hobbies" value="4">
                                Soy incapaz de hacer todas o casi todas esas actividades.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.9.	Darse vuelta en la cama:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.9.	Darse vuelta en la cama: durante los últimos siete días ¿ha tenido habitualmente problemas para darse la vuelta en la cama?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_cama" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_cama" value="1">
                                Tengo algún problema, pero no necesito ayuda.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_cama" value="2">
                                Tengo muchos problemas para darme la vuelta y ocasionalente necesito que alguien me ayude.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_cama" value="3">
                                Para darme la vueta necesito, con frecuencia, que alguen me ayude.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_cama" value="4">
                                Soy incapaz de darme la vuelta sinayuda de alguien.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.10	Temblor: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.10	Temblor: durante los últimos siete días ¿ha tenido temblor?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_temblor" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor" value="1">
                                Tengo temblor, pero no me causa problemas en ninguna actividad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor" value="2">
                                El temblor me causa problemas solo en algunas actividades.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor" value="3">
                                El temblor me causa problemas en muchas de mis actividades diarias.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor" value="4">
                                El temblor me causa problemas en la mayoría o todas mis actividades.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.11.	Levantarse de la cama, del asiento de un coche o de un sillón: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.11.	Levantarse de la cama, del asiento de un coche o de un sillón: durante los últimos siete días ¿ha tenido habitualmente problemas para levantarse de a cama, del asiento de un coche o de un sillón?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_levantarse" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse" value="1">
                                Estoy lento o torpe, pero normalmente puedo hacerlo al primer intento.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse" value="2">
                                Necesito más de un intento para levantarme o necesito ayuda ocasionalmente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse" value="3">
                                Algunas veces necesito ayuda para levantarme, pero la mayor parte de las veces puedo hacerlo solo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse" value="4">
                                Necesito ayuda siempre o casi siempre.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.12.	Caminar y equilibrio: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.12.	Caminar y equilibrio: durante los últimos siete días ¿ha tenido problemas con el equilibrio y para caminar?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_caminar" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_caminar" value="1">
                                Soy un poco lento o puede que arrastre una pierna. Nunca utilizo una ayuda para caminar (bastón, andador).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_caminar" value="2">
                                Utilizo ocasionalmente una ayuda para caminar (bastón, andador), pero no necesito ayuda de otra persona.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_caminar" value="3">
                                Normalmente uso una ayuda para caminar (bastón, andador) de forma segura sin caerme. Sin embargo, normalmente no necesito apoyarme en otra persona.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_caminar" value="4">
                                Normalmente necesito apoyarme en otra persona para andar de forma segura sin caerme.
                            </label>

                        </div>
                    </div>
                </div>
                {{--2.13.	Congelaciones o bloqueos (al caminar):  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        2.13.	Congelaciones o bloqueos (al caminar): durante los últimos siete días ¿se queda parado o bloqueado de repente como si sus pies estuvieran pegados al suelo?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_congelacion" value="0">
                                No, en absoluto (sin problemas).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion" value="1">
                                Me bloqueo brevemente, pero puedo volver a andar con facilidad. No necesito que nadie me ayude ni una ayuda para caminar (bastón/andador) por los bloqueos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion" value="2">
                                Me bloqueo y tengo problemas para volver a andar, pero no necesito que nadie me ayude ni una ayuda para caminar (bastón/andador) por los bloqueos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion" value="3">
                                Cuando me bloqueo tengo muchos problemas para volver a andar y, debido a los bloqueos, algunas veces necesito usar una ayuda para caminar (bastón/andador) o que alguien me ayude.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion" value="4">
                                Debido a los bloqueos necesito usar, la mayor parte o todo el tiempo, una ayuda para caminar o que alguien me ayude.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3a. ¿Está recibiendo el paciente mediación para tratar los síntomas de la enfermedad de Parkinson?  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3a. ¿Está recibiendo el paciente mediación para tratar los síntomas de la enfermedad de Parkinson?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_medicacion" value="si">
                                Si
                            </label>
                            <label class="radio"><input type="radio" name="opcion_medicacion" value="no">
                                No
                            </label>

                        </div>
                    </div>
                </div>
                {{--3b. Si el paciente está tomando medicación para tratar los síntomas de la enfermedad de Parkinson, marque el estado clínico del paciente de acuerdo con las siguientes definiciones:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3b. Si el paciente está tomando medicación para tratar los síntomas de la enfermedad de Parkinson, marque el estado clínico del paciente de acuerdo con las siguientes definiciones:
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_medicacion2" value="on">
                                ON
                            </label>
                            <label class="radio"><input type="radio" name="opcion_medicacion2" value="off">
                                OFF
                            </label>

                        </div>
                    </div>
                </div>
                {{--3c. ¿Está el paciente tomando levodopa?  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3c. ¿Está el paciente tomando levodopa?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_levodoga" value="si">
                                Si
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levodoga" value="no">
                                No
                            </label>

                        </div>
                    </div>
                </div>
                {{--3c. ¿Está el paciente tomando levodopa?  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3c.1. ¿En caso afirmativo, minutos transcurridos desde la última dosis de levodopa:
                        ') !!}
                        {!! Form::text('dosis',null,['class'=>'form-control','placeholder'=>'5']) !!}
                    </div>
                </div>
                {{--3.1.	Lenguaje:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.1.	Lenguaje: Escuche el lenguaje espontáneo del paciente y mantenga una conversación con él. Temas sugeridos: pregúntele sobre su trabajo, aficiones, ejercicio o cómo ha llegado hasta la consulta. Evalúe el volumen, modulación (prosodia) y claridad, incluyendo mala articulación del lenguaje, palilalia (repetición de sílaba) y taquifemia (lenguaje rápido, juntando sílabas).
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="0">
                                Sin problemas de lenguaje.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="1">
                                Pérdida de modulación, dicción o volumen, pero todas las palabras se entenden fácilmente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="2">
                                Pérdida de modulación, dicción o volumen, con algunas palabras poco claras, pero se úeden entender las frases en conjunto.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="3">
                                El lenguaje es difícil de entender hasta tal punto que algunas, pero no todas las frases, se entienden mal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="4">
                                La mayor parte del lenguaje es difícil de entender o ininteligible.
                            </label>

                        </div>
                    </div>
                </div>
                {{--3.2.	Expresión facial: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.2.	Expresión facial: Observe al paciente sentado en reposo durante 10 segundos, mientras habla y sin hablar. Observe la frecuencia del parpadeo, si existe “cara de máscara” (amimia) o pérdida de la expresión facial, sonrisa espontánea y apertura de labios.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_expresion_facial" value="0">
                                Expresión facial normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_expresion_facial" value="1">
                                Minima “cara de máscara” (animia), manifestada uúnicamente por disminución de la frecuencia del parpadeo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_expresion_facial" value="2">
                                Además de la disminución de la frecuencia de parpadeo, también presenta amimia en la parte inferior de la cara, es decir, haymenos movimientos alrededor de la boca, como menos sonrisa espontánea, pero sin apertura de los labios.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_expresion_facial" value="3">
                                “Cara de máscara” (amimia) con apertura de labios parte del tiempo cuando la boca está en reposo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_expresion_facial" value="4">
                                “Cara de máscara” (amimia) con apertura de labios la mayor parte del tiempo cuando la boca está en reposo.
                            </label>

                        </div>
                    </div>
                </div>
                {{--3.3.	Rigidez:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.3.	Rigidez:La rigidez se evalúa mediante movimientos pasivos lentos de las grandes articulaciones con el paciente en una posición relajada y el evaluador manipulando las extremidades y el cuello. Primero, explore sin maniobra de activación. Explore y evalúe el cuello y cada extremidad por separado. Para los brazos, examine las articulaciones de muñecas y codo simultáneamente. Para las piernas, examine las articulaciones de cadera y rodilla simultáneamente. Si no se detecta rigidez, utilice una maniobra de activación, como por ejemplo el golpeteo de dedos (tapping), abrir/cerrar el puño, o taconeo, con una extremidad que no esté siendo explorada. Explique al paciente que permanezca tan relajado como sea posible mientras usted explora la rigidez.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_rigidez" value="0">
                                Sin rigidez
                            </label>
                            <label class="radio"><input type="radio" name="opcion_rigidez" value="1">
                                Rigidez solo detectable con maniobra de activación.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_rigidez" value="2">
                                La rigidez se detecta sin maniobra de activación, pero se consigue fácilmente el rango completo de movimiento.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_rigidez" value="3">
                                La rigidez se detecta sin maniobra de activación; se consigue el rango de movimiento completo con esfuerzo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_rigidez" value="4">
                                La rigidez se detecta sin maniobra de activación y no se consigue el rango completo de movimiento.
                            </label>

                        </div>
                    </div>
                </div>
                {{--3.4.	Golpeteo de dedos (finger tapping):  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.4.	Golpeteo de dedos (finger tapping): Explore cada mano por separado. Haga una demostración de la tarea, pero no continúe realizándola mientras evalúa al paciente. Instruya al paciente para que golpee el índice con el pulgar 10 veces tan rápida y ampliamente como sea posible. Puntúe cada lado por separado, evaluando velocidad, amplitud, titubeos, interrupciones y disminución de la amplitud.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_golpeteo" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo" value="1">
                                Cualquiera de los siguientes: a) el ritmo regular se rompe con una o dos interrupciones o titubeos en el movimiento de golpeteo; b) mínimo enlentecimiento; c) la amplitud disminuye cerca del final de los 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo" value="2">
                                Cualquiera de los siguientes: a) de 3 a 5 interrupciones durante el golpeteo; b) enlentecimiento leve; c) la amplitud disminuye hacia la mitad de la secuencia de 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo" value="3">
                                Cualquiera de los siguientes: a) más de 5 interrupciones durante el golpeteo o al menos una interrupción más prolongada (congelación) durante el movimiento en curso; b) enlentecimiento moderado; c) la amplitud disminuye después del primer golpeteo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo" value="4">
                                No puede o apenas puede realizar la tarea debido a enlentecimiento, interrupciones o decrementos.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.5.	Movimientos con las manos:   --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.5.	Movimientos con las manos: Explore cada mano por separado. Haga una demostración de la tarea, pero no continúe realizándola mientras evalúa al paciente. Instruya al paciente para que cierre fuerte el puño con el brazo doblado por el codo de forma que muestre la palma de la mano al evaluador. Pida al paciente que abra y cierre la mano 10 veces tan rápida y completamente como le sea posible. Si el paciente no cierra fuerte el puño o no abre la mano completamente, recuérdele que lo haga. Puntúe cada lado por separado, evaluando velocidad, amplitud, titubeos, interrupciones y disminución de la amplitud.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_mov_manos" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mov_manos" value="1">
                                Cualquiera de los siguientes: a) el ritmo regular se rompe con una o dos interrupciones o titubeos en el movimiento de golpeteo; b) mínimo enlentecimiento; c) la amplitud disminuye cerca del final de los 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mov_manos" value="2">
                                Cualquiera de los siguientes: a) de 3 a 5 interrupciones durante el golpeteo; b) enlentecimiento leve; c) la amplitud disminuye hacia la mitad de la secuencia de 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mov_manos" value="3">
                                Cualquiera de los siguientes: a) más de 5 interrupciones durante el golpeteo o al menos una interrupción más prolongada (congelación) durante el movimiento en curso; b) enlentecimiento moderado; c) la amplitud disminuye después del primer golpeteo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mov_manos" value="4">
                                No puede o apenas puede realizar la tarea debido a enlentecimiento, interrupciones o decrementos.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.6.	Movimientos de pronación-supinación de las manos:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.6.	Movimientos de pronación-supinación de las manos: Explore cada mano por separado. Haga una demostración de la tarea, pero no continúe realizándola mientras evalúa al paciente. Instruya al paciente para que extienda el brazo hacia el frente con la palma de la mano hacia abajo; luego, que gire la palma de la mano hacia arriba y hacia abajo alternativamente 10 veces, tan rápida y completamente como sea posible. Puntúe cada lado por separado, evaluando velocidad, amplitud, titubeos, interrupciones y disminución de la amplitud.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_pronacion_manos" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pronacion_manos" value="1">
                                Cualquiera de los siguientes: a) el ritmo regular se rompe con una o dos interrupciones o titubeos en el movimiento de golpeteo; b) mínimo enlentecimiento; c) la amplitud disminuye cerca del final de los 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pronacion_manos" value="2">
                                Cualquiera de los siguientes: a) de 3 a 5 interrupciones durante el golpeteo; b) enlentecimiento leve; c) la amplitud disminuye hacia la mitad de la secuencia de 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pronacion_manos" value="3">
                                Cualquiera de los siguientes: a) más de 5 interrupciones durante el golpeteo o al menos una interrupción más prolongada (congelación) durante el movimiento en curso; b) enlentecimiento moderado; c) la amplitud disminuye después del primer golpeteo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pronacion_manos" value="4">
                                No puede o apenas puede realizar la tarea debido a enlentecimiento, interrupciones o decrementos.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.7.	Golpeteo con los dedos de los pies (toe tapping):   --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.7.	Golpeteo con los dedos de los pies (toe tapping): Haga que el paciente se siente en una silla con respaldo recto y reposabrazos, con ambos pies sobre el suelo. Explore cada pie por separado. Haga una demostración de la tarea, pero no continúe realizándola mientras evalúa al paciente. Instruya al paciente para que coloque los talones en el suelo en una posición cómoda y luego golpee con los dedos de los pies (antepié) 10 veces tan amplia y rápidamente como sea posible. Puntúe cada lado por separado, evaluando velocidad, amplitud, titubeos (dubitaciones), interrupciones y disminución de la amplitud
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_golpeteo_dedos" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo_dedos" value="1">
                                Cualquiera de los siguientes: a) el ritmo regular se rompe con una o dos interrupciones o titubeos en el movimiento de golpeteo; b) mínimo enlentecimiento; c) la amplitud disminuye cerca del final de los 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo_dedos" value="2">
                                Cualquiera de los siguientes: a) de 3 a 5 interrupciones durante el golpeteo; b) enlentecimiento leve; c) la amplitud disminuye hacia la mitad de la secuencia de 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo_dedos" value="3">
                                Cualquiera de los siguientes: a) más de 5 interrupciones durante el golpeteo o al menos una interrupción más prolongada (congelación) durante el movimiento en curso; b) enlentecimiento moderado; c) la amplitud disminuye después del primer golpeteo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_golpeteo_dedos" value="4">
                                No puede o apenas puede realizar la tarea debido a enlentecimiento, interrupciones o decrementos.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.8.	Agilidad de las piernas:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.8.	Agilidad de las piernas:  Haga que el paciente se siente en una silla con respaldo recto y reposabrazos. El paciente debe tener ambos pies colocados cómodamente en el suelo. Puntúe cada pierna por separado. Haga una demostración de la tarea, pero no continúe realizándola mientras evalúa al paciente. Instruya al paciente para que ponga un pie en el suelo en una posición cómoda y luego lo levante y golpee el suelo 10 veces tan rápida y ampliamente como le sea posible. Puntúe cada lado por separado, evaluando velocidad, amplitud, titubeos, interrupciones y diminución de la amplitud.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_agilidad_piernas" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_agilidad_piernas" value="1">
                                Cualquiera de los siguientes: a) el ritmo regular se rompe con una o dos interrupciones o titubeos en el movimiento de golpeteo; b) mínimo enlentecimiento; c) la amplitud disminuye cerca del final de los 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_agilidad_piernas" value="2">
                                Cualquiera de los siguientes: a) de 3 a 5 interrupciones durante el golpeteo; b) enlentecimiento leve; c) la amplitud disminuye hacia la mitad de la secuencia de 10 golpeteos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_agilidad_piernas" value="3">
                                Cualquiera de los siguientes: a) más de 5 interrupciones durante el golpeteo o al menos una interrupción más prolongada (congelación) durante el movimiento en curso; b) enlentecimiento moderado; c) la amplitud disminuye después del primer golpeteo.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_agilidad_piernas" value="4">
                                No puede o apenas puede realizar la tarea debido a enlentecimiento, interrupciones o decrementos.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.9.	Levantarse de la silla:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.9.	Levantarse de la silla: Haga que el paciente se siente en una silla con respaldo recto y reposabrazos, con ambos pies en el suelo y la espalda apoyada en el respaldo (si el paciente no es demasiado bajo). Pida al paciente que cruce los brazos sobre el pecho y se levante. Si no lo consigue, repita el intento dos veces más como máximo. Si sigue sin conseguirlo, permita al paciente que avance un poco hacia adelante en la silla para levantarse con los brazos cruzados sobre el pecho. Permita solo un intento en esta situación. Si tampoco lo consigue, permita al paciente que se levante apoyando las manos en el reposabrazos. Permita hasta tres intentos de levantarse. Si no lo consigue, ayude al paciente a levantarse. Después de que el paciente se levante, observe la postura para el ítem 3.13.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_levantarse_silla" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse_silla" value="1">
                                Se levanta más lentamente de lo normal; o puede necesitar más de un intento; o puede necesitar avanzar un poco hacia adelante en la silla para levantarse. No necesita usar los reposabrazos de la silla.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse_silla" value="2">
                                Se levanta sin dificultad apoyándose en los reposabrazos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse_silla" value="3">
                                Necesita apoyarse, pero tiende a caer hacia atrás; o puede tener que intentarlo más de una vez utilizando los reposabrazos, pero puede levantarse sin ayuda.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_levantarse_silla" value="4">
                                Incapaz de levantarse sin ayuda.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.10.	Marcha:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.10.	Marcha: La marcha se explora mejor haciendo que el paciente camine alejándose y acercándose al evaluador, de forma que se pueda observar fácilmente el lado izquierdo y derecho del cuerpo de manera simultánea. El paciente debe caminar al menos 10 metros (30 pies), luego girar y volver hacia el evaluador. Este ítem evalúa varios aspectos: amplitud de la zancada, velocidad de la zancada, altura a la que se levantan los pies, taloneo al caminar, giro y balanceo de los brazos, pero no la congelación (freezing). Evalúe también la “congelación de la marcha” (siguiente ítem 3.11) mientras el paciente camina. Observe la postura para el ítem 3.13.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_marcha" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marcha" value="1">
                                Camina independientemente con minima alteración de la marcha.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marcha" value="2">
                                Camina independientemente pero con alteración sustancial de la marcha.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marcha" value="3">
                                Requere un dispositivo de ayuda para caminar de forma segura (bastón, andador) pero no ayuda de otra persona.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_marcha" value="4">
                                No puede andar en absoluto o solo camina con ayuda de otra persona.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.11.	Congelación de la marcha:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.11.	Mientras evalúa la marcha, evalúe también la presencia de cualquier episodio de congelación de la marcha. Observe si hay dubitación al inicio y movimientos “de titubeo” (stuttering) especialmente en el giro y cuando esté llegando al final de la tarea. Hasta donde la seguridad lo permita, los pacientes NO deben usar trucos sensoriales durante la evaluación.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_congelacion_marcha" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion_marcha" value="1">
                                Congelación al inicio, al girarse o al pasar una puerta con solo una interrupción durante cualquiera de estas actividades, pero luego continúa sin congelaciones durante la marcha en línea recta.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion_marcha" value="2">
                                Congelación al inicio, al girarse o al pasar una puerta con más de una interrupción durante cualquiera de estas actividades, pero luego continúa sin congelaciones durante la marcha en línea recta.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion_marcha" value="3">
                                Aparece congelación una vez durante la marcha en línea recta.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_congelacion_marcha" value="4">
                                Aparece congelación varias veces durante la marcha en línea reacta.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.12.	Estabilidad postural:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.12.	Estabilidad postural: Esta prueba explora la respuesta a un desplazamiento súbito del cuerpo producido por un empujón rápido y enérgico sobre los hombros del paciente mientras permanece erguido de pie con los ojos abiertos y los pies comodamente separados y paralelos entre sí. Examine la retropulsión. Colóquese detrás del paciente y explíquele lo que va a ocurrir. Explique que puede dar un paso atrás para evitar caerse. Debe haber una pared sólida detrás del evaluador, a 1-2 metros de distancia al menos para poder observar el número de pasos en retropulsión. El primer empujón es sólo de demostración, intencionadamente leve y no se evalúa. En el segundo, se empuja los hombros vigorosamente hacia el evaluador, con suficiente fuerza como para desplazar el centro de gravedad del paciente y que éste TENGA QUE dar un paso hacia atrás. El evaluador debe estar preparado para sujetar al paciente, pero debe ponerse suficientemente atrás como para permitir que el paciente dé varios pasos y se pueda recuperar por sí solo. No permita que el paciente flexione el cuerpo hacia delante anormalmente anticipándose al empujón. Observe el número de pasos hacia atrás o si se cae. Hasta dos pasos hacia atrás para recuperarse se considera normal, por lo que se considera anormal a partir de tres pasos. Si el paciente no comprende la prueba, el evaluador puede repetirla, de tal forma que la puntuación se base en la valoración que el evaluador piense que refleja las limitaciones del paciente en lugar de la falta de comprensión o de preparación. Observe la postura al estar de pie para el ítem 3.13.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_estabilidad_postural" value="0">
                                Sin problemas: el paciente se recupera en uno o dos pasos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estabilidad_postural" value="1">
                                De 3 a 5 pasos, pero el paciente se recupera sin ayuda.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estabilidad_postural" value="2">
                                Más de 5 pasos, pero el paciente se recupera sin ayuda
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estabilidad_postural" value="3">
                                Permanece de pie de forma segura, pero con ausencia de respuesta postural; se cae si el evaluador no lo sujeta.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estabilidad_postural" value="4">
                                Muy inestable, tiende a perder el equilibrio espontáneamente o solo con un ligero empujón en los hombros.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.13.	Postura:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.13.	Postura: La postura se evalúa con el paciente erguido de pie después de levantarse de una silla, durante la marcha, y mientras se evalúan los reflejos posturales. Si observa una postura anormal, pida al paciente que se ponga derecho para ver si la postura mejora (ver la opción 2 más abajo). Evalúe la peor postura que haya observado en estos tres momentos de observación. Observe si hay flexión e inclinación hacia los lados.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_postura" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_postura" value="1">
                                El paciente no está totalmente erguido, pero la postura puede ser normal para una persona mayor.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_postura" value="2">
                                Evidente flexión, escoliosis o inclinación hacia un lado, pero el paciente puede corregir hasta adoptar una postura normal si se le pide.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_postura" value="3">
                                Postura encorvada, escoliosis o inclinación hacia un lado, que el paciente no puede corregir voluntariamente hasta una postura normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_postura" value="4">
                                Flexión, escoliosis o inclinación con anormalidad postural extrema.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.14.	Espontaniedad global del movimiento (bradicinesia corporal):--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.14.	Espontaniedad global del movimiento (bradicinesia corporal): Esta puntuación global combina todas las observaciones sobre enlentecimiento, titubeos, y escasa amplitud y pobreza de movimientos en general, incluyendo una reducción en la gesticulación y en el cruce de piernas. La evaluación se basa en la impresión global del evaluador después de observar la gesticulación espontánea mientras que el paciente está sentado, y la forma de levantarse y caminar.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_espontaneidad_global" value="0">
                                Sin problemas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_espontaneidad_global" value="1">
                                Minimo enlentecimiento global y pobreza de movimientos espontáneos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_espontaneidad_global" value="2">
                                Leve enlentecimiento global y pobreza de movimientos espontáneos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_espontaneidad_global" value="3">
                                Moderado enlentecimiento global y pobreza de movimientos espontáneos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_espontaneidad_global" value="4">
                                Enlentecimiento global grave y pobreza de movimientos espontáneos.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.15.	Temblor postural de las manos:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.15.	Temblor postural de las manos: Se incluye en la evaluación todo temblor, incluido el temblor de reposo re-emergente, que esté presente en esta postura. Evalúe cada mano por separado. Evalúe la mayor amplitud observada. Instruya al paciente para que estire los brazos hacia delante con las palmas de las manos hacia abajo. La muñeca debe estar recta y los dedos cómodamente separados de tal forma que no se toquen entre sí. Observe esta postura durante 10 segundos.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_temblor_postural_manos" value="0">
                                Sin temblor.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_postural_manos" value="1">
                                Hay temblor pero de una amplitud menor de 1 cm.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_postural_manos" value="2">
                                El temblor tiene una amplitud de al menos 1 cm pero menor de 3 cm.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_postural_manos" value="3">
                                El temblor tiene una amplitud de al menos 3 cm pero menor de 10 cm.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_postural_manos" value="4">
                                El temblor tiene una amplitud de al menos 10 cm.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.16.	Temblor de acción de las manos:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.16.	Temblor de acción de las manos: Se evalúa con la maniobra dedo-nariz. Partiendo de la posición con los brazos estirados, pida al paciente que toque tres veces la punta de la nariz con un dedo de cada mano, llegando tan lejos como sea posible para tocar el dedo del evaluador. La maniobra dedo-nariz debe ejecutarse lo suficientemente lenta para que no se encubra cualquier temblor, lo que ocurriría con movimientos del brazo muy rápidos. Repetir con la otra mano, evaluando cada mano por separado. El temblor puede estar presente durante el transcurso del movimiento o cuando se alcance cualquiera de los objetivos (nariz o dedo). Evalúe la mayor amplitud observada.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_temblor_accion_manos" value="0">
                                Sin temblor.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_accion_manos" value="1">
                                Hay temblor pero de una amplitud menor de 1 cm.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_accion_manos" value="2">
                                El temblor tiene una amplitud de al menos 1 cm pero menor de 3 cm.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_accion_manos" value="3">
                                El temblor tiene una amplitud de al menos 3 cm pero menor de 10 cm.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_temblor_accion_manos" value="4">
                                El temblor tiene una amplitud de al menos 10 cm.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.17.	Amplitud del temblor de reposo:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.17.	Amplitud del temblor de reposo: Este ítem y el siguiente se han colocado intencionadamente al final de la exploración con el propósito de permitir que el evaluador reúna las observaciones sobre el temblor de reposo que aparezca durante la exploración, incluyendo cuando el paciente está sentado tranquilamente, al caminar y durante aquellas actividades en que mueva determinadas partes del cuerpo mientras otras están en reposo. Considere como puntuación final la amplitud máxima que observe en cualquier momento. Evalúe solo la amplitud y no la persistencia o intermitencia del temblor. Como parte de esta evaluación, el paciente debe estar sentado tranquilamente en una silla con las manos apoyadas en el reposa-brazos (no en el regazo) y los pies apoyados en el suelo de forma cómoda, durante 10 segundos, sin recibir ninguna otra indicación. El temblor de reposo se evalúa por separado para cada extremidad y también para el labio y la mandíbula. Considere como evaluación final sólo la amplitud máxima que haya observado en cualquier momento.
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Puntuación para las extremidades:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_extremidades" value="0">
                                Sin temblor.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_extremidades" value="1">
                                < 1 cm de amplitud máxima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_extremidades" value="2">
                                > 1 cm pero < 2 cm de amplitud máxima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_extremidades" value="3">
                                3 – 10 cm de amplitud máxima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_extremidades" value="4">
                                > 10 cm de amplitud máxima.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Puntuación para labio/mandibula:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_labio" value="0">
                                Sin temblor.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_labio" value="1">
                                < 1 cm de amplitud máxima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_labio" value="2">
                                > 1 cm pero < 2 cm de amplitud máxima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_labio" value="3">
                                3 – 10 cm de amplitud máxima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_amplitud_temblor_reposo_labio" value="4">
                                > 10 cm de amplitud máxima.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.18.	Persistencia del temblor de reposo:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        3.18.	Persistencia del temblor de reposo: Este ítem recibe una puntuación única para todo el temblor de reposo y se centra en la persistencia de dicho temblor durante la exploración, cuando diferentes partes del cuerpo están en reposo. Se puntúa al final de la exploración con el propósito de unir en la evaluación varios minutos de información.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_persistencia_temblor" value="0">
                                Sin temblor.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_persistencia_temblor" value="1">
                                El temblor de reposo está presente < 25% del tiempo total de la expoloración.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_persistencia_temblor" value="2">
                                El temblor de reposo está presente 26 - 50% del tiempo total de la expoloración.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_persistencia_temblor" value="3">
                                El temblor de reposo está presente 51 – 75% del tiempo total de la expoloración.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_persistencia_temblor" value="4">
                                El temblor de reposo está presente > 75% del tiempo total de la expoloración.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.18.	Persistencia del temblor de reposo:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <label style="display: block; font-weight: 700" for="">
                            IMPACTO DE LA DISCINESIA EN LA PUNTUACIÓN DE LA PARTE III
                        </label>
                        {!! Form::label('','
                        A. ¿Hubo durante la exploración discinesias (corea o distonía)?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_discinesias" value="si">
                                Si
                            </label>
                            <label class="radio"><input type="radio" name="opcion_discinesias" value="no">
                                No
                            </label>
                        </div>
                    </div>
                </div>
                {{--3.18.	Persistencia del temblor de reposo:--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        B. En caso afirmativo, ¿interfirieron estos movimientos con la puntuación?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_interferir" value="si">
                                Si
                            </label>
                            <label class="radio"><input type="radio" name="opcion_interferir" value="no">
                                No
                            </label>
                        </div>
                    </div>
                </div>
                {{--4.1. Tiempo con discinesias: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        4.1. Tiempo con discinesias: Durante los últimos siete días, ¿cuántas horas ha dormido habitualmente al día, incluyendo sueño nocturno y siestas? De acuerdo, si usted duerme ___ horas, está despierto ___ horas. De estas horas en las que está despierto, ¿durante cuántas en total ha tenido movimientos serpenteantes, sacudidas, o tirones?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_tiempo_discinesias" value="0">
                                Sin discinesias.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_tiempo_discinesias" value="1">
                                < o = 25% del tiempo que permanece despierto durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_tiempo_discinesias" value="2">
                                26 – 50% del tiempo que permanece despierto durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_tiempo_discinesias" value="3">
                                51 – 75% del tiempo que permanece despierto durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_tiempo_discinesias" value="4">
                                > 75% del tiempo quepermanece despierto durante el día.
                            </label>
                        </div>
                    </div>
                </div>
                {{--4.2. Impacto funcional de las discinesia:  --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        4.2. Impacto funcional de las discinesia: Durante los últimos siete días, ¿ha tenido habitualmente problemas para hacer cosas o para estar con gente cuando ocurrían esos movimientos bruscos? ¿Le han impedido hacer cosas o estar con gente?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_impacti_funcional_discinesias" value="0">
                                Sin discinesias o las discinesias no interfieren en las actividades o en la interacción social.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacti_funcional_discinesias" value="1">
                                Las discinesias interfieren unas pocas actividades, pero el paciente realiza habitualmente todas las actividades y participa en todas las interacciones sociales durante los periodos con discinesia.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacti_funcional_discinesias" value="2">
                                Las discinesias interfieren muchas actividades, pero el paciente realiza habitualmente todas sus actividades y participa en todas las interacciones sociales durante los periodos con discinesia.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacti_funcional_discinesias" value="3">
                                Las discinesias interfieren las actividades hasta el punto en que el paciente no puede realizar habitualmente algunas actividades o no participa habitualmente en algunas actividades sociales durante los periodos con discinesia.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacti_funcional_discinesias" value="4">
                                Las discinesias interfieren en el funcionamiento hasta el punto en que, habitualmente, el paciente no puede realizar la mayoría de actividades o participar en la mayoría de actividades sociales durante los periodos con discinesia.
                            </label>
                        </div>
                    </div>
                </div>
                {{--4.3. Tiempo en estado OFF: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        4.3. Tiempo en estado OFF: Me dijo antes que, durante la semana pasada, estuvo habitualmente despierto ___ horas al día. De estas horas en las que está despierto, ¿durante cuántas en total tiene habitualmente este tipo de nivel bajo o funcionamiento en OFF? ___ (utilice este número para hacer sus cálculos).
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_estado_off" value="0">
                                Sin periodos OFF.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estado_off" value="1">
                                < o = 25% del tiempo que permanece despierto durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estado_off" value="2">
                                26 – 50% del tiempo que permanece despierto durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estado_off" value="3">
                                51 – 75% del tiempo que permanece despierto durante el día.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_estado_off" value="4">
                                > 75% del tiempo quepermanece despierto durante el día.
                            </label>
                        </div>
                    </div>
                </div>
                {{--4.4. Impacto funcional de las fluctuaciones: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        4.4. Impacto funcional de las fluctuaciones: Piense en los periodos “bajos” o “con el bajón” o en OFF que hayan ocurrido durante los últimos siete días ¿Tiene habitualmente más problemas para hacer cosas o para estar con gente en comparación con el resto del día en que nota que su medicación funciona? ¿Hay algunas cosas que haga durante los periodos buenos que no pueda hacer o que tenga problemas para hacer cuando está “con el bajón”?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_impacto_funcional_fluctuaciones" value="0">
                                Sin fluctuaciones o las fluctuaciones no interfieren en las actividades o en la interacción social.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacto_funcional_fluctuaciones" value="1">
                                Las fluctuaciones interfieren unas pocas actividades pero, durante el periodo OFF, el paciente normalmente realiza todas las actividades y participa en todas las interacciones sociales que ocurren habitualmente durante el estado ON.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacto_funcional_fluctuaciones" value="2">
                                Las fluctuaciones interfieren muchas actividades pero, durante el periodo OFF, el paciente normalmente realiza todas sus actividades y participa en todas las interacciones sociales que ocurren habitualmente durante el estado ON.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacto_funcional_fluctuaciones" value="3">
                                Las fluctuaciones interfieren la realización de actividades durante el periodo OFF hasta el punto que el paciente no puede realizar habitualmente algunas actividades o no participa habitualmente en algunas actividades sociales que se llevan a cabo durante los períodos ON.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_impacto_funcional_fluctuaciones" value="4">
                                Las fluctuaciones interfieren en el funcionamiento hasta el punto que, durante el periodo en OFF, el paciente no realiza habitualmente la mayoría de actividades o no participa en la mayoría de actividades sociales que se llevan a cabo durante los períodos ON.
                            </label>
                        </div>
                    </div>
                </div>
                {{--4.5. Complejidad de las fluctuaciones motoras: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        4.5. Complejidad de las fluctuaciones motoras: Durante los últimos siete días, ¿supo habitualmente cuándo iban a ocurrir sus períodos “de bajón”? En otras palabras, ¿sus períodos “bajos” o “de bajón” aparecen siempre en determinados momentos? ¿Aparecen en su mayor parte en determinados momentos? ¿Aparecen solo algunas veces en determinados momentos? ¿Son sus periodos “de bajón” totalmente impredecibles?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_fluctuaciones_motoras" value="0">
                                Sin fluctuaciones motoras.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_fluctuaciones_motoras" value="1">
                                Los periodos en OFF son predecibles todas o casi todas las veces (> 75%).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_fluctuaciones_motoras" value="2">
                                Los periodos en OFF son predecibles la mayor parte de las veces (51-75%).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_fluctuaciones_motoras" value="3">
                                Los periodos en OFF son predecibles algunas veces (26-50%).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_fluctuaciones_motoras" value="4">
                                Los periodos en OFF son raramente predecibles (< o = 25%).
                            </label>
                        </div>
                    </div>
                </div>
                {{--4.6. Distonía dolorosa en estado OFF: --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                        4.6. Distonía dolorosa en estado OFF: En una de las preguntas anteriores, dijo que generalmente pasa ___ horas “bajo” o “con el bajón” o en OFF cuando su enfermedad de Parkinson no está bien controlada. Durante estos periodos “de bajón” o en OFF, ¿tiene habitualmente calambres o espasmos dolorosos? Del total de ___ horas “bajo” o “con el bajón”, si suma todo el tiempo que pasa en un día con estos calambres dolorosos, ¿cuántas horas serían?
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_distonia_dolorosa" value="0">
                                Sin distonías o periodos en OFF.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_distonia_dolorosa" value="1">
                                < o = 25% del tiempo en situación OFF.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_distonia_dolorosa" value="2">
                                26-50% del tiempo en situación OFF.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_distonia_dolorosa" value="3">
                                51-75% del tiempo en situación OFF.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_distonia_dolorosa" value="4">
                                > 75% del tiempo en situación OFF.
                            </label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaUpdrs').validate({
        rules: {
            opcion_deterioro_congnitivo: {
                required: true
            },
            opcion_alucinaciones: {
                required: true
            },
            opcion_animo_depresivo: {
                required: true
            },
            opcion_ansiedad: {
                required: true
            },
            opcion_apatia: {
                required: true
            },
            opcion_disregulacion: {
                required: true
            },
            opcion_sueño: {
                required: true
            },
            opcion_somnolencia: {
                required: true
            },
            opcion_dolor: {
                required: true
            },
            opcion_urinario: {
                required: true
            },
            opcion_estreñimiento: {
                required: true
            },
            opcion_mareo: {
                required: true
            },
            opcion_habla: {
                required: true
            },
            opcion_saliva: {
                required: true
            },
            opcion_marticacion: {
                required: true
            },
            opcion_act_comer: {
                required: true
            },
            opcion_vestirse: {
                required: true
            },
            opcion_higiene: {
                required: true
            },
            opcion_escritura: {
                required: true
            },
            opcion_hobbies: {
                required: true
            },
            opcion_cama: {
                required: true
            },
            opcion_temblor: {
                required: true
            },
            opcion_levantarse: {
                required: true
            },
            opcion_caminar: {
                required: true
            },
            opcion_congelacion: {
                required: true
            },
            opcion_medicacion:{
                required: true
            },
            opcion_medicacion2: {
                required: false
            },
            opcion_levodoga: {
                required: true
            },
            dosis: {
                required: false,
                number: true
            },
            opcion_lenguaje: {
                required: true
            },
            opcion_expresion_facial: {
                required: true
            },
            opcion_rigidez: {
                required: true
            },
            opcion_golpeteo: {
                required: true
            },
            opcion_mov_manos: {
                required: true
            },
            opcion_pronacion_manos: {
                required: true
            },
            opcion_golpeteo_dedos: {
                required: true
            },
            opcion_agilidad_piernas: {
                required: true
            },
            opcion_levantarse_silla: {
                required: true
            },
            opcion_marcha: {
                required: true
            },
            opcion_congelacion_marcha: {
                required: true
            },
            opcion_estabilidad_postural: {
                required: true
            },
            opcion_postura: {
                required: true
            },
            opcion_espontaneidad_global: {
                required: true
            },
            opcion_temblor_postural_manos: {
                required: true
            },
            opcion_temblor_accion_manos: {
                required: true
            },
            opcion_amplitud_temblor_reposo_extremidades: {
                required: true
            },
            opcion_amplitud_temblor_reposo_labio: {
                required: true
            },
            opcion_persistencia_temblor: {
                required: true
            },
            opcion_discinesias: {
                required: true
            },
            opcion_interferir:{
                required: true
            },
            opcion_tiempo_discinesias:{
                required: true
            },
            opcion_impacti_funcional_discinesias:{
                required: true
            },
            opcion_estado_off:{
                required: true
            },
            opcion_impacto_funcional_fluctuaciones:{
                required: true
            },
            opcion_fluctuaciones_motoras:{
                required: true
            },
            opcion_distonia_dolorosa:{
                required: true
            },

        }
    });

</script>
@endpush