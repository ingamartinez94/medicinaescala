<div class="modal fade" id="modal-escala-test-mental-abreviado" tabindex="-1" role="dialog" aria-labelledby="modal_escala_test_mental_abreviado">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_test_mental_abreviado">Test Mental Abreviado</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'test-mental-abreviado/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaTestMentalAbreviado']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_edad" value="1">
                                    Edad
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_hora_dia" value="1">
                                    Hora del Día
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_año" value="1">
                                    Año
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_nombre_hospital" value="1">
                                    Nombre del Hospital
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label style="display: block; font-weight: normal" for="">
                            Dar una dirección que se le pedirá al paciente repita para asegurar que la escucho y que recuerde al final del test: Calle 42 Este
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_direccion" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label style="display: block; font-weight: normal" for="">
                            Reconocer 2 personas, ejemplo: Doctor, Enfermera
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_reconocer" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_fecha" value="1">
                                Fecha de nacimiento
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_año_guerra" value="1">
                                Año de la Primera Guerra Mundial
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_monarca" value="1">
                                Nombre del presente Monarca
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_contar" value="1">
                                Contar del 20 al 1
                            </label>
                        </div>
                    </div>

                </div>
                <div class="row">


                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>