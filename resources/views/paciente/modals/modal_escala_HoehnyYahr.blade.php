<div class="modal fade" id="modal_escala_HoehnyYahr" tabindex="-1" role="dialog" aria-labelledby="modal_escala_HoehnyYahr">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_hunt_hess">Escala de Hoehn y Yahr</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(['url'=>'hoehn-yahr/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaHoehYahr']) !!}
                <table id="table-rankin" class="table table-striped table-bordered dataTable" cellspacing="0"
                       width="100%">

                    <thead>

                        <tr>
                            <th colspan="2"> Estadios de Hoehn y Yahr</th>
                        </tr>

                    <tr>
                        <th>Estadio</th>
                        <th>Descripcion</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Estadio 1</td>
                        <td>Afectación exclusivamente unilateral, sin afectación funcional o con mínima afectación.</td>
                    </tr>

                    <tr>
                        <td>Estadio 2</td>
                        <td>Afectación bilateral o axial (línea media), sin alteración del equilibrio.</td>
                    </tr>

                    <tr>
                        <td>Estadio 3 </td>
                        <td>Alteración de los reflejos de enderezamiento (al dar vueltas o en el test de pulsión).
                            Restricción discreta de las actividades laborales, pero puede hacer algunos trabajos. Vida independiente.</td>
                    </tr>

                    <tr>
                        <td>Estadio 4 </td>
                        <td>Enfermedad completamente desarrollada, gravemente incapacitante.
                            El paciente aún es capaz de caminar y permanecer en pie sin ayuda.</td>
                    </tr>

                    <tr>
                        <td>Estadio 5</td>
                        <td>Confinamiento en cama o silla de ruedas, a menos que se le preste asistencia.</td>
                    </tr>

                    </tbody>
                </table>
                <br>
                <table id="table-rankin" class="table table-striped table-bordered dataTable" cellspacing="0"
                       width="100%">

                    <thead>

                    <tr>
                        <th colspan="2"> Estadios de Hoehn y Yahr modificados </th>
                    </tr>

                    <tr>
                        <th>Estadio</th>
                        <th>Descripcion</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Estadio 1</td>
                        <td>Afectación exclusivamente unilateral, sin afectación funcional o con mínima afectación.</td>
                    </tr>

                    <tr>
                        <td>Estadio 1,5</td>
                        <td>Compromiso unilateral más afectación axial.</td>
                    </tr>

                    <tr>
                        <td>Estadio 2 </td>
                        <td>Afectación bilateral o axial (línea media), sin alteración del equilibrio.</td>
                    </tr>

                    <tr>
                        <td>Estadio 2,5</td>
                        <td>Afectación bilateral con recuperación en el test de pulsión.</td>
                    </tr>

                    <tr>
                        <td>Estadio 3</td>
                        <td>Alteración de los reflejos de enderezamiento (al dar vueltas o en el test de pulsión).
                            Restricción discreta de las actividades laborales, pero puede hacer algunos trabajos. Vida independiente.</td>
                    </tr>

                    <tr>
                        <td>Estadio 4</td>
                        <td>Enfermedad completamente desarrollada, gravemente incapacitante.
                            El paciente aún es capaz de caminar y permanecer en pie sin ayuda.</td>
                    </tr>

                    <tr>
                        <td>Estadio 4,5</td>
                        <td>El paciente no puede salir de su domicilio sin ayuda.</td>
                    </tr>

                    <tr>
                        <td>Estadio 5</td>
                        <td>Confinamiento en cama o silla de ruedas, a menos que se le preste asistencia.</td>
                    </tr>

                    </tbody>
                </table>


                <div class="form-group">
                    <label>Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="2"></textarea>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}


            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaHoehYahr').validate({
        rules: {
            observaciones: {
                required: true
            }
        }
    });

</script>
@endpush