<div class="modal fade" id="modal_escala_valoracion_clinica" tabindex="-1" role="dialog"
     aria-labelledby="modal_escala_valoracion_clinica">
    <div class="modal-dialog modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_valoracion_clinica">Valoración clínica de la
                    demencia (Versión en español de la CDR).</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'valoracion-clinica/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaValoracionClinica']) !!}
                {{--1a. Nivel de conciencia--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        <div class="table-responsive">
                            <table width="850" border="1" cellspacing="2">
                                <tr align="center" valign="middle">
                                    <th colspan="6">Nivel del deterioro</th>
                                </tr>
                                <tr align="center" valign="middle">

                                    <th></th>

                                    <th>
                                        <p class="padding">
                                            <label class="radio fix-radio"><input type="radio" name="opcion_valoracion"
                                                                                  value="0">
                                                Ninguno (0)
                                            </label>
                                        </p>
                                    </th>

                                    <th>
                                        <p class="padding">
                                            <label class="radio fix-radio"><input type="radio" name="opcion_valoracion"
                                                                                  value="0.5">
                                                Dudoso (0.5)
                                            </label>
                                        </p>

                                    </th>

                                    <th>
                                        <p class="padding">
                                            <label class="radio fix-radio" pa><input type="radio"
                                                                                     name="opcion_valoracion" value="1">
                                                Leve (1)
                                            </label>
                                        </p>
                                    </th>

                                    <th>
                                        <p class="padding">
                                            <label class="radio fix-radio"><input type="radio" name="opcion_valoracion"
                                                                                  value="2">
                                                Moderado (2)
                                            </label>
                                        </p>

                                    </th>
                                    <th>
                                        <p class="padding">
                                            <label class="radio fix-radio"><input type="radio" name="opcion_valoracion"
                                                                                  value="3">
                                                Grave (3)
                                            </label>
                                        </p>
                                    </th>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td><strong> Memoria </strong></td>
                                    <td>Sin pérdida de memoria o leves olvidos inconstantes.</td>
                                    <td>Olvidos leves constantes; recolección parcial de eventos; olvidos “benignos”.
                                    </td>
                                    <td>Pérdida moderada de memoria; más marcada par eventos recientes; el defecto
                                        interfiere con las actividades diarias.
                                    </td>
                                    <td>Pérdida grave de memoria; sólo retiene materias con mucho aprendizaje;
                                        materias nuevas se pierden con rapidez.
                                    </td>
                                    <td>Pérdida grave de memoria; sólo retiene fragmentos.</td>

                                </tr>

                                <tr align="center" valign="middle">
                                    <td><strong> Orientación. </strong></td>
                                    <td>Completamente orientado.</td>
                                    <td>Completamente orientado pero, con leve dificultad para las relaciones
                                        temporales.
                                    </td>
                                    <td>Dificultad moderada con las relaciones temporales; orientado en el lugar del
                                        examen;
                                        puede tener algo de desorientación geográfica en otro lugar.
                                    </td>
                                    <td>Dificultad grave con las relaciones temporales; habitualmente desorientados/a en
                                        el tiempo;
                                        a menudo en el lugar.
                                    </td>
                                    <td>Orientado sólo en persona.</td>

                                </tr>

                                <tr align="center" valign="middle">
                                    <td><strong> Razonamiento y solución de problemas. </strong></td>
                                    <td>Resuelve problemas diarios y se encarga bien de los negocios y finanzas;
                                        razonamiento bueno con relación al comportamiento previo.
                                    </td>
                                    <td>Leve dificultad para resolver problemas, similitudes y diferencias.</td>
                                    <td>Dificultad moderada para hacer frente a problemas, similitudes y diferencias;
                                        razonamiento social habitual mantenido.
                                    </td>
                                    <td>Grandes dificultades para hacer frente a problemas, similitudes y diferencias;
                                        juicio social habitual limitado.
                                    </td>
                                    <td>Incapaz de razonar o resolver problemas.</td>
                                </tr>

                                <tr align="center" valign="middle">
                                    <td><strong> Actividades fuera de casa. </strong></td>
                                    <td>Función independiente a su nivel habitual en el trabajo, compras, voluntariado y
                                        agrupaciones sociales.
                                    </td>
                                    <td>Leve dificultad en estas actividades.</td>
                                    <td>Incapaz de ser independiente en estas actividades, aunque aún puede participar
                                        en alguna;
                                        parece normal a primera vista.
                                    </td>
                                    <td>Parece estar lo suficientemente bien como para realizar funciones fuera de
                                        casa.
                                    </td>
                                    <td>Parece demasiado enfermo/a como para realizar funciones fuera de su casa.</td>

                                </tr>

                                <tr align="center" valign="middle">
                                    <td><strong> Actividades domésticas y aficiones </strong></td>
                                    <td>Vida en casa, aficiones e intereses intelectuales bien conservados.</td>
                                    <td>Vida en casa, aficiones e intereses intelectuales algo limitados.</td>
                                    <td>Dificultad leve pero clara de su actividad doméstica; abandono de las tareas más
                                        difíciles;
                                        abandono de las aficiones e intereses más complicados.
                                    </td>
                                    <td>Sólo realiza tareas simples; intereses muy restringidos y mal mantenidos.</td>
                                    <td>Sin función significativa en casa.</td>

                                </tr>

                                <tr align="center" valign="middle">
                                    <td><strong> Cuidado personal. </strong></td>
                                    <td colspan="2">Completamente capaz de cuidarse por sí mismo/a.</td>

                                    <td>Necesita recordatorios.</td>
                                    <td>Requiere ayuda para vestirse, asearse y encargarse de sus efectos personales.
                                    </td>
                                    <td>Requiere mucha ayuda para su cuidado personal; incontinencia frecuente.</td>

                                </tr>


                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase() == "radio") {
                error.insertBefore(element.closest('table'));
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaValoracionClinica').validate({
        rules: {
            opcion_valoracion: {
                required: true
            }
        },
        messages: {
            opcion_valoracion: {
                required: "Debe escoger una de las opciones resaltadas"
            }
        }
    });

</script>
@endpush