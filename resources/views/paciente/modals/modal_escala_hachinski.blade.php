<div class="modal fade" id="modal_escala_hachinski" tabindex="-1" role="dialog" aria-labelledby="modal_escala_hachinski">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_cincinnati">Escala de Hachinski</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'hachinski/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalHachinski']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('brusco','Comienzo brusco.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_brusco" value="2">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_brusco" value="0">No</label>
                            </label>
                        </div>

                    </div>

                    <div class="col-md-6 form-group">

                        {!! Form::label('deterioro','Deterioro escalonado.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_deterioro" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_deterioro" value="0">No</label>
                            </label>
                        </div>


                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('fluctuante','Curso fluctuante.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_fluctuante" value="2">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_fluctuante" value="0">No</label>
                            </label>
                        </div>

                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('desorientacion','Desorientación nocturna.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_desorientacion" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_desorientacion" value="0">No</label>
                            </label>
                        </div>

                    </div>

                </div>




                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('preservación','Preservación relativa de la personalidad.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_preservación" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_preservación" value="0">No</label>
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('depresion','Depresión.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_depresion" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_depresion" value="0">No</label>
                            </label>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('somatización','Somatización.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_somatización" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_somatización" value="0">No</label>
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('labilidad','Labilidad emocional.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_labilidad" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_labilidad" value="0">No</label>
                            </label>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6 form-group">
                        {!! Form::label('historia','Historia de hipertensión arterial.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_historia" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_historia" value="0">No</label>
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('historia_ictus','Historia de ictus previos.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_historia_ictus" value="2">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_historia_ictus" value="0">No</label>
                            </label>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-md-6 form-group">
                        {!! Form::label('evidencia','Evidencia de arteriosclerosis asociada.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_evidencia" value="1">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_evidencia" value="0">No</label>
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('sintomas','Síntomas neurológicos focales.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_sintomas" value="2">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_sintomas" value="0">No</label>
                            </label>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-md-6 form-group">
                        {!! Form::label('signos','Signos neurológicos focales.') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_signos" value="2">Si</label>
                                <label class="radio-inline"><input type="radio" name="opcion_signos" value="0">No</label>
                            </label>
                        </div>
                    </div>

                </div>



            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalHachinski').validate({
        rules: {

            opcion_brusco: {
                required: true
            },
            opcion_deterioro: {
                required: true
            },
            opcion_fluctuante: {
                required: true
            },
            opcion_desorientacion: {
                required: true
            },
            opcion_preservación: {
                required: true
            },
            opcion_depresion: {
                required: true
            },
            opcion_somatización: {
                required: true
            },
            opcion_historia: {
                required: true
            },
            opcion_historia_ictus: {
                required: true
            },
            opcion_evidencia: {
                required: true
            },
            opcion_sintomas: {
                required: true
            },
            opcion_signos: {
                required: true
            },
            opcion_labilidad: {
                required: true
            }
        }
    });

</script>
@endpush