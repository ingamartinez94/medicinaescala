<div class="modal fade" id="modal_escala_mini_mental" tabindex="-1" role="dialog" aria-labelledby="modal_escala_mini_mental">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_mini_mental">Escala Mini Mental</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'mini-mental/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaMiniMental']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Orientación') !!}
                        <label style="display: block; font-weight: normal" for="">
                            ¿Cuál es el ...?
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="11" value="1">
                                    Año?
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="12" value="1">
                                    Temporada?
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="13" value="1">
                                    Fecha?
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="14" value="1">
                                    Mes?
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="15" value="1">
                                    Día?
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Orientación') !!}
                        <label style="display: block; font-weight: normal" for="">
                            ¿Dónde estamos?
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="16" value="1">
                                Pais
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="17" value="1">
                                Estado
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="18" value="1">
                                Ciudad
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="19" value="1">
                                Hospital
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="20" value="1">
                                Piso
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Registro') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Mencione 3 objetos, espere 1 segundo entre cada uno, entonces pida al paciente que los mencione.
                            Repita hasta que el paciente diga los 3.
                        </label>
                        <div class="radio">
                            <label class="radio" ><input type="checkbox" name="21" value="1">
                                1 objeto
                            </label>
                            <label class="radio" ><input type="checkbox" name="22" value="1">
                                2 objetos
                            </label>
                            <label class="radio" ><input type="checkbox" name="23" value="1">
                                3 objetos
                            </label>

                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Recuerdo') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Pregunte los tres objetos dichos en el ítem “Registro” (por cada respuesta correcta se da 1 punto).
                        </label>
                        <div class="radio">
                            <label class="radio" ><input type="checkbox" name="31" value="1">
                                Recordó 1 objeto (1 punto)
                            </label>
                            <label class="radio" ><input type="checkbox" name="32" value="2">
                                Recordó 2 objetos (2 puntos)
                            </label>
                            <label class="radio" ><input type="checkbox" name="33" value="3">
                                Recordó 3 objetos (3 puntos)
                            </label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        {!! Form::label('','Atención y Cálculo') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Realice sustracciones de 3 en 3 (ejemplo: si tiene 100 pesos y me va dando de 3 en 3 ¿Cuánto le queda?), haga series de 7 y deténgase después de 5 respuestas.
                            Si el paciente no puede realizar la esta prueba, pídale deletrear “mundo” al revés
                        </label>
                        <div class="radio">
                            <label class="radio" ><input type="checkbox" name="41" value="1">
                                1 respuesta
                            </label>
                            <label class="radio" ><input type="checkbox" name="42" value="2">
                                2 respuestas
                            </label>
                            <label class="radio" ><input type="checkbox" name="43" value="3">
                                3 respuestas
                            </label>
                            <label class="radio" ><input type="checkbox" name="44" value="4">
                                4 respuestas
                            </label>
                            <label class="radio" ><input type="checkbox" name="45" value="5">
                                5 respuestas
                            </label>
                            <label class="radio" ><input type="checkbox" name="46" value="5">
                                "Mundo" al revés.
                            </label>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        {!! Form::label('','Lenguaje') !!}
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Muestre un lápiz y pregunte ¿Qué es?
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="51" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','') !!}
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Muestre un reloj y pregunte ¿Qué es?
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="52" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','') !!}
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Repita lo siguiente: "ni si, ni no, ni pero"
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="53" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','') !!}
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Seguimiento de 3 comandos:
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="54" value="1">
                                Tome el papel con su mano derecha
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="55" value="1">
                                Dóblelo por la mitad
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="56" value="1">
                                Colóquelo en el piso
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','') !!}
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Escriba una frase con sujeto y predicado
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="57" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','') !!}
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Lea y obedezca lo siguiente: Cierre los ojos
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="58" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        {!! Form::label('','') !!}
                        <label style="display: block; font-weight: normal;margin: 0" for="">
                            Dibuje 2 pentágonos cruzados y pida al paciente que los copie tal cual.
                            Seleccione correcto si están presentes los 10 ángulos y la intersección.
                        </label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="59" value="1">
                                Correcto
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>