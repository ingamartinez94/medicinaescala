<div class="modal fade" id="modal_escala_test_mental" tabindex="-1" role="dialog" aria-labelledby="modal_escala_test_mental">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_test_mental_abreviado">Test Mental</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'test-mental/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaTestMental']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_nombre" value="1">
                                    Nombre
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_edad" value="1">
                                    Edad
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_hora" value="1">
                                    Hora
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_tiempo" value="1">
                                    Tiempo del día (Mañana, tarde, noche)
                            </label>
                        </div>

                    </div>
                    <div class="col-md-6 form-group">
                        <label style="display: block; font-weight: normal" for="">
                            De un nombre y una dirección, el paciente debe repetirlos para asegurar que los ha escuchado correctamente
                        </label>
                        <ul>
                            <li>Mr. John Brown (1 o 2)
                                <input type="radio" name="opcion_nombre_direccion1" value="1">1
                                <input type="radio" name="opcion_nombre_direccion1" value="2">2
                            </li>
                            <li>Calle 42 este (1 o 2)
                                <input type="radio" name="opcion_nombre_direccion2" value="1">1
                                <input type="radio" name="opcion_nombre_direccion2" value="2">2
                            </li>
                            <li>Plaza central
                                <input type="radio" name="opcion_nombre_direccion3" value="1">1
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_dia" value="1">
                                Día de la semana
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_fecha_mes" value="1">
                                Fecha (correcto día del mes)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_mes" value="1">
                                Mes
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_anio" value="1">
                                Año
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label style="display: block; font-weight: normal" for="">
                            Lugar donde se encuentra:
                        </label>
                        <ul>
                            <li>Ejemplo: hospital
                                <input type="checkbox" name="check_hospital" value="1">
                            </li>
                            <li>Nombre del hospital
                                <input type="checkbox" name="check_nombre_hospital" value="1">
                            </li>
                            <li>Nombre de la sala
                                <input type="checkbox" name="check_sala" value="1">
                            </li>
                            <li>Nombre del pueblo
                                <input type="checkbox" name="check_pueblo" value="1">
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label style="display: block; font-weight: normal" for="">
                            Reconocimiento de 2 personas
                        </label>
                        <ul>
                            <li>Ejemplo: Doctor, enfermera (1 o 2)
                                <input type="radio" name="opcion_doctor_enfermera" value="1">1
                                <input type="radio" name="opcion_doctor_enfermera" value="2">2
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_fecha_nacimiento" value="1">
                                Fecha de nacimiento (día y mes)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_lugar_nacimiento" value="1">
                                Lugar de nacimiento
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_colegio" value="1">
                                Colegio al que asistió
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_asistir" value="1">
                                Asistió a la escuela
                            </label>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_ocupacion" value="1">
                                Ocupación formal
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_esposa" value="1">
                                Nombre de la esposa, hermano o pariente cercano
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_guerra_p" value="1">
                                Dato de la Primera Guerra Mundial (año)
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_guerra_s" value="1">
                                Dato de la Segunda Guerra Mundial (año)
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_monarca" value="1">
                                Nombre del presente Monarca
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_ministro" value="1">
                                Nombre del Primer Ministro
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_meses_anio" value="1">
                                Meses del año dichos hacia atrás
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Contar del 1 al 20 (1 ó 2)
                            <input type="radio" name="opcion_contar" value="1">1
                            <input type="radio" name="opcion_contar" value="2">2
                        </label>
                        <label style="display: block; font-weight: normal" for="">
                            Contar del 20 al 1 (1 ó 2)
                            <input type="radio" name="opcion_contar_rever" value="1">1
                            <input type="radio" name="opcion_contar_rever" value="2">2
                        </label>
                    </div>

                </div>

                <div class="row">


                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaCincinnati').validate({
        rules: {
            opcion_nombre_direccion1: {
                required: true
            },
            opcion_nombre_direccion2: {
                required: true
            },
            opcion_doctor_enfermera: {
                required: true
            },
            opcion_contar: {
                required: true
            },
            opcion_contar_rever: {
                required: true
            }
        }
    });

</script>
@endpush