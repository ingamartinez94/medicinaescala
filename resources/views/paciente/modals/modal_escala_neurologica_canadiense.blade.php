<div class="modal fade" id="modal_escala_neurologica_canadiense" tabindex="-1" role="dialog"
     aria-labelledby="modal_escala_neurologica_canadiense">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_neurologica_canadiense">Escala Neurologica Canadiense</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'neuro-canadiense/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaNeurologicaCanadiense']) !!}

                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Nivel de conciencia ') !!}
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_nivel_conciencia"
                                                                   value="3">Alerta</label>
                                <label class="radio-inline"><input type="radio" name="opcion_nivel_conciencia"
                                                                   value="1.5">Somnoliento</label>
                            </label>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Orientacion') !!}
                        <label style="display: block; font-weight: normal" for="">
                            El paciente está orientado en tiempo (mes y año) y en espacio (ciudad y hospital). Se admite
                            el mes anterior en los tres primeros días del mes como respuesta correcta
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_orientacion" value="1">Orientado</label>
                                <label class="radio-inline"><input type="radio" name="opcion_orientacion" value="0">Desorientado</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Habla') !!}
                        <label style="display: block; font-weight: normal" for="">
                            El paciente obedece las 3 órdenes siguientes:
                        </label>
                        <ul>
                            <li>"Cierre los ojos"
                            </li>
                            <li>"Señale el Techo"
                            </li>
                            <li>"¿Se hunde una piedra en el agua?"
                            </li>
                        </ul>
                        <label style="display: block; font-weight: normal" for="">
                            Si obedece 2 o menos ordenes, se anotará la puntuación de “déficit de comprensión” y se
                            procederá a examinar la función motora.
                        </label>
                        <label style="display: block; font-weight: normal" for="">
                            Serán necesarios un bolígrafo, una llave y un reloj.
                        </label>
                        <ul>
                            <li>Pedir al paciente que nombre cada objeto
                                <ul>
                                    <li>
                                        Si nombra 2 o menos, se anotara la puntuación de "déficit de comprensión".
                                    </li>
                                </ul>
                            </li>
                            <li>Preguntar al paciente para qué sirve el bolígrafo, la llave y el reloj.
                                <ul>
                                    <li>
                                        Si responde las 3 se anotara la puntuación "habla normal".
                                    </li>
                                    <li>
                                        o Si responde 2 o menos se puntuará como "déficit de expresión".
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_habla"
                                                                   value="1">Normal</label>
                                <label class="radio-inline"><input type="radio" name="opcion_habla" value="0.5">Déficit
                                    de expresión </label>
                                <label class="radio-inline"><input type="radio" name="opcion_habla" value="0">Déficit de
                                    comprensión</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','FUNCIONES MOTORAS(SIN PROBLEMAS EN LA COMPRENSIÓN VERBAL)') !!}
                        <label style="display: block; font-weight: 700" for="">
                            Cara:
                        </label>
                        <label style="display: block; font-weight: normal" for="">
                            Invitar al enfermo a enseñar las encías.
                        </label>
                        <ul>
                            <li>No paresia: no existe asimetría en las comisuras bucales.
                            </li>
                            <li>Paresia: asimetría facial.
                            </li>
                        </ul>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_fun_motora_sin_cara"
                                                                   value="0.5">No debilidad facial</label>
                                <label class="radio-inline"><input type="radio" name="opcion_fun_motora_sin_cara"
                                                                   value="0">Debilidad facial</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Extremidad superior porción proximal.') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Se invita al paciente a abducir los brazos a 90°, si estuviera en cama, de 45 a 90°, se
                            examinarán ambas extremidades al tiempo, aplicando resistencia en la mitad del brazo.
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior"
                                                                   value="1.5">No debilidad</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior"
                                                                   value="1">Paresia 3-4/5</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior"
                                                                   value="0.5">Paresia 2/5</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior"
                                                                   value="0">Fuerza a 0-1/5</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Extremidad superior porción distal.') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Se evaluará estando sentado o tumbado con los brazos elevados, indicando al enfermo que
                            cierre los puños y extienda las muñecas. Si la extensión es simétrica, se procederá a
                            examinar la fuerza muscular en las muñecas, estabilizando firmemente el brazo del paciente.
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior_distal"
                                                                   value="1.5">No debilidad</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior_distal"
                                                                   value="1">Paresia 3-4/5</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior_distal"
                                                                   value="0.5">Paresia 2/5</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_superior_distal"
                                                                   value="0">Fuerza a 0-1/5</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Extremidad inferior.') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Tendido en cama, la puntuación debe ser la correspondiente al déficit más acusado con
                            cualquiera de las siguientes maniobras:
                        </label>
                        <ul>
                            <li>Rodillas a 90°, indicar al paciente que flexione el muslo sobre el abdomen.
                            </li>
                            <li>Ordenar al paciente que flexione el pie y los dedos dorsalmente
                            </li>
                        </ul>
                        <label style="display: block; font-weight: normal" for="">
                            Se aplica resistencia alternativamente en muslo y pie, y se evalúa la amplitud del
                            movimiento.
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_inferior"
                                                                   value="1.5">No debilidad</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_inferior"
                                                                   value="1">Paresia 3-4/5</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_inferior"
                                                                   value="0.5">Paresia 2/5</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_sin_extremidad_inferior"
                                                                   value="0">Fuerza a 0-1/5</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','FUNCIONES MOTORAS(CON PROBLEMAS EN LA COMPRENSIÓN VERBAL)') !!}
                        <label style="display: block; font-weight: 700" for="">
                            Cara:
                        </label>
                        <label style="display: block; font-weight: normal" for="">
                            Invitar al paciente a imitar nuestro movimiento facial o respuesta a estímulos nociceptivos
                            con mueca facial simétrica o asimétrica.
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio" name="opcion_fun_motora_con_cara"
                                                                   value="0.5">Simétrica</label>
                                <label class="radio-inline"><input type="radio" name="opcion_fun_motora_con_cara"
                                                                   value="0">Asimétrica</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Extremidades superiores.') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Colocar los brazos extendidos a 90° por delante del paciente manteniendo la posición por 3 a
                            5 segundos o las retira de forma simétrica ante estímulos dolorosos. En caso de asimetría el
                            retiro o la posición es diferente al momento de comparar las extremidades.
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_con_extremidad_superior"
                                                                   value="1.5">Iguales</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_con_extremidad_superior"
                                                                   value="0">Desiguales</label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('label','Extremidades inferiores.') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Flexión de las caderas con las rodillas flexionadas a 90° manteniendo la posición por 3 a 5
                            segundos o las retira de forma simétrica ante estímulos dolorosos. En caso de asimetría el
                            retiro o la posición es diferente al momento de comparar las extremidades.
                        </label>
                        <div class="radio">
                            <label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_con_extremidad_inferior"
                                                                   value="1.5">Iguales</label>
                                <label class="radio-inline"><input type="radio"
                                                                   name="opcion_fun_motora_con_extremidad_inferior"
                                                                   value="0">Desiguales</label>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaNeurologicaCanadiense').validate({
        rules: {
            opcion_nivel_conciencia: {
                required: true
            },
            opcion_orientacion: {
                required: true
            },
            opcion_habla: {
                required: true
            },
            opcion_fun_motora_sin_cara: {
                required: true
            },
            opcion_fun_motora_sin_extremidad_superior: {
                required: true
            },
            opcion_fun_motora_sin_extremidad_superior_distal: {
                required: true
            },
            opcion_fun_motora_sin_extremidad_inferior: {
                required: true
            },
            opcion_fun_motora_con_cara: {
                required: true
            },
            opcion_fun_motora_con_extremidad_superior: {
                required: true
            },
            opcion_fun_motora_con_extremidad_inferior: {
                required: true
            },
        }
    });

</script>
@endpush