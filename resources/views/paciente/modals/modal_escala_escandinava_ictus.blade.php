<div class="modal fade" id="modal_escala_escandinava_ictus" tabindex="-1" role="dialog" aria-labelledby="modal_escala_escandinava_ictus">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_escandinava_ictus">Escala Escandinava Ictus</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'escandinava-ictus/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaEscandinavaIctus']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Conciencia') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_conciencia" value="6">
                                Totalmente consciente
                            </label>
                            <label class="radio"><input type="radio" name="opcion_conciencia" value="4">
                                Somnoliento, despierta consciente
                            </label>
                            <label class="radio"><input type="radio" name="opcion_conciencia" value="2">
                                Responde órdenes verbales, pero no está completamente consciente
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Movimientos Oculares') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_mov_ocular" value="4">
                                Sin parálisis de la mirada
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mov_ocular" value="2">
                                Parálisis de la mirada presente
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mov_ocular" value="0">
                                Desviación conjugada de la mirada
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Fuerza Muscular en el brazo') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_brazo" value="6">
                                Lo eleva con fuerza normal.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_brazo" value="5">
                                Lo eleva con fuerza reducida.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_brazo" value="4">
                                Lo eleva con el codo flexionado.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_brazo" value="2">
                                Lo mueve, pero no contra la gravedad.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_brazo" value="0">
                                Parálisis.
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Fuerza Muscular en la mano') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_mano" value="6">
                                Conservada.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_mano" value="4">
                                Reducida.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_mano" value="2">
                                Algunos movimientos, los dedos no llegan a tocar la palma de las manos.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_mano" value="0">
                                Parálisis.
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Fuerza Muscular en la pierna') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_pierna" value="6">
                                La eleva con fuerza normal.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_pierna" value="5">
                                La eleva con fuerza reducida.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_pierna" value="4">
                                La eleva con rodilla flexionada.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_pierna" value="2">
                                La mueve, pero no contra la gravedad.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_mov_musc_pierna" value="0">
                                Parálisis.
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('','Orientación') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_orientacion" value="6">
                                Correcta para el tiempo, espacio y persona.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_orientacion" value="4">
                                Dos de esas.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_orientacion" value="2">
                                Una de esas.
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_orientacion" value="0">
                                Completamente desorientado
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','Lenguaje') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_lenguaje" value="10">
                                Sin afasia
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_lenguaje" value="6">
                                Vocabulario limitado o incoherente
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_lenguaje" value="3">
                                Más que SI o NO, pero no oraciones completas
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_lenguaje" value="0">
                                Solo SI o NO o menos
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        {!! Form::label('','Marcha') !!}
                        <div class="radio">
                            <label class="radio" ><input type="radio" name="opcion_marcha" value="12">
                                Camina 5 metros sin ayuda
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_marcha" value="9">
                                Camina sin apoyo
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_marcha" value="6">
                                Camina con ayuda de otra persona
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_marcha" value="3">
                                Se sienta, sin apoyarse
                            </label>
                            <label class="radio" ><input type="radio" name="opcion_marcha" value="0">
                                Encamado, silla de ruedas
                            </label>
                        </div>
                    </div>
                </div>



            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>


@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaEscandinavaIctus').validate({
        rules: {

            opcion_conciencia: {
                required: true
            },
            opcion_mov_ocular: {
                required: true
            },
            opcion_mov_musc_brazo: {
                required: true
            },
            opcion_mov_musc_mano: {
                required: true
            },
            opcion_mov_musc_pierna: {
                required: true
            },
            opcion_orientacion: {
                required: true
            },
            opcion_lenguaje: {
                required: true
            },
            opcion_marcha: {
                required: true
            },
        }
    });

</script>
@endpush