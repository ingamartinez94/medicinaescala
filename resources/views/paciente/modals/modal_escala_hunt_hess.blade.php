<div class="modal fade" id="modal_escala_hunt_hess" tabindex="-1" role="dialog" aria-labelledby="modal_escala_hunt_hess">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_hunt_hess">Escala de Hunt y Hess para la hemorragia subaracnoidea</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'hunt-hess/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaHuntHess']) !!}
                <table id="table-rankin" class="table table-striped table-bordered dataTable" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Grado</th>
                        <th>Clínica</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>I</td>
                        <td>Cefalea mínima, situación neurológica normal.</td>
                    </tr>

                    <tr>
                        <td>II</td>
                        <td>Cefalea moderada, meningismo. Ausencia de déficit neurológico salvo acaso el referido a un nervio craneal.</td>
                    </tr>

                    <tr>
                        <td>III</td>
                        <td>Confusión persistente o déficit focal neurológico.</td>
                    </tr>

                    <tr>
                        <td>IV</td>
                        <td>Estupor.</td>
                    </tr>

                    <tr>
                        <td>V</td>
                        <td>Coma.</td>
                    </tr>

                    </tbody>
                </table>
                <div class="form-group">
                    <label>Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="2"></textarea>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaHuntHess').validate({
        rules: {
            observaciones: {
                required: true
            }
        }
    });

</script>
@endpush