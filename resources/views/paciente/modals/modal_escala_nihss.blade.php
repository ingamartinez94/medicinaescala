<div class="modal fade bs-example-modal-lg" id="modal_escala_nihss" tabindex="-1" role="dialog" aria-labelledby="modal_escala_nihss">
    <div class="modal-dialog modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_cincinnati">Escala del Instituto Nacional de Salud</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'nihss/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaNihss']) !!}
                {{--1a. Nivel de conciencia--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            El investigador debe elegir una respuesta, aunque obstáculos como la intubación, barreras lingüísticas o traumas/vendajes orotraqueales impidan una evaluación completa.
                            Se puntúa un 3 sólo si el paciente no hace ningún movimiento (a excepción de posturas reflejas) en respuesta a la estimulación dolorosa.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_nivel_conciencia" value="0">
                                Alerta: Nivel de conciencia normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_nivel_conciencia" value="1">
                                Somnoliento: Se despierta y responde a estímulos verbales.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_nivel_conciencia" value="2">
                                Estuporoso: Se despierta y responde sólo frente a estímulos dolorosos o repetitivos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_nivel_conciencia" value="3">
                                Responde sólo con reflejos motores, efectos autonómicos o no responde en absoluto, flácido, arrefléctico.
                            </label>
                        </div>

                    </div>
                </div>
                {{--1b. Preguntas LOC --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Se le pregunta al paciente el mes y su edad. La respuesta debe ser correcta; no hay puntuación parcial por aproximarse.
                            Los pacientes afásicos y estuporosos que no comprenden las preguntas puntuarán 2.
                            A los pacientes incapaces de hablar que no sea debida por afasias se les da un 1.
                            Sólo la respuesta inicial sea valorada.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_preguntas_LOC" value="0">
                                Responde ambas preguntas correctamente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_preguntas_LOC" value="1">
                                Responde una pregunta correctamente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_preguntas_LOC" value="2">
                                No responde ninguna pregunta correctamente.
                            </label>
                        </div>
                    </div>
                </div>
                {{--1c. Órdenes LOC --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Se le pide al paciente que abra y cierre los ojos y luego que apriete y afloje la mano no parética.
                            Sustituirlo por otra orden sencilla si no puede utilizar las manos.
                            Puntúa si hace el intento, pero no lo completa por debilidad.
                            Si el paciente no responde a la orden se le muestra la tarea a realizar (pantomima) y se puntúa al resultado (sigue dos, una o ninguna orden). A los pacientes con traumatismos, amputación u otro impedimento físico se les darán órdenes sencillas que se adapten a su situación.
                            Se puntúa sólo el primer intento.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_ordenes_LOC" value="0">
                                Realiza ambas tareas correctamente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ordenes_LOC" value="1">
                                Realiza una tarea correctamente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ordenes_LOC" value="2">
                                No realiza ninguna tarea correctamente.
                            </label>
                        </div>
                    </div>
                </div>
                {{--2. Mirada --}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Sólo se explorarán movimientos horizontales.
                            Se puntúan movimientos oculares voluntarios o reflejos (oculocefálicos), pero no se realizarán pruebas calóricas.
                            Si el paciente tiene una desviación conjugada de la mirada que puede ser vencida por actividad voluntaria o refleja la puntuación será 1.
                            Si el paciente tiene una paresia periférica aislada (nervio craneal III, IV o VI) la puntuación será 1.
                            A los pacientes con traumatismo ocular, vendajes, ceguera preexistente u otra alteración de la agudeza visual o de los campos visuales se les explorará con movimientos reflejos y según la preferencia del investigador.
                            Establecer contacto ocular y moverse de un lado a otro del paciente clarificará ocasionalmente la presencia de una parálisis parcial de la mirada.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_mirada" value="0">
                                Normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mirada" value="1">
                                Parálisis parcial de la mirada. No hay presencia de desviación oculocefálica o la parálisis total de la mirada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_mirada" value="2">
                                Desviación oculocefálica o parálisis total de la mirada que no vencen con maniobras oculocefálicas.
                            </label>
                        </div>
                    </div>
                </div>
                {{--3. Visión--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Los campos visuales (cuadrantes superiores e inferiores) se exploran por confrontación, utilizando contaje de dedos o amenaza visual según se considere adecuado.
                            El paciente debe hacerlo de forma correcta, pero si mira claramente al lado en que se mueven los dedos se puntúa como normal.
                            Si existe ceguera o enucleación unilateral se puntúan los campos visuales en el otro ojo.
                            Se puntúa 1 si se encuentra una nítida asimetría o una cuadrantanopsia. Si el paciente es ciego por cualquier causa se puntúa 3.
                            En este punto se realiza una doble estimulación simultánea; si hay extinción el paciente recibe un 1 y los resultados se utilizan para el apartado 11.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_vision" value="0">
                                Sin déficits campimétricos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vision" value="1">
                                Hemianopsia parcial, cuadrantanopsia.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vision" value="2">
                                Hemianopsia homónima.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_vision" value="3">
                                Hemianopsia homónima bilateral, ceguera (incluyendo cortical).
                            </label>
                        </div>
                    </div>
                </div>
                {{--4. Parálisis facial--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Pedir al paciente que enseñe los dientes, levante las cejas y cierre los ojos (verbalmente o utilizando pantomima).
                            Puntuar la simetría de la mueca a estímulos en pacientes poco reactivos o que no comprenden.
                            Si la cara está tapada por vendajes, tubos orotraqueales u otras barreras físicas se deberían retirar en la medida de lo posible.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_paralisis_facial" value="0">
                                Movimientos normales y simétricos.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_paralisis_facial" value="1">
                                Paresia ligera (borramiento del surco naso-labial, asimetría al sonreír).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_paralisis_facial" value="2">
                                Parálisis parcial (parálisis total o casi total de la cara inferior).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_paralisis_facial" value="3">
                                Parálisis completa de uno o ambos lados /ausencia de movimientos faciales (en cara inferior y superior.)
                            </label>
                        </div>
                    </div>
                </div>
                {{--5a. Motor – Brazo Izquierdo--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Brazo Izquierdo. La extremidad se sitúa en la posición correcta: extender los brazos (con las palmas hacia abajo) 90° si el paciente está sentado y 45° si está en decúbito supino. Claudica si el brazo cae antes de 10 segundos.
                            Al paciente afásico se le insiste verbalmente y con pantomima, pero no con estímulos dolorosos.
                            Exploramos cada extremidad empezando por el brazo no parético.
                            Sólo en caso de amputación o fusión articular en hombro o cadera se puntúa 9 y el examinador debe escribir claramente la explicación de por qué puntúa 9.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_brazo_izquierdo" value="0">
                                No claudica. Fuerza: 5.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_izquierdo" value="1">
                                Claudica antes de 10 segundos. Fuerza: 4.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_izquierdo" value="2">
                                Algún esfuerzo contra la gravedad. Fuerza: 3.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_izquierdo" value="3">
                                No hace esfuerzo contra la gravedad. Fuerza: 2-1
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_izquierdo" value="4">
                                Ningún movimiento. Fuerza: 0.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_izquierdo" value="9">
                                Amputación, fusión articular.
                            </label>
                        </div>
                    </div>
                </div>
                {{--5b. Motor – Brazo Derecho--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Brazo Derecho. La extremidad se sitúa en la posición correcta: extender los brazos (con las palmas hacia abajo) 90° si el paciente está sentado y 45° si está en decúbito supino. Claudica si el brazo cae antes 1de 10 segundos.
                            Al paciente afásico se le insiste verbalmente y con pantomima, pero no con estímulos dolorosos.
                            Exploramos cada extremidad empezando por el brazo no parético.
                            Sólo en caso de amputación o fusión articular en hombro o cadera se puntúa 9 y el examinador debe escribir claramente la explicación de por qué puntúa 9.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_brazo_derecho" value="0">
                                No claudica. Fuerza: 5.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_derecho" value="1">
                                Claudica antes de 10 segundos. Fuerza: 4.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_derecho" value="2">
                                Algún esfuerzo contra la gravedad. Fuerza: 3.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_derecho" value="3">
                                No hace esfuerzo contra la gravedad. Fuerza: 2-1
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_derecho" value="4">
                                Ningún movimiento. Fuerza: 0.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_brazo_derecho" value="9">
                                Amputación, fusión articular.
                            </label>
                        </div>
                    </div>
                </div>
                {{--6a. Motor – Pierna Izquierda--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Pierna Izquierda. La extremidad a 30° (siempre explorado en decúbito). Claudica si cae antes de 5 segundos.
                            Al paciente afásico se le insiste verbalmente y con pantomima, pero no con estímulos dolorosos.
                            Exploramos cada extremidad empezando por la pierna no parética.
                            Sólo en caso de amputación o fusión articular en hombro o cadera se puntúa 9 y el examinador debe escribir claramente la explicación de por qué puntúa 9.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_pierna_izquierdo" value="0">
                                No claudica. Fuerza: 5.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pierna_izquierdo" value="1">
                                Claudica antes de 10 segundos. Fuerza: 4.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pierna_izquierdo" value="2">
                                Algún esfuerzo contra la gravedad. Fuerza: 3.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pierna_izquierdo" value="3">
                                No hace esfuerzo contra la gravedad. Fuerza: 2-1
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pierna_izquierdo" value="4">
                                Ningún movimiento. Fuerza: 0.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_pierna_izquierdo" value="9">
                                Amputación, fusión articular.
                            </label>
                        </div>
                    </div>
                </div>
                {{--6b. Motor – Pierna Derecha--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Pierna Derecha. La extremidad a 30° (siempre explorado en decúbito). Claudica si cae antes de 5 segundos.
                            Al paciente afásico se le insiste verbalmente y con pantomima, pero no con estímulos dolorosos.
                            Exploramos cada extremidad empezando por la pierna no parética.
                            Sólo en caso de amputación o fusión articular en hombro o cadera se puntúa 9 y el examinador debe escribir claramente la explicación de por qué puntúa 9.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_piernas_derecha" value="0">
                                No claudica. Fuerza: 5.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_piernas_derecha" value="1">
                                Claudica antes de 10 segundos. Fuerza: 4.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_piernas_derecha" value="2">
                                Algún esfuerzo contra la gravedad. Fuerza: 3.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_piernas_derecha" value="3">
                                No hace esfuerzo contra la gravedad. Fuerza: 2-1
                            </label>
                            <label class="radio"><input type="radio" name="opcion_piernas_derecha" value="4">
                                Ningún movimiento. Fuerza: 0.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_piernas_derecha" value="9">
                                Amputación, fusión articular.
                            </label>
                        </div>
                    </div>
                </div>
                {{--7. Ataxia de miembros--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Este ítem pretende descartar la existencia de una lesión cerebelosa unilateral y se explora con los ojos abiertos.
                            Las pruebas dedo-nariz y talón-rodilla se realizan en ambos lados y se puntúa la ataxia sólo cuando es desproporcionada a la debilidad.
                            No hay ataxia en un paciente que no comprende o está paralizado.
                            Sólo en caso de amputación o fusión articular se puntúa 9, y el examinador debe escribir claramente el porqué.
                            En caso de ceguera explorar tocando la nariz desde la posición de brazo extendido.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_ataxia" value="0">
                                Ausente.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ataxia" value="1">
                                Presente en una extremidad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_ataxia" value="2">
                                Presente en dos extremidades.
                            </label>
                        </div>
                    </div>
                </div>
                {{--8. Sensibilidad--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Valoramos la sensación o muecas ante el pinchazo y la retirada a estímulos dolorosos en el paciente estuporoso o afásico.
                            Sólo puntúa la pérdida de sensibilidad atribuida al ictus y deben explorarse tantas áreas corporales como sean necesarias para confirmar la hipoestesia (brazos [no manos], piernas, tronco, cara).
                            Una puntuación de 2 deberá darse sólo en caso de que una pérdida de sensibilidad severa o total pueda ser claramente demostrada. Por tanto, pacientes estuporosos y afásicos recibirán probablemente una puntuación de 1 a 0.
                            El paciente con un ictus de tronco que presenta una pérdida de sensibilidad bilateral puntuará un 2.
                            Si el paciente no responde y está cuadripléjico puntuará un 2.
                            A los pacientes en coma (ítem 1a = 3) se les da arbitrariamente un 2 en este apartado.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_sensibilidad" value="0">
                                Normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_sensibilidad" value="1">
                                Ligera a moderada disminución de la sensibilidad. El paciente nota que el pinchazo es menos intenso o más apagado en el lado afecto; o hay una pérdida de dolor superficial con el pinchazo, pero el paciente se da cuenta de que está siendo tocado.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_sensibilidad" value="2">
                                Severa a total pérdida de sensibilidad. El paciente no se da cuenta de que está siendo tocado en la cara, brazo y pierna.
                            </label>
                        </div>
                    </div>
                </div>
                {{--9. Lenguaje--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Valorar la comprensión y expresión del lenguaje (fluencia, nominación, repetición y comprensión).
                            Se puede obtener mucha información acerca de la comprensión durante el examen de los apartados previos.
                            A los pacientes intubados se les solicitará que escriban.
                            El paciente en coma (ítem 1a = 3) será puntuado de forma arbitraria con un 3 en este apartado.
                            El examinador debe elegir una puntuación en el paciente estuporoso o poco colaborador, teniendo en cuenta que un 3 se utilizará sólo en el caso de que el paciente presente mutismo o no cumpla ninguna orden sencilla.

                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="0">
                                Normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="1">
                                Afasia ligera a moderada. Errores de nominación, parafasias y/o afectación de la comprensión o expresión. Afasia incompleta. Pérdida obvia de fluencia o de facilidad de comprensión sin una gran limitación en las ideas expresadas o en la forma de expresarlas.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="2">
                                Afasia severa. Afasia de Broca. Afasia de Wernicke. Afasia transcortical. Afasia nominal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_lenguaje" value="3">
                                Afasia global o mutismo.
                            </label>
                        </div>
                    </div>
                </div>
                {{--10. Disartria--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Sólo en caso de que el paciente esté intubado o tenga otras barreras físicas para emitir lenguaje puntuaremos este apartado con un 9, dejando claramente escrita la explicación de por qué puntuamos así.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_disartria" value="0">
                                Normal.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disartria" value="1">
                                Ligera a moderada. Desde pronunciar mal algunas palabras a ser entendido con alguna dificultad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disartria" value="2">
                                Severa o anartria. El lenguaje llega a ser incomprensible en ausencia de afasia (o desproporcionado al grado de ésta) o el paciente está anártrico.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_disartria" value="9">
                                Paciente con barreras físicas para emitir leguaje.
                            </label>
                        </div>
                    </div>
                </div>
                {{--11. Extinción e inatención (negligencia)--}}
                <div class="row">
                    <div class="col-lg-12 form-group">
                        {!! Form::label('','
                            Durante la exploración previa se puede obtener información suficiente para identificar negligencia.
                            Si el paciente tiene un severo déficit visual evitaremos la doble estimulación simultánea y si la cutánea es normal lo puntuaremos como normal.
                            Si el paciente está afásico pero parece atender en ambos lados lo puntuaremos como normal.
                            No reconocer su propia mano u orientarse sólo en un lado del espacio puntúa como 2. Como la anormalidad se puntúa sólo si está presente, no debemos dejar de explorar este apartado.
                        ') !!}
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_negligencia" value="0">
                                Sin anormalidad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_negligencia" value="1">
                                Inatención o extinción visual, táctil, auditiva, espacial o personal. Sólo afecta a una modalidad.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_negligencia" value="2">
                                Negligencia o extinción visual, táctil, auditiva, especial o personal. Afecta a más de una modalidad.
                            </label>

                        </div>
                    </div>
                </div>
                {{--11. Extinción e inatención (negligencia)--}}
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        Laminas utilizadas para la exploración del lenguaje, del habla y de la extinción visual
                        ') !!}
                        <img src="{{ URL::asset('img/nihss-left.png')}}" class="img-responsive" alt="Responsive image">
                        <ul>
                            <li>Mamá
                            </li>
                            <li>Tic – Tac
                            </li>
                            <li>Cinco – Cinco
                            </li>
                            <li>Gracias
                            </li>
                            <li>Mermelada
                            </li>
                            <li>Futbolista
                            </li>
                            <li>Excavadora
                            </li>

                        </ul>

                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        Laminas utilizadas para la exploración del lenguaje, del habla y de la extinción visual
                        ') !!}
                        <img src="{{ URL::asset('img/nihss-right.png')}}" class="img-responsive" alt="Responsive image">
                        <ul>
                            <li>Ya lo veo.
                            </li>
                            <li>Baja a la calle.
                            </li>
                            <li>Volví del trabajo a casa.
                            </li>
                            <li>Está junto a la mesa del comedor.
                            </li>
                            <li>Anoche oyeron al ministro hablar por la radio.
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="modal-footer">

            <button type="submit" class="btn btn-info">Finalizar Escala</button>

            {!! Form::close() !!}
            {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>


@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaNihss').validate({
        rules: {
            opcion_nivel_conciencia: {
                required: true
            },
            opcion_preguntas_LOC: {
                required: true
            },
            opcion_ordenes_LOC: {
                required: true
            },
            opcion_mirada: {
                required: true
            },
            opcion_vision: {
                required: true
            },
            opcion_paralisis_facial: {
                required: true
            },
            opcion_brazo_izquierdo: {
                required: true
            },
            opcion_brazo_derecho: {
                required: true
            },
            opcion_pierna_izquierdo: {
                required: true
            },
            opcion_piernas_derecha: {
                required: true
            },
            opcion_ataxia: {
                required: true
            },
            opcion_sensibilidad: {
                required: true
            },
            opcion_lenguaje: {
                required: true
            },
            opcion_disartria: {
                required: true
            },
            opcion_negligencia: {
                required: true
            }
        }
    });

</script>
@endpush