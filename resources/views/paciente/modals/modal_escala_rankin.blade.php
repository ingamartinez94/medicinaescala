<div class="modal fade" id="modal_escala_rankin" tabindex="-1" role="dialog" aria-labelledby="modal_escala_rankin">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_rankin">Escala de Rankin modificada</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'rankin/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaRankin']) !!}
                <table id="table-rankin" class="table table-striped table-bordered dataTable" cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Grado</th>
                        <th>Clínica</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>0</td>
                        <td>Ausencia de síntomas.</td>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Síntomas leves que no interfieren con las actividades habituales del paciente.</td>
                    </tr>

                    <tr>
                        <td>2</td>
                        <td>AIncapacidad leve. Síntomas que no imponen alguna restricción en las actividades del
                            paciente, pero no impiden que sea capaz de cuidarse por sí mismo.
                        </td>
                    </tr>

                    <tr>
                        <td>3</td>
                        <td>Incapacidad moderada. Síntomas que restringen de forma significativa las actividades
                            habituales del paciente y le impiden una existencia completamente independiente.
                        </td>
                    </tr>

                    <tr>
                        <td>4</td>
                        <td>Incapacidad moderada-severa. Imposibilidad para una existencia independiente, pero no
                            requiere atención constante.
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Incapacidad severa. Requiere atención constante noche y día.</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Muerte.</td>
                    </tr>

                    </tbody>
                </table>
                <div class="form-group">
                    <label>Observaciones</label>
                    <textarea class="form-control" name="observaciones" rows="2"></textarea>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>


@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertBefore(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaRankin').validate({
        rules: {
            observaciones: {
                required: true
            }

        }
    });

</script>
@endpush