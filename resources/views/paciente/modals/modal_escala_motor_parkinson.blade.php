<div class="modal fade bs-example-modal-lg" id="modal_escala_motor_parkinson" tabindex="-1" role="dialog" aria-labelledby="modal_escala_motor_parkinson">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_escala_motor_parkinson">Escala de evaluación de síntomas no motores en la enfermedad de Parkinson</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['url'=>'valoracion-parkinson/'.$id,'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEscalaParkinson']) !!}

                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        1.  ¿Nota el paciente mareo, aturdimiento o debilidad al ponerse de pie después de haber estado sentado o tumbado?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p1" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p1" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p1" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p1" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p1" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p1" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p1" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p1" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        2. ¿Se cae el paciente por desmayo o pérdida de conocimiento?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p2" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p2" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p2" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p2" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p2" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p2" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p2" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p2" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        3. ¿Se queda el paciente adormilado o se duerme sin querer durante las actividades diurnas?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p3" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p3" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p3" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p3" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p3" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p3" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p3" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p3" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        4. ¿Limitan la fatiga (cansancio) o falta de energía (no el enlentecimiento) las actividades diurnas del paciente?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p4" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p4" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p4" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p4" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p4" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p4" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p4" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p4" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        5. ¿Tiene dificultad el paciente para dormirse o permanecer dormido?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p5" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p5" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p5" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p5" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p5" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p5" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p5" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p5" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        6. Cuando el paciente está inactivo, bien sea sentado o tumbado, ¿siente la necesidad de mover las piernas o siente inquietud en las piernas que mejora con el movimiento?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p6" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p6" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p6" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p6" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p6" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p6" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p6" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p6" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        7. ¿Ha perdido interés el paciente por lo que le rodea?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p7" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p7" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p7" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p7" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p7" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p7" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p7" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p7" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        8. ¿Ha perdido interés el paciente en realizar actividades o le falta motivación para empezar nuevas actividades?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p8" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p8" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p8" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p8" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p8" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p8" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p8" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p8" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        9. ¿Se siente el paciente nervioso, preocupado o asustado sin razón aparente?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p9" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p9" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p9" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p9" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p9" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p9" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p9" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p9" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        10. ¿Parece el paciente triste o deprimido o ha referido tener tales sentimientos?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p10" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p10" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p10" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p10" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p10" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p10" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p10" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p10" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        11. ¿Tiene el paciente un estado de ánimo aplanado, sin los altibajos normales?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p11" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p11" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p11" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p11" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p11" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p11" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p11" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p11" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        12. ¿Tiene el paciente dificultad para sentir placer con sus actividades habituales o refiere que no son placenteras?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p12" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p12" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p12" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p12" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p12" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p12" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p12" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p12" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        13. ¿Refiere el paciente ver cosas que no están?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p13" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p13" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p13" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p13" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p13" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p13" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p13" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p13" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        14. ¿Cree el paciente cosas que no son verdad? (Por ejemplo, sobre intención de daño, robo o infidelidad.)
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p14" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p14" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p14" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p14" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p14" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p14" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p14" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p14" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        15. ¿Ve doble el paciente? (2 objetos reales separados; no visión borrosa).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p15" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p15" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p15" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p15" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p15" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p15" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p15" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p15" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        16. ¿Tiene el paciente problemas para mantener la concentración mientras realiza actividades? (Por ejemplo, lectura o conversación).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p16" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p16" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p16" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p16" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p16" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p16" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p16" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p16" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        17. ¿Olvida el paciente cosas que le han dicho hace poco o hechos que ocurrieron hace pocos días?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p17" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p17" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p17" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p17" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p17" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p17" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p17" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p17" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        18. ¿Se olvida el paciente de hacer cosas? (Por ejemplo, tomar sus pastillas o apagar electrodomésticos).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p18" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p18" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p18" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p18" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p18" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p18" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p18" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p18" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        19. ¿Babea el paciente durante el día?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p19" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p19" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p19" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p19" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p19" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p19" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p19" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p19" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        20. ¿Tiene el paciente dificultad para tragar?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p20" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p20" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p20" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p20" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p20" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p20" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p20" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p20" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        21. ¿Sufre el paciente estreñimiento? (Defecar menos de tres veces a la semana).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p21" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p21" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p21" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p21" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p21" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p21" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p21" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p21" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        22. ¿Tiene dificultad el paciente para retener la orina? (Urgencia)
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p22" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p22" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p22" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p22" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p22" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p22" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p22" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p22" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        23. ¿Tiene que orinar el paciente en el transcurso de 2 horas desde la última vez? (Frecuencia)
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p23" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p23" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p23" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p23" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        24. ¿Tiene que levantarse el paciente habitualmente a orinar por la noche? (Nocturia)
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p24" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p24" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p24" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p24" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p24" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p24" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p24" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p24" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        25. ¿Se ha alterado el interés del paciente por el sexo? (Muy aumentado o disminuido – por favor, subrayar).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p25" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p25" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p25" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p25" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p25" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p25" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p25" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p25" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        26. ¿Tiene problemas el paciente para mantener relaciones sexuales?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p26" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p26" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p26" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p26" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p26" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p26" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p26" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p26" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        27. ¿Sufre el paciente dolor no explicable por otros padecimientos? (¿Está relacionado con la toma de medicamentos y se alivia con los fármacos antiparkinsonianos?)
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p27" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p27" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p27" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p27" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p27" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p27" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p27" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p27" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        28. ¿Refiere el paciente algún cambio en su capacidad para percibir sabores u olores?
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p28" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p28" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p28" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p28" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p28" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p28" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p28" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p28" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        29. ¿Refiere el paciente algún cambio reciente en su peso? (No relacionado con hacer dieta).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p29" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p29" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p29" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p29" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p29" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p29" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p29" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p29" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        {!! Form::label('','
                        30. ¿Suda el paciente excesivamente? (Sin relación con un ambiente caluroso).
                        ') !!}
                        <label style="display: block; font-weight: normal" for="">
                            Gravedad:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_gravedad_p30" value="0">
                                Ninguna
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p30" value="1">
                                Leve
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p30" value="2">
                                Moderada.
                            </label>
                            <label class="radio"><input type="radio" name="opcion_gravedad_p30" value="3">
                                Grave.
                            </label>
                        </div>
                        <label style="display: block; font-weight: normal" for="">
                            Frecuencia:
                        </label>
                        <div class="radio">
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p30" value="1">
                                Raramente (<1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p30" value="2">
                                Ocasional (1 vez/sem).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p30" value="3">
                                Frecuente (varias veces por semana).
                            </label>
                            <label class="radio"><input type="radio" name="opcion_frecuencia_p30" value="4">
                                Muy frecuente (diariamente o continua).
                            </label>
                        </div>
                    </div>
                </div>




            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Escala</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalEscalaParkinson').validate({
        rules: {
            opcion_gravedad_p1: {
                required: true
            },
            opcion_frecuencia_p1: {
                required: true
            },
            opcion_gravedad_p2: {
                required: true
            },
            opcion_frecuencia_p2: {
                required: true
            },
            opcion_gravedad_p3: {
                required: true
            },
            opcion_frecuencia_p3: {
                required: true
            },
            opcion_gravedad_p4: {
                required: true
            },
            opcion_frecuencia_p4: {
                required: true
            },
            opcion_gravedad_p5: {
                required: true
            },
            opcion_frecuencia_p5: {
                required: true
            },
            opcion_gravedad_p6: {
                required: true
            },
            opcion_frecuencia_p6: {
                required: true
            },
            opcion_gravedad_p7: {
                required: true
            },
            opcion_frecuencia_p7: {
                required: true
            },
            opcion_gravedad_p8: {
                required: true
            },
            opcion_frecuencia_p8: {
                required: true
            },
            opcion_gravedad_p9: {
                required: true
            },
            opcion_frecuencia_p9: {
                required: true
            },
            opcion_gravedad_p10: {
                required: true
            },
            opcion_frecuencia_p10: {
                required: true
            },
            opcion_gravedad_p11: {
                required: true
            },
            opcion_frecuencia_p11: {
                required: true
            },
            opcion_gravedad_p12: {
                required: true
            },
            opcion_frecuencia_p12: {
                required: true
            },
            opcion_gravedad_p13: {
                required: true
            },
            opcion_frecuencia_p13: {
                required: true
            },
            opcion_gravedad_p14: {
                required: true
            },
            opcion_frecuencia_p14: {
                required: true
            },
            opcion_gravedad_p15: {
                required: true
            },
            opcion_frecuencia_p15: {
                required: true
            },
            opcion_gravedad_p16: {
                required: true
            },
            opcion_frecuencia_p16: {
                required: true
            },
            opcion_gravedad_p17: {
                required: true
            },
            opcion_frecuencia_p17: {
                required: true
            },
            opcion_gravedad_p18: {
                required: true
            },
            opcion_frecuencia_p18: {
                required: true
            },
            opcion_gravedad_p19: {
                required: true
            },
            opcion_frecuencia_p19: {
                required: true
            },
            opcion_gravedad_p20: {
                required: true
            },
            opcion_frecuencia_p20: {
                required: true
            },
            opcion_gravedad_p21: {
                required: true
            },
            opcion_frecuencia_p21: {
                required: true
            },
            opcion_gravedad_p22: {
                required: true
            },
            opcion_frecuencia_p22: {
                required: true
            },
            opcion_gravedad_p23: {
                required: true
            },
            opcion_frecuencia_p23: {
                required: true
            },
            opcion_gravedad_p24: {
                required: true
            },
            opcion_frecuencia_p24: {
                required: true
            },
            opcion_gravedad_p25: {
                required: true
            },
            opcion_frecuencia_p25: {
                required: true
            },
            opcion_gravedad_p26: {
                required: true
            },
            opcion_frecuencia_p26: {
                required: true
            },
            opcion_gravedad_p27: {
                required: true
            },
            opcion_frecuencia_p27: {
                required: true
            },
            opcion_gravedad_p28: {
                required: true
            },
            opcion_frecuencia_p28: {
                required: true
            },
            opcion_gravedad_p29: {
                required: true
            },
            opcion_frecuencia_p29: {
                required: true
            },
            opcion_gravedad_p30: {
                required: true
            },
            opcion_frecuencia_p30: {
                required: true
            },

        }
    });

</script>
@endpush