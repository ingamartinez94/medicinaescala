@extends('layouts.app')



@if(Auth::guard('web_medico')->check())

@section('opciones')


    @forelse($valoraciones as $valoracion)

        @if($valoracion->escala == "cincinnati")
            <li><a href="{{route('grafico.cincinnati',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Cincinnati</a>
            </li>
        @endif

        @if($valoracion->escala == "barthel")
            <li><a href="{{route('grafico.barthel',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Índice de Barthel</a>
            </li>
        @endif

        @if($valoracion->escala == "canadiense")
            <li><a href="{{route('grafico.canadiense',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Neurológica Canadiense</a>
            </li>
        @endif

        @if($valoracion->escala == "escandinava")
            <li><a href="{{route('grafico.escandinava',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escandinava para el ictus</a>
            </li>
        @endif

        @if($valoracion->escala == "nihss")
            <li><a href="{{route('grafico.nihss',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Instituto Nacional de Salud (NIHSS)</a>
            </li>
        @endif

        @if($valoracion->escala == "modificado")
            <li><a href="{{route('grafico.rankin-modificado',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Rankin modificada</a>
            </li>
        @endif

        @if($valoracion->escala == "hunt-hess")
            <li><a href="{{route('grafico.hunt-hess',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Hunt y Hess para la hemorragia subaracnoidea</a>
            </li>
        @endif

        @if($valoracion->escala == "mini-mental")
            <li><a href="{{route('grafico.mini-mental',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Mini-mental Examen</a>
            </li>
        @endif

        @if($valoracion->escala == "test-mental")
            <li><a href="{{route('grafico.test-mental',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental</a>
            </li>
        @endif

        @if($valoracion->escala == "mental-abreviado")
            <li><a href="{{route('grafico.mental-abreviado',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental Abreviado</a>
            </li>
        @endif

        @if($valoracion->escala == "valoracion")
            <li><a href="{{route('grafico.valoracion',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Valoración clínica de la demencia </a>
            </li>
        @endif

        @if($valoracion->escala == "hachinski")
            <li><a href="{{route('grafico.hachinski',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hachinski</a>
            </li>
        @endif

        @if($valoracion->escala == "hoehn-yahr")
            <li><a href="{{route('grafico.hoehn-yahr',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hoehn-Yahr</a>
            </li>
        @endif

        @if($valoracion->escala == "schwab")
            <li><a href="{{route('grafico.schwab',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Schwab</a>
            </li>
        @endif

        @if($valoracion->escala == "evaluacion-parkinson")
            <li><a href="{{route('grafico.evaluacion-parkinson',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Evaluacion-Parkinson</a>
            </li>
        @endif

        @if($valoracion->escala == "updrs")

            <li><a href="{{route('grafico.updrs',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Unified Parkinson's Disease Rating Scale (UPDRS)</a>
            </li>
        @endif


    @empty
        "No tiene resultados"
    @endforelse
@endsection

@elseif(Auth::guard('web_paciente')->check())

@section('opciones')


    @forelse($valoraciones as $valoracion)

        @if($valoracion->escala == "cincinnati")
            <li><a href="{{route('grafico.cincinnati')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escala de Cincinnati</a>
            </li>
        @endif

        @if($valoracion->escala == "barthel")
            <li><a href="{{route('grafico.barthel')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Índice de Barthel</a>
            </li>
        @endif

        @if($valoracion->escala == "canadiense")
            <li><a href="{{route('grafico.canadiense')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Neurológica Canadiense</a>
            </li>
        @endif

        @if($valoracion->escala == "escandinava")
            <li><a href="{{route('grafico.escandinava')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escandinava para el ictus</a>
            </li>
        @endif

        @if($valoracion->escala == "nihss")
            <li><a href="{{route('grafico.nihss')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Instituto Nacional de Salud (NIHSS)</a>
            </li>
        @endif

        @if($valoracion->escala == "modificado")
            <li><a href="{{route('grafico.rankin-modificado')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escala de Rankin modificada</a>
            </li>
        @endif

        @if($valoracion->escala == "hunt-hess")
            <li><a href="{{route('grafico.hunt-hess')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Hunt y Hess para la hemorragia subaracnoidea</a>
            </li>
        @endif

        @if($valoracion->escala == "mini-mental")
            <li><a href="{{route('grafico.mini-mental')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Mini-mental Examen</a>
            </li>
        @endif

        @if($valoracion->escala == "test-mental")
            <li><a href="{{route('grafico.test-mental')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Test Mental</a>
            </li>
        @endif

        @if($valoracion->escala == "mental-abreviado")
            <li><a href="{{route('grafico.mental-abreviado')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Test Mental Abreviado</a>
            </li>
        @endif

        @if($valoracion->escala == "valoracion")
            <li><a href="{{route('grafico.valoracion')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Valoración clínica de la demencia </a>
            </li>
        @endif

        @if($valoracion->escala == "hachinski")
            <li><a href="{{route('grafico.hachinski')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escala de Hachinski</a>
            </li>
        @endif

        @if($valoracion->escala == "hoehn-yahr")
            <li><a href="{{route('grafico.hoehn-yahr')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escala de Hoehn-Yahr</a>
            </li>
        @endif

        @if($valoracion->escala == "schwab")
            <li><a href="{{route('grafico.schwab')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escala de Schwab</a>
            </li>
        @endif

        @if($valoracion->escala == "evaluacion-parkinson")
            <li><a href="{{route('grafico.evaluacion-parkinson')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Escala de Evaluacion-Parkinson</a>
            </li>
        @endif

        @if($valoracion->escala == "updrs")

            <li><a href="{{route('grafico.updrs')}}">
            <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
            Unified Parkinson's Disease Rating Scale (UPDRS)</a>
            </li>
        @endif


    @empty
        "No tiene resultados"
    @endforelse

@endsection
@endif

@section('panel-tabla')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    @if(Auth::guard('web_medico')->check())

                        {{Auth::guard('web_medico')->user()->nombre}}

                    @elseif(Auth::guard('web_paciente')->check())

                        {{Auth::guard('web_paciente')->user()->nombre}}
                    @endif
                </div>


                <div class="panel-body">

                </div>
            </div>
        </div>
    </div><!--/.row-->
@endsection

@section('script')

    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
                }
            });
        });
    </script>

@endsection