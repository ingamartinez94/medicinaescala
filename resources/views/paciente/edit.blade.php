@extends('layouts.app')

@section('opciones')

    @forelse($valoraciones as $valoracion)

        @if($valoracion->escala == "cincinnati")
            <li><a href="{{route('grafico.cincinnati')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Cincinnati</a>
            </li>
        @endif

        @if($valoracion->escala == "barthel")
            <li><a href="{{route('grafico.barthel')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Índice de Barthel</a>
            </li>
        @endif

        @if($valoracion->escala == "canadiense")
            <li><a href="{{route('grafico.canadiense')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Neurológica Canadiense</a>
            </li>
        @endif

        @if($valoracion->escala == "escandinava")
            <li><a href="{{route('grafico.escandinava')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escandinava para el ictus</a>
            </li>
        @endif

        @if($valoracion->escala == "nihss")
            <li><a href="{{route('grafico.nihss')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Instituto Nacional de Salud (NIHSS)</a>
            </li>
        @endif

        @if($valoracion->escala == "modificado")
            <li><a href="{{route('grafico.rankin-modificado')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Rankin modificada</a>
            </li>
        @endif

        @if($valoracion->escala == "hunt-hess")
            <li><a href="{{route('grafico.hunt-hess')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Hunt y Hess para la hemorragia subaracnoidea</a>
            </li>
        @endif

        @if($valoracion->escala == "mini-mental")
            <li><a href="{{route('grafico.mini-mental')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Mini-mental Examen</a>
            </li>
        @endif

        @if($valoracion->escala == "test-mental")
            <li><a href="{{route('grafico.test-mental')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental</a>
            </li>
        @endif

        @if($valoracion->escala == "mental-abreviado")
            <li><a href="{{route('grafico.mental-abreviado')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental Abreviado</a>
            </li>
        @endif

        @if($valoracion->escala == "valoracion")
            <li><a href="{{route('grafico.valoracion')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Valoración clínica de la demencia </a>
            </li>
        @endif

        @if($valoracion->escala == "hachinski")
            <li><a href="{{route('grafico.hachinski')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hachinski</a>
            </li>
        @endif

        @if($valoracion->escala == "hoehn-yahr")
            <li><a href="{{route('grafico.hoehn-yahr')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hoehn-Yahr</a>
            </li>
        @endif

        @if($valoracion->escala == "schwab")
            <li><a href="{{route('grafico.schwab')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Schwab</a>
            </li>
        @endif

        @if($valoracion->escala == "evaluacion-parkinson")
            <li><a href="{{route('grafico.evaluacion-parkinson')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Evaluacion-Parkinson</a>
            </li>
        @endif

        @if($valoracion->escala == "updrs")

            <li><a href="{{route('grafico.updrs')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Unified Parkinson's Disease Rating Scale (UPDRS)</a>
            </li>
        @endif


    @empty
        "No tiene resultados"
    @endforelse

@endsection


@section('panel-tabla')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    {{--Inicio Form--}}
                    {!! Form::model($objPaciente,array('route'=>array('paciente.update',$objPaciente->id),'method'=>'PUT','id'=>'editPaciente')) !!}

                    <div class="row">

                        <div class="col-lg-6 form-group">
                            {!! Form::label('nombre','Nombre') !!}
                            {!! Form::text('nombre',null,['disabled','class'=>'form-control','placeholder'=>'Nombre del medico','id'=>'modalEditarMedico-nombre']) !!}
                        </div>

                        <div class="col-lg-6 form-group">
                            {!! Form::label('apellido','Apellido') !!}
                            {!! Form::text('apellido',null,['disabled','class'=>'form-control','placeholder'=>'Apellido del medico','id'=>'modalEditarMedico-apellido']) !!}
                        </div>

                        <div class="col-lg-6 form-group">
                            {!! Form::label('epecialidad','Cedula') !!}
                            {!! Form::text('cedula',null,['disabled','class'=>'form-control','placeholder'=>'Especialidad del medico','id'=>'modalEditarMedico-especialidad']) !!}
                        </div>

                        <div class="col-lg-6 form-group">
                            {!! Form::label('epecialidad','Direccion') !!}
                            {!! Form::text('direccion',null,['class'=>'form-control','placeholder'=>'Especialidad del medico','id'=>'modalEditarMedico-especialidad']) !!}
                        </div>

                        <div class="col-lg-6 form-group">
                            {!! Form::label('correo','Correo') !!}
                            {!! Form::text('correo',null,['class'=>'form-control','placeholder'=>'Correo del medico','id'=>'modalEditarMedico-correo']) !!}
                        </div>

                        <div class="col-lg-6 form-group">
                            {!! Form::label('contraseña','Contraseña') !!}
                            {!! Form::text('contra',null,['class'=>'form-control','placeholder'=>'Nueva contraseña','id'=>'modalEditarMedico-correo']) !!}
                        </div>


                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Guardar Cambios</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>

@endsection

@push('script')
            <script>
                $.validator.setDefaults({
                    errorElement: "span",
                    errorClass: "help-block",
                    //	validClass: 'stay',
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass(errorClass); //.removeClass(errorClass);
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass(errorClass); //.addClass(validClass);
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else if (element.hasClass('select2')) {
                            error.insertAfter(element.next('span'));
                        } else if (element.prop('type').toLowerCase()=="radio") {
                            error.insertAfter(element.parent().parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                $('#editPaciente').validate({
                    rules: {
                        email: {
                            required: true
                        },
                        contra: {
                            required: true
                        },
                        nombre: {
                            required: true
                        },
                        apellido: {
                            required: true
                        },
                        cedula: {
                            required: true
                        },
                        direccion: {
                            required: true
                        }
                    }
                });

</script>
@endpush