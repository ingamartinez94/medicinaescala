@extends('layouts.app')
@include('admin.modals.agregarmedico')
@include('admin.modals.editar-medico')

@section('opciones')


    <li><a  data-toggle="modal" data-target="#modalAgregarMedico">
            <svg class="glyph stroked pencil">
                <use xlink:href="#stroked-pencil"></use>
            </svg>
            Agregar Medico</a></li>
@endsection

@section('panel-tabla')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">


                    {{Auth::guard('web_admin')->user()->email}}
                </div>
                <div class="panel-body">
                    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>

                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Especialidad</th>
                            <th>Correo</th>

                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($listadoMedicos as $medico)
                        <tr data-id="{{$medico->id}}">

                            <td>{{$medico->nombre}}</td>
                            <td>{{$medico->apellido}}</td>
                            <td>{{$medico->especialidad}}</td>
                            <td>{{$medico->email}}</td>
                            <td>
                                <a class= "btn btn-primary editar" href="#">Editar</a>
                            </td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!--/.row-->
@endsection

@push('script')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
        $('.editar').on('click', function (e) {
            e.preventDefault();
            var fila = $(this).parents('tr');
            var id = fila.data('id');
            $.ajax({
                type: 'GET',
                url: 'medico/'+id,
                success: function(data){
                    console.log(data);
                    $("#modalEditarMedico-nombre").val(data['nombre']);
                    $("#modalEditarMedico-apellido").val(data['apellido']);
                    $("#modalEditarMedico-especialidad").val(data['especialidad']);
                    $("#modalEditarMedico-correo").val(data['email']);
                    $("#modalEditarMedico-id-medico").val(data['id']);

                    $("#modalEditarMedico").modal('toggle');
                }
            });
        });
        $('#formModalEditarMedico').on('submit', function (e) {
            e.preventDefault();
            var id=$("#modalEditarMedico-id-medico").val();

            $.ajax({
                type: 'PUT',
                url: 'medico/'+id,
                data: $('#formModalEditarMedico').serialize(),
                success: function(){
                    location.reload();
                }
            });
        });
    </script>

@endpush