<div class="modal fade" id="modalAgregarMedico" tabindex="-1" role="dialog" aria-labelledby="modalAgregarPaciente">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Paciente</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['route'=>'admin.store','method'=> 'POST','autocomplete'=> 'off','id'=>'formModalAgregarPaciente']) !!}

                <div class="row">

                    <div class="col-lg-6 form-group">
                        {!! Form::label('nombre','Nombre') !!}
                        {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre del medico']) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::label('apellido','Apellido') !!}
                        {!! Form::text('apellido',null,['class'=>'form-control','placeholder'=>'Apellido del medico']) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::label('epecialidad','Especialidad') !!}
                        {!! Form::text('especialidad',null,['class'=>'form-control','placeholder'=>'Especialidad del medico']) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::label('correo','Correo') !!}
                        {!! Form::text('correo',null,['class'=>'form-control','placeholder'=>'Correo del medico']) !!}
                    </div>

                    <div class="col-lg-6 form-group">
                        {!! Form::label('contraseña','Contraseña') !!}
                        {!! Form::text('password',null,['class'=>'form-control','placeholder'=>'Contraseña para el medico']) !!}
                    </div>

                </div>

            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Agregar Medico</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalAgregarPaciente').validate({
        rules: {
            nombre: {
                required: true,
                maxlength: 60
            },
            apellido: {
                required: true,
                maxlength: 60
            },
            especialidad: {
                required: true,
                maxlength: 60
            },
            correo: {
                required: true,
                email:true,
                maxlength: 60
            },
            password: {
                required: true,
                maxlength: 60
            },

        }
    });

</script>
@endpush