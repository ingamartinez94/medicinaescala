@extends('layouts.app')

@if(Auth::guard('web_medico')->check())

@section('opciones')
    @forelse($tipos as $valoracion)

        @if($valoracion->escala == "cincinnati")
            <li><a href="{{route('grafico.cincinnati',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Cincinnati</a>
            </li>
        @endif

        @if($valoracion->escala == "barthel")
            <li><a href="{{route('grafico.barthel',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Índice de Barthel</a>
            </li>
        @endif

        @if($valoracion->escala == "canadiense")
            <li><a href="{{route('grafico.canadiense',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Neurológica Canadiense</a>
            </li>
        @endif

        @if($valoracion->escala == "escandinava")
            <li><a href="{{route('grafico.escandinava',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escandinava para el ictus</a>
            </li>
        @endif

        @if($valoracion->escala == "nihss")
            <li><a href="{{route('grafico.nihss',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Instituto Nacional de Salud (NIHSS)</a>
            </li>
        @endif

        @if($valoracion->escala == "modificado")
            <li><a href="{{route('grafico.rankin-modificado',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Rankin modificada</a>
            </li>
        @endif

        @if($valoracion->escala == "hunt-hess")
            <li><a href="{{route('grafico.hunt-hess',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Hunt y Hess para la hemorragia subaracnoidea</a>
            </li>
        @endif

        @if($valoracion->escala == "mini-mental")
            <li><a href="{{route('grafico.mini-mental',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Mini-mental Examen</a>
            </li>
        @endif

        @if($valoracion->escala == "test-mental")
            <li><a href="{{route('grafico.test-mental',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental</a>
            </li>
        @endif

        @if($valoracion->escala == "mental-abreviado")
            <li><a href="{{route('grafico.mental-abreviado',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental Abreviado</a>
            </li>
        @endif

        @if($valoracion->escala == "valoracion")
            <li><a href="{{route('grafico.valoracion',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Valoración clínica de la demencia </a>
            </li>
        @endif

        @if($valoracion->escala == "hachinski")
            <li><a href="{{route('grafico.hachinski',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hachinski</a>
            </li>
        @endif

        @if($valoracion->escala == "hoehn-yahr")
            <li><a href="{{route('grafico.hoehn-yahr',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hoehn-Yahr</a>
            </li>
        @endif

        @if($valoracion->escala == "schwab")
            <li><a href="{{route('grafico.schwab',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Schwab</a>
            </li>
        @endif

        @if($valoracion->escala == "evaluacion-parkinson")
            <li><a href="{{route('grafico.evaluacion-parkinson',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Evaluacion-Parkinson</a>
            </li>
        @endif

        @if($valoracion->escala == "updrs")

            <li><a href="{{route('grafico.updrs',$id)}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Unified Parkinson's Disease Rating Scale (UPDRS)</a>
            </li>
        @endif


    @empty
        "No tiene resultados"
    @endforelse
@endsection

@elseif(Auth::guard('web_paciente')->check())

@section('opciones')


    @forelse($tipos as $valoracion)

        @if($valoracion->escala == "cincinnati")
            <li><a href="{{route('grafico.cincinnati')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Cincinnati</a>
            </li>
        @endif

        @if($valoracion->escala == "barthel")
            <li><a href="{{route('grafico.barthel')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Índice de Barthel</a>
            </li>
        @endif

        @if($valoracion->escala == "canadiense")
            <li><a href="{{route('grafico.canadiense')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Neurológica Canadiense</a>
            </li>
        @endif

        @if($valoracion->escala == "escandinava")
            <li><a href="{{route('grafico.escandinava')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escandinava para el ictus</a>
            </li>
        @endif

        @if($valoracion->escala == "nihss")
            <li><a href="{{route('grafico.nihss')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Instituto Nacional de Salud (NIHSS)</a>
            </li>
        @endif

        @if($valoracion->escala == "modificado")
            <li><a href="{{route('grafico.rankin-modificado')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Rankin modificada</a>
            </li>
        @endif

        @if($valoracion->escala == "hunt-hess")
            <li><a href="{{route('grafico.hunt-hess')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Hunt y Hess para la hemorragia subaracnoidea</a>
            </li>
        @endif

        @if($valoracion->escala == "mini-mental")
            <li><a href="{{route('grafico.mini-mental')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Mini-mental Examen</a>
            </li>
        @endif

        @if($valoracion->escala == "test-mental")
            <li><a href="{{route('grafico.test-mental')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental</a>
            </li>
        @endif

        @if($valoracion->escala == "mental-abreviado")
            <li><a href="{{route('grafico.mental-abreviado')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Test Mental Abreviado</a>
            </li>
        @endif

        @if($valoracion->escala == "valoracion")
            <li><a href="{{route('grafico.valoracion')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Valoración clínica de la demencia </a>
            </li>
        @endif

        @if($valoracion->escala == "hachinski")
            <li><a href="{{route('grafico.hachinski')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hachinski</a>
            </li>
        @endif

        @if($valoracion->escala == "hoehn-yahr")
            <li><a href="{{route('grafico.hoehn-yahr')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Hoehn-Yahr</a>
            </li>
        @endif

        @if($valoracion->escala == "schwab")
            <li><a href="{{route('grafico.schwab')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Schwab</a>
            </li>
        @endif

        @if($valoracion->escala == "evaluacion-parkinson")
            <li><a href="{{route('grafico.evaluacion-parkinson')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Escala de Evaluacion-Parkinson</a>
            </li>
        @endif

        @if($valoracion->escala == "updrs")

            <li><a href="{{route('grafico.updrs')}}">
                    <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    Unified Parkinson's Disease Rating Scale (UPDRS)</a>
            </li>
        @endif


    @empty
        "No tiene resultados"
    @endforelse

@endsection
@endif


@section('panel-tabla')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(Auth::guard('web_medico')->check())

                    {{Auth::guard('web_medico')->user()->nombre}}

                    @elseif(Auth::guard('web_paciente')->check())

                        {{Auth::guard('web_paciente')->user()->nombre}}
                    @endif

                </div>
                <div class="panel-body">
                    <canvas id="myChart" width="600" height="200"></canvas>
                </div>

            </div>
        </div>
    </div><!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                @if($escala == "hunt")
                    <div class="panel-body">
                    @include('graficos.include.hunt-hess')
                    </div>
                @elseif($escala !="hunt")

                    <div class="panel-body">
                        @if($escala == "mental-abreviado")
                            @include('graficos.include.mental-abreviado')
                        @elseif($escala == "barthel")
                            @include('graficos.include.barthel')
                        @elseif($escala == "cincinnati")
                            @include('graficos.include.cincinnati')
                        @elseif($escala == "escandinava-ictus")
                            @include('graficos.include.escandinava-ictus')
                        @elseif($escala == "mini-mental")
                            @include('graficos.include.mini-mental')
                        @elseif($escala == "test-mental")
                            @include('graficos.include.test-mental')
                        @elseif($escala == "hoehn")
                            @include('graficos.include.hoehn-yahr')
                        @elseif($escala == "rankin")
                            @include('graficos.include.rankin')
                        @elseif($escala == "schwab")
                            @include('graficos.include.schwab')
                        @elseif($escala == "hachinski")
                            @include('graficos.include.hachinski')
                        @elseif($escala == "nihss")
                            @include('graficos.include.nihss')
                        @elseif($escala == "valoracion")
                            @include('graficos.include.valoracionClinica')
                        @elseif($escala == "canadiense")
                            @include('graficos.include.canadiense')
                        @elseif($escala == "evParkinson")
                        @include('graficos.include.evaluacionParkinson')
                        @elseif($escala == "updrs")
                            @include('graficos.include.updrs')
                        @endif
                    </div>

                @endif
            </div>
        </div>
    </div><!--/.row-->
@endsection

@if($escala =="hunt" || $escala =="hoehn" || $escala =="rankin" || $escala == "schwab")
@else
@push('script')
    <script src={{ URL::asset('js/Chart.js') }}></script>
    <script>
        Chart.defaults.global.responsive = true;
        var ctx = document.getElementById("myChart");
        var data = {
            labels: {!! json_encode($mes_formateado) !!},
            datasets: [
                {
                    label: "Escala: {{$escala}}",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    borderColor: "rgba(255,99,132,1)",
                    borderWidth: 1,
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    hoverBorderColor: "rgba(255,99,132,1)",
                    data: {!! json_encode($porcentajeFinal_Formateado) !!},
                }

            ]
        };
        var myLineChart = Chart.Bar(ctx, {
            data: data
        });
    </script>

@endpush
@endif