
<div class="table-responsive">
    <table class="table table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Puntaje</th>
            <th>Interpretación</th>
        </tr>
        </thead>
        <tbody>
        <tr class="success">
            <td> 0</td>
            <td> Ausencia de demencia.</td>
        </tr>
        <tr class="success">
            <td> 0.5</td>
            <td> La demencia es cuestionable o muy leve. </td>
        </tr>
        <tr class="success">
            <td> 1 </td>
            <td> Demencia leve.</td>
        </tr>
        <tr class="success">
            <td> 2</td>
            <td> Demencia moderada.</td>
        </tr>
        <tr class="success">
            <td> 3</td>
            <td> Demencia severa.</td>
        </tr>
        <tr class="success">
            <td colspan="2"> La categoría de 0.5 incluye pacientes con
                algún deterioro cognitivo, pero no lo suficientemente grave
                como para interferir con sus funciones diarias, y los que sufren
                otra enfermedad como depresión, que puede ser la causa de su deterioro.</td>

        </tr>
        </tbody>
    </table>
</div>
<br>

<div class="table-responsive">
<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
    <thead>
    <tr>

        <th>Item</th>

    </tr>
    </thead>
    <tbody>
    @forelse($dataEscalas as $mentales)
        <tr>
            <td>{{$mentales->item.": ".$mentales->respuesta }}</td>
        </tr>
        @empty
            <div class="alert alert-dismissable alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <h4>Mensaje del sistema!</h4>
                <p>No se encuentran registros para este periodo.</p>
            </div>
            <td colspan="4">No hay registros para mostrar</td>
    @endforelse
    </tbody>
</table>
</div>