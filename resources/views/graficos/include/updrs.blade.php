<div class="table-responsive">
<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
    <thead>
    <tr>

        <th>Item 1</th>
        <th>Item 2</th>
        <th>Item 3</th>
        <th>Item 4</th>
        <th>Item 5</th>
        <th>Item 6</th>
        <th>Item 7</th>
        <th>Item 8</th>
        <th>Item 9</th>
        <th>Item 10</th>
        <th>Item 11</th>
        <th>Item 12</th>
        <th>Item 13</th>
        <th>Item 14</th>
        <th>Item 15</th>
        <th>Item 16</th>
        <th>Item 17</th>
        <th>Item 18</th>
        <th>Item 19</th>
        <th>Item 20</th>
        <th>Item 21</th>
        <th>Item 22</th>
        <th>Item 23</th>
        <th>Item 24</th>
        <th>Item 25</th>
        <th>Item 26</th>
        <th>Item 27</th>
        <th>Item 28</th>
        <th>Item 29</th>
        <th>Item 30</th>
        <th>Item 31</th>
        <th>Item 32</th>
        <th>Item 33</th>
        <th>Item 34</th>
        <th>Item 35</th>
        <th>Item 36</th>
        <th>Item 37</th>
        <th>Item 38</th>
        <th>Item 39</th>
        <th>Item 40</th>
        <th>Item 41</th>
        <th>Item 42</th>
        <th>Item 43</th>
        <th>Item 44</th>
        <th>Item 45</th>
        <th>Item 46</th>
        <th>Item 47</th>
        <th>Item 48</th>
        <th>Item 49</th>
        <th>Item 50</th>
        <th>Item 51</th>
        <th>Item 52</th>
        <th>Item 53</th>
        <th>Item 54</th>
        <th>Item 55</th>
        <th>Item 56</th>




    </tr>
    </thead>
    <tbody>
    @forelse($dataEscalas as $mentales)
        <tr>
            <td>{{$mentales->item1.": ".$mentales->respuesta_item1}}</td>
            <td>{{$mentales->item2.": ".$mentales->respuesta_item2}}</td>
            <td>{{$mentales->item3.": ".$mentales->respuesta_item3}}</td>
            <td>{{$mentales->item4.": ".$mentales->respuesta_item4}}</td>
            <td>{{$mentales->item5.": ".$mentales->respuesta_item5}}</td>
            <td>{{$mentales->item6.": ".$mentales->respuesta_item6}}</td>
            <td>{{$mentales->item7.": ".$mentales->respuesta_item7}}</td>
            <td>{{$mentales->item8.": ".$mentales->respuesta_item8}}</td>
            <td>{{$mentales->item9.": ".$mentales->respuesta_item9}}</td>
            <td>{{$mentales->item10.": ".$mentales->respuesta_item10}}</td>
            <td>{{$mentales->item11.": ".$mentales->respuesta_item11}}</td>
            <td>{{$mentales->item12.": ".$mentales->respuesta_item21}}</td>
            <td>{{$mentales->item13.": ".$mentales->respuesta_item13}}</td>
            <td>{{$mentales->item14.": ".$mentales->respuesta_item14}}</td>
            <td>{{$mentales->item15.": ".$mentales->respuesta_item15}}</td>
            <td>{{$mentales->item16.": ".$mentales->respuesta_item16}}</td>
            <td>{{$mentales->item17.": ".$mentales->respuesta_item17}}</td>
            <td>{{$mentales->item18.": ".$mentales->respuesta_item18}}</td>
            <td>{{$mentales->item19.": ".$mentales->respuesta_item19}}</td>
            <td>{{$mentales->item20.": ".$mentales->respuesta_item20}}</td>
            <td>{{$mentales->item21.": ".$mentales->respuesta_item21}}</td>
            <td>{{$mentales->item22.": ".$mentales->respuesta_item22}}</td>
            <td>{{$mentales->item23.": ".$mentales->respuesta_item23}}</td>
            <td>{{$mentales->item24.": ".$mentales->respuesta_item24}}</td>
            <td>{{$mentales->item25.": ".$mentales->respuesta_item25}}</td>
            <td>{{$mentales->item26.": ".$mentales->respuesta_item26}}</td>
            <td>{{$mentales->item27.": ".$mentales->respuesta_item27}}</td>
            <td>{{$mentales->item28.": ".$mentales->respuesta_item28}}</td>
            <td>{{$mentales->item29.": ".$mentales->respuesta_item29}}</td>
            <td>{{$mentales->item30.": ".$mentales->respuesta_item30}}</td>
            <td>{{$mentales->item31.": ".$mentales->respuesta_item31}}</td>
            <td>{{$mentales->item32.": ".$mentales->respuesta_item32}}</td>
            <td>{{$mentales->item33.": ".$mentales->respuesta_item33}}</td>
            <td>{{$mentales->item34.": ".$mentales->respuesta_item34}}</td>
            <td>{{$mentales->item35.": ".$mentales->respuesta_item35}}</td>
            <td>{{$mentales->item36.": ".$mentales->respuesta_item36}}</td>
            <td>{{$mentales->item37.": ".$mentales->respuesta_item37}}</td>
            <td>{{$mentales->item38.": ".$mentales->respuesta_item38}}</td>
            <td>{{$mentales->item39.": ".$mentales->respuesta_item39}}</td>
            <td>{{$mentales->item40.": ".$mentales->respuesta_item40}}</td>
            <td>{{$mentales->item41.": ".$mentales->respuesta_item41}}</td>
            <td>{{$mentales->item42.": ".$mentales->respuesta_item42}}</td>
            <td>{{$mentales->item43.": ".$mentales->respuesta_item43}}</td>
            <td>{{$mentales->item44.": ".$mentales->respuesta_item44}}</td>
            <td>{{$mentales->item45.": ".$mentales->respuesta_item45}}</td>
            <td>{{$mentales->item46.": ".$mentales->respuesta_item46}}</td>
            <td>{{$mentales->item47.": ".$mentales->respuesta_item47}}</td>
            <td>{{$mentales->item48.": ".$mentales->respuesta_item48}}</td>
            <td>{{$mentales->item49.": ".$mentales->respuesta_item49}}</td>
            <td>{{$mentales->item50.": ".$mentales->respuesta_item50}}</td>
            <td>{{$mentales->item51.": ".$mentales->respuesta_item51}}</td>
            <td>{{$mentales->item52.": ".$mentales->respuesta_item52}}</td>
            <td>{{$mentales->item53.": ".$mentales->respuesta_item53}}</td>
            <td>{{$mentales->item54.": ".$mentales->respuesta_item54}}</td>
            <td>{{$mentales->item55.": ".$mentales->respuesta_item55}}</td>
            <td>{{$mentales->item56.": ".$mentales->respuesta_item56}}</td>
        </tr>
    @empty
        <div class="alert alert-dismissable alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Mensaje del sistema!</h4>
            <p>Usted no se ha realizado este test.</p>
        </div>
        <td colspan="56">No hay registros para mostrar</td>
    @endforelse
    </tbody>
</table>
    </div>