
<div class="table-responsive">
    <table class="table table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Puntaje</th>
            <th>Interpretación</th>
        </tr>
        </thead>
        <tbody>
        <tr class="success">
            <td> < 20</td>
            <td> Total</td>
        </tr>
        <tr class="success">
            <td> 20 – 35</td>
            <td> Grave</td>
        </tr>
        <tr class="success">
            <td> 40 – 55</td>
            <td> Moderada</td>
        </tr>
        <tr class="success">
            <td> > 60</td>
            <td> Leve</td>
        </tr>
        <tr class="success">
            <td> 100</td>
            <td> Independiente</td>
        </tr>
        </tbody>
    </table>
</div>
<br>



<div class="table-responsive">
<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
    <thead>
    <tr>

        <th>Item 1</th>
        <th>Item 2</th>
        <th>Item 3</th>
        <th>Item 4</th>
        <th>Item 5</th>
        <th>Item 6</th>
        <th>Item 7</th>
        <th>Item 8</th>
        <th>Item 9</th>
        <th>Item 10</th>

    </tr>
    </thead>
    <tbody>
    @forelse($dataEscalas as $mentales)
        <tr>
            <td>{{$mentales->item1.": ".$mentales->respuesta1}}</td>
            <td>{{$mentales->item2.": ".$mentales->respuesta2}}</td>
            <td>{{$mentales->item3.": ".$mentales->respuesta3}}</td>
            <td>{{$mentales->item4.": ".$mentales->respuesta4}}</td>
            <td>{{$mentales->item5.": ".$mentales->respuesta5}}</td>
            <td>{{$mentales->item6.": ".$mentales->respuesta6}}</td>
            <td>{{$mentales->item7.": ".$mentales->respuesta7}}</td>
            <td>{{$mentales->item8.": ".$mentales->respuesta8}}</td>
            <td>{{$mentales->item9.": ".$mentales->respuesta9}}</td>
            <td>{{$mentales->item10.": ".$mentales->respuesta10}}</td>
        </tr>
    @empty
        <div class="alert alert-dismissable alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Mensaje del sistema!</h4>
            <p>No se encuentran registros para este periodo.</p>
        </div>
        <td colspan="4">No hay registros para mostrar</td>
    @endforelse
    </tbody>
</table>
</div>

