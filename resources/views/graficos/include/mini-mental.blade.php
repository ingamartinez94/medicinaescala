
<div class="table-responsive">
    <table class="table table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Puntaje</th>
            <th>Interpretación</th>
        </tr>
        </thead>
        <tbody>
        <tr class="success">
            <td> 30 - 27</td>
            <td> Normal.</td>
        </tr>
        <tr class="success">
            <td> 26 – 24</td>
            <td> Sospecha patológica.</td>
        </tr>
        <tr class="success">
            <td> 23 – 12</td>
            <td> Deterioro.</td>
        </tr>
        <tr class="success">
            <td>11 – 9</td>
            <td> Demencia.</td>
        </tr>
        </tbody>
    </table>
</div>
<br>


<div class="table-responsive">
<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
    <thead>
    <tr>

        <th>Item 1</th>
        <th>Item 2</th>
        <th>Item 3</th>
        <th>Item 4</th>
        <th>Item 5</th>

    </tr>
    </thead>
    <tbody>
    @forelse($dataEscalas as $mentales)
        <tr>
            <td>{{$mentales->item1.": ".$mentales->respuesta1}}</td>
            <td>{{$mentales->item2.": ".$mentales->respuesta2}}</td>
            <td>{{$mentales->item3.": ".$mentales->respuesta3}}</td>
            <td>{{$mentales->item4.": ".$mentales->respuesta4}}</td>
            <td>{{$mentales->item5.": ".$mentales->respuesta5}}</td>

        </tr>
    @empty
        <div class="alert alert-dismissable alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Mensaje del sistema!</h4>
            <p>No se encuentran registros para este periodo.</p>
        </div>
        <td colspan="4">No hay registros para mostrar</td>
    @endforelse
    </tbody>
</table>
    </div>