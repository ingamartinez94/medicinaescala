
<div class="table-responsive">
    <table class="table table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Interpretación</th>
        </tr>
        </thead>
        <tbody>
        <tr class="success">
            <td>La escala permite el seguimiento, tipificación de la gravedad y respuesta
                a los tratamientos de los síntomas no motores de la enfermedad de Parkinson.</td>

        </tr>
        </tbody>
    </table>
</div>
<br>

<div class="table-responsive">
    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
        <thead>
        <tr>

            <th>Item 1</th>
            <th>Item 2</th>
            <th>Item 3</th>
            <th>Item 4</th>
            <th>Item 5</th>
            <th>Item 6</th>
            <th>Item 7</th>
            <th>Item 8</th>
            <th>Item 9</th>
            <th>Item 10</th>
            <th>Item 11</th>
            <th>Item 12</th>
            <th>Item 13</th>
            <th>Item 14</th>
            <th>Item 15</th>
            <th>Item 16</th>
            <th>Item 17</th>
            <th>Item 18</th>
            <th>Item 19</th>
            <th>Item 20</th>
            <th>Item 21</th>
            <th>Item 22</th>
            <th>Item 23</th>
            <th>Item 24</th>
            <th>Item 25</th>
            <th>Item 26</th>
            <th>Item 27</th>
            <th>Item 28</th>
            <th>Item 29</th>
            <th>Item 30</th>

        </tr>
        </thead>
        <tbody>
        @forelse($dataEscalas as $mentales)
            <tr>
                <td>{{$mentales->item1.": G ".$mentales->gravedad1." F ".$mentales->frecuencia1}}</td>
                <td>{{$mentales->item2.": G ".$mentales->gravedad2." F ".$mentales->frecuencia2}}</td>
                <td>{{$mentales->item3.": G ".$mentales->gravedad3." F ".$mentales->frecuencia3}}</td>
                <td>{{$mentales->item4.": G ".$mentales->gravedad4." F ".$mentales->frecuencia4}}</td>
                <td>{{$mentales->item5.": G ".$mentales->gravedad5." F ".$mentales->frecuencia5}}</td>
                <td>{{$mentales->item6.": G ".$mentales->gravedad6." F ".$mentales->frecuencia6}}</td>
                <td>{{$mentales->item7.": G ".$mentales->gravedad7." F ".$mentales->frecuencia7}}</td>
                <td>{{$mentales->item8.": G ".$mentales->gravedad8." F ".$mentales->frecuencia8}}</td>
                <td>{{$mentales->item9.": G ".$mentales->gravedad9." F ".$mentales->frecuencia9}}</td>
                <td>{{$mentales->item10.": G ".$mentales->gravedad10." F ".$mentales->frecuencia10}}</td>
                <td>{{$mentales->item11.": G ".$mentales->gravedad11." F ".$mentales->frecuencia11}}</td>
                <td>{{$mentales->item12.": G ".$mentales->gravedad12." F ".$mentales->frecuencia12}}</td>
                <td>{{$mentales->item13.": G ".$mentales->gravedad13." F ".$mentales->frecuencia13}}</td>
                <td>{{$mentales->item14.": G ".$mentales->gravedad14." F ".$mentales->frecuencia14}}</td>
                <td>{{$mentales->item15.": G ".$mentales->gravedad15." F ".$mentales->frecuencia15}}</td>
                <td>{{$mentales->item16.": G ".$mentales->gravedad16." F ".$mentales->frecuencia16}}</td>
                <td>{{$mentales->item17.": G ".$mentales->gravedad17." F ".$mentales->frecuencia17}}</td>
                <td>{{$mentales->item18.": G ".$mentales->gravedad18." F ".$mentales->frecuencia18}}</td>
                <td>{{$mentales->item19.": G ".$mentales->gravedad19." F ".$mentales->frecuencia19}}</td>
                <td>{{$mentales->item20.": G ".$mentales->gravedad20." F ".$mentales->frecuencia20}}</td>
                <td>{{$mentales->item21.": G ".$mentales->gravedad21." F ".$mentales->frecuencia21}}</td>
                <td>{{$mentales->item22.": G ".$mentales->gravedad22." F ".$mentales->frecuencia22}}</td>
                <td>{{$mentales->item23.": G ".$mentales->gravedad23." F ".$mentales->frecuencia23}}</td>
                <td>{{$mentales->item24.": G ".$mentales->gravedad24." F ".$mentales->frecuencia24}}</td>
                <td>{{$mentales->item25.": G ".$mentales->gravedad25." F ".$mentales->frecuencia25}}</td>
                <td>{{$mentales->item26.": G ".$mentales->gravedad26." F ".$mentales->frecuencia26}}</td>
                <td>{{$mentales->item27.": G ".$mentales->gravedad27." F ".$mentales->frecuencia27}}</td>
                <td>{{$mentales->item28.": G ".$mentales->gravedad28." F ".$mentales->frecuencia28}}</td>
                <td>{{$mentales->item29.": G ".$mentales->gravedad29." F ".$mentales->frecuencia29}}</td>
                <td>{{$mentales->item30.": G ".$mentales->gravedad30." F ".$mentales->frecuencia30}}</td>

            </tr>
        @empty
            <div class="alert alert-dismissable alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <h4>Mensaje del sistema!</h4>
                <p>No se encuentran registros para este periodo.</p>
            </div>
            <td colspan="4">No hay registros para mostrar</td>
        @endforelse
        </tbody>
    </table>
</div>