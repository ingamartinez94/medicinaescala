
<div class="table-responsive">
    <table class="table table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Puntaje</th>
            <th>Interpretación</th>
        </tr>
        </thead>
        <tbody>
        <tr class="success">
            <td> 0</td>
            <td> No síntomas de ictus.</td>
        </tr>
        <tr class="success">
            <td> 1 - 4</td>
            <td> Ictus menor.</td>
        </tr>
        <tr class="success">
            <td>5 - 15</td>
            <td> Ictus moderado.</td>
        </tr>
        <tr class="success">
            <td>16 - 20</td>
            <td> Ictus moderado a severo.</td>
        </tr>
        <tr class="success">
            <td>21 – 42</td>
            <td> Ictus severo.</td>
        </tr>
        <tr class="success">
            <td>Terapia con activador del plasminógeno tisular:</td>
            <td> Si el paciente no presenta otra contraindicación, se recomienda con puntajes entre 5 – 25.</td>
        </tr>
        </tbody>
    </table>
</div>
<br>

<div class="table-responsive">
<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
    <thead>
    <tr>

        <th>Item 1</th>
        <th>Item 2</th>
        <th>Item 3</th>
        <th>Item 4</th>
        <th>Item 5</th>
        <th>Item 6</th>
        <th>Item 7</th>
        <th>Item 8</th>
        <th>Item 9</th>
        <th>Item 10</th>
        <th>Item 11</th>
        <th>Item 12</th>
        <th>Item 13</th>
        <th>Item 14</th>
        <th>Item 15</th>



    </tr>
    </thead>
    <tbody>
    @forelse($dataEscalas as $mentales)
        <tr>
            <td>{{$mentales->item1.": ".$mentales->respuesta1}}</td>
            <td>{{$mentales->item2.": ".$mentales->respuesta2}}</td>
            <td>{{$mentales->item3.": ".$mentales->respuesta3}}</td>
            <td>{{$mentales->item4.": ".$mentales->respuesta4}}</td>
            <td>{{$mentales->item5.": ".$mentales->respuesta5}}</td>
            <td>{{$mentales->item6.": ".$mentales->respuesta6}}</td>
            <td>{{$mentales->item7.": ".$mentales->respuesta7}}</td>
            <td>{{$mentales->item8.": ".$mentales->respuesta8}}</td>
            <td>{{$mentales->item9.": ".$mentales->respuesta9}}</td>
            <td>{{$mentales->item10.": ".$mentales->respuesta10}}</td>
            <td>{{$mentales->item11.": ".$mentales->respuesta11}}</td>
            <td>{{$mentales->item12.": ".$mentales->respuesta12}}</td>
            <td>{{$mentales->item13.": ".$mentales->respuesta13}}</td>
            <td>{{$mentales->item14.": ".$mentales->respuesta14}}</td>
            <td>{{$mentales->item15.": ".$mentales->respuesta15}}</td>
        </tr>
    @empty
        <div class="alert alert-dismissable alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Mensaje del sistema!</h4>
            <p>No se encuentran registros para este periodo.</p>
        </div>
        <td colspan="4">No hay registros para mostrar</td>
    @endforelse
    </tbody>
</table>
    </div>