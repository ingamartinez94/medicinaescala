<div class="modal fade" id="modalAgregarPaciente" tabindex="-1" role="dialog" aria-labelledby="modalAgregarPaciente">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Paciente</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['route'=>'paciente.store','method'=> 'POST','autocomplete'=> 'off','id'=>'formModalAgregarPaciente']) !!}

                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::label('codigo','Nombre:') !!}
                        {!! Form::text('nombrePaciente',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('apellido','Apellido') !!}
                        {!! Form::text('apellidoPaciente',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('cedula','Cedula') !!}
                        {!! Form::text('cedula',null,['class'=>'form-control','placeholder'=>'Ingrese la cedula del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('direccion','Direccion') !!}
                        {!! Form::text('direccion',null,['class'=>'form-control','placeholder'=>'Ingrese la direccion del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('correo','Correo') !!}
                        {!! Form::text('emailPaciente',null,['class'=>'form-control','placeholder'=>'Ingrese el correo electronico del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('contraseña','Contraseña') !!}
                        {!! Form::text('passworoPaciente',null,['class'=>'form-control','placeholder'=>'Ingrese la contraseña para el paciente']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Agregar Paciente</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalAgregarPaciente').validate({
        rules: {
            nombrePaciente: {
                required: true,
                maxlength: 60
            },
            apellidoPaciente: {
                required: true,
                maxlength: 60
            },
            cedula: {
                required: true,
                number: true,
                maxlength: 20,
                minlength: 5

            },
            direccion: {
                required: true
            },
            emailPaciente: {
                required: true,
                email:true,
                maxlength: 60
            },
            passworoPaciente: {
                required: true,
                maxlength: 60
            },

        }
    });

</script>
@endpush