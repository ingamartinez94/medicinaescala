<div class="modal fade" id="modalEditarPaciente" tabindex="-1" role="dialog" aria-labelledby="modalEditarPaciente">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Paciente</h4>
            </div>
            <div class="modal-body">
                {{--Inicio Form--}}
                {!! Form::open(['route'=>['paciente.update',':PACIENTE_ID'],'method'=> 'POST','autocomplete'=> 'off','id'=>'formModalEditarPaciente']) !!}
                <input id="modalEditarPaciente-id-paciente" name="id-paciente" type="hidden" value="">
                <div class="row">
                    <div class="col-lg-6 form-group">
                        {!! Form::label('codigo','Nombre:') !!}
                        {!! Form::text('nombre',null,['class'=>'form-control','id'=>'modalEditarPaciente-nombre','placeholder'=>'Ingrese el nombre del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('apellido','Apellido') !!}
                        {!! Form::text('apellido',null,['class'=>'form-control','id'=>'modalEditarPaciente-apellido','placeholder'=>'Ingrese el apellido del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('cedula','Cedula') !!}
                        {!! Form::number('cedula',null,['class'=>'form-control','id'=>'modalEditarPaciente-cedula','placeholder'=>'Ingrese la cedula del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('direccion','Direccion') !!}
                        {!! Form::text('direccion',null,['class'=>'form-control','id'=>'modalEditarPaciente-direccion','placeholder'=>'Ingrese la direccion del paciente']) !!}
                    </div>
                    <div class="col-lg-6 form-group">
                        {!! Form::label('correo','Correo') !!}
                        {!! Form::email('correo',null,['class'=>'form-control','id'=>'modalEditarPaciente-email','placeholder'=>'Ingrese el correo electronico del paciente']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-info">Finalizar Edición Paciente</button>

                {!! Form::close() !!}
                {{--Fin del form--}}

            </div>
        </div>
    </div>
</div>

@push('script')
<script>
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: "help-block",
        //	validClass: 'stay',
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass); //.removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass); //.addClass(validClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('select2')) {
                error.insertAfter(element.next('span'));
            } else if (element.prop('type').toLowerCase()=="radio") {
                error.insertAfter(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $('#formModalAgregarPaciente').validate({
        rules: {
            nombrePaciente: {
                required: true,
                maxlength: 60
            },
            apellidoPaciente: {
                required: true,
                maxlength: 60
            },
            cedula: {
                required: true,
                number: true,
                maxlength: 20,
                minlength: 5

            },
            direccion: {
                required: true
            },
            emailPaciente: {
                required: true,
                email:true,
                maxlength: 60
            }
        }
    });

</script>
@endpush