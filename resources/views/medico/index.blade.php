@extends('layouts.app')



@section('opciones')


    <li><a  href="#" data-toggle="modal" data-target="#modalAgregarPaciente">
            <svg class="glyph stroked pencil">
                <use xlink:href="#stroked-pencil"></use>
            </svg>
            Agregar Paciente</a></li>
@endsection


@section('panel-tabla')
    @include('medico.modals.agregar-usuario')
    @include('medico.modals.editar-usuario')


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">


                </div>
                <div class="panel-body">
                    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Cedula</th>
                            <th>Direccion</th>
                            <th>Correo</th>
                            <th>Opciones</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pacientes as $paciente)
                            <tr data-id="{{$paciente->id}}">

                                <td>{{$paciente->nombre}}</td>
                                <td>{{$paciente->apellido}}</td>
                                <td>{{$paciente->cedula}}</td>
                                <td>{{$paciente->direccion}}</td>
                                <td>{{$paciente->email}}</td>
                                <td>
                                    <a class= "btn btn-primary editar" href="#">Editar</a>
                                    {{--<a class= "btn btn-info" href="{{ URL::to('Historia/'.$paciente->id)}}">Historia</a>--}}
                                    <a class= "btn btn-danger" href="{{ URL::to('paciente/'.$paciente->id.'/listado')}}">Valorar</a>
                                    <a class= "btn btn-warning" href="{{ URL::to('graficos/'.$paciente->id)}}">Graficos</a>

                                </td>


                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!--/.row-->
@endsection

@push('script')

    <script>
        $(document).ready(function () {
            $('#example').DataTable();

        });
        $('.editar').on('click', function (e) {
            e.preventDefault();
            var fila = $(this).parents('tr');
            var id = fila.data('id');
            $.ajax({
                type: 'GET',
                url: 'paciente/'+id,
                success: function(data){
                    console.log(data);
                    $("#modalEditarPaciente-nombre").val(data.nombre);
                    $("#modalEditarPaciente-apellido").val(data['apellido']);
                    $("#modalEditarPaciente-cedula").val(data['cedula']);
                    $("#modalEditarPaciente-direccion").val(data['direccion']);
                    $("#modalEditarPaciente-email").val(data['email']);
                    $("#modalEditarPaciente-id-paciente").val(data['id']);

                    $("#modalEditarPaciente").modal('toggle');
                }
            });
        });
        $('#formModalEditarPaciente').on('submit', function (e) {
            e.preventDefault();
            var id=$("#modalEditarPaciente-id-paciente").val();

            $.ajax({
                type: 'PUT',
                url: 'paciente/'+id,
                data: $('#formModalEditarPaciente').serialize(),
                success: function(){
                    location.reload();
                }
            });
        });
    </script>

@endpush