@extends('layouts.app')

@section('opciones')


    <li><a  href="#" data-toggle="modal" data-target="#modalAgregarPaciente">
            <svg class="glyph stroked pencil">
                <use xlink:href="#stroked-pencil"></use>
            </svg>
            Agregar Paciente</a></li>
@endsection


@section('panel-tabla')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <div class="panel-body">
    {{--Inicio Form--}}
    {!! Form::model($objMedico,array('route'=>array('medico.update',$objMedico->id),'method'=>'PUT','id'=>'editMedico')) !!}

    <div class="row">

        <div class="col-lg-6 form-group">
            {!! Form::label('nombre','Nombre') !!}
            {!! Form::text('nombre',null,['disabled','class'=>'form-control','placeholder'=>'Nombre del medico','id'=>'modalEditarMedico-nombre']) !!}
        </div>

        <div class="col-lg-6 form-group">
            {!! Form::label('apellido','Apellido') !!}
            {!! Form::text('apellido',null,['disabled','class'=>'form-control','placeholder'=>'Apellido del medico','id'=>'modalEditarMedico-apellido']) !!}
        </div>

        <div class="col-lg-6 form-group">
            {!! Form::label('epecialidad','Especialidad') !!}
            {!! Form::text('especialidad',null,['disabled','class'=>'form-control','placeholder'=>'Especialidad del medico','id'=>'modalEditarMedico-especialidad']) !!}
        </div>

        <div class="col-lg-6 form-group">
            {!! Form::label('correo','Correo') !!}
            {!! Form::text('correo',null,['class'=>'form-control','placeholder'=>'Correo del medico','id'=>'modalEditarMedico-correo']) !!}
        </div>

        <div class="col-lg-6 form-group">
            {!! Form::label('contraseña','Contraseña') !!}
            {!! Form::text('contra',null,['class'=>'form-control','placeholder'=>'Nueva contraseña','id'=>'modalEditarMedico-correo']) !!}
        </div>


    </div>
     </div>
            </div>
<div class="modal-footer">

    <button type="submit" class="btn btn-info">Guardar Cambios</button>

    {!! Form::close() !!}
    {{--Fin del form--}}

</div>

@endsection

@push('script')
            <script>
                $.validator.setDefaults({
                    errorElement: "span",
                    errorClass: "help-block",
                    //	validClass: 'stay',
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass(errorClass); //.removeClass(errorClass);
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass(errorClass); //.addClass(validClass);
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else if (element.hasClass('select2')) {
                            error.insertAfter(element.next('span'));
                        } else if (element.prop('type').toLowerCase()=="radio") {
                            error.insertAfter(element.parent().parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                $('#editMedico').validate({
                    rules: {
                        email: {
                            required: true
                        },
                        contra: {
                            required: true
                        },
                        nombre: {
                            required: true
                        },
                        apellido: {
                            required: true
                        },
                        especialidad: {
                            required: true
                        }
                    }
                });

            </script>
@endpush