<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Proyecto - Medicina</title>


    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/bs/dt-1.10.11/datatables.min.css"/>

    <link rel="stylesheet" href={{URL::asset('css/datepicker3.css')}}>
    <link rel="stylesheet" href={{URL::asset('css/styles.css')}}>

    <script type="text/javascript" src="{{ URL::asset('js/lumino.glyphs.js') }}"></script>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ URL::asset('js/html5shiv.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/respond.min.js') }}"></script>
    <![endif]-->
    @yield('css')
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span><img src="{{URL::asset('img/LogoUnisinuWhite.png')}}" style="height: 60px" class="img-responsive" alt="Responsive image"></span></a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <svg class="glyph stroked male-user">
                            <use xlink:href="#stroked-male-user"></use>
                        </svg>
                        <span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu">

                        @if (Auth::guard('web_medico')->check())

                            <li>
                                <a href="{{ route('medico.edit',Auth::guard('web_medico')->user()->id) }}">
                                    <svg class="glyph stroked male-user">
                                        <use xlink:href="#stroked-male-user"></use>
                                    </svg>
                                    Perfil
                                </a>
                            </li>

                        @elseif(Auth::guard('web_paciente')->check())
                            <li>
                                <a href="{{ route('paciente.edit',Auth::guard('web_paciente')->user()->id) }}">
                                    <svg class="glyph stroked male-user">
                                        <use xlink:href="#stroked-male-user"></use>
                                    </svg>
                                    Perfil
                                </a>
                            </li>

                        @endif
                            <li><a href="{{ url('/logout') }}">
                                <svg class="glyph stroked cancel">
                                    <use xlink:href="#stroked-cancel"></use>
                                </svg>
                                Salir</a>
                            </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <form role="search">

    </form>
    <ul class="nav menu">

        @if (Auth::guard('web_medico')->check())
            <li class="active"><a href="{{ URL::route('medico.index') }}">
                    <svg class="glyph stroked dashboard-dial">
                        <use xlink:href="#stroked-dashboard-dial"></use>
                    </svg>
                    Dashboard</a></li>
        @elseif(Auth::guard('web_admin')->check())
            <li class="active"><a href="{{ URL::route('admin.index') }}">
                    <svg class="glyph stroked dashboard-dial">
                        <use xlink:href="#stroked-dashboard-dial"></use>
                    </svg>
                    Dashboard</a></li>
        @elseif(Auth::guard('web_paciente')->check())
            <li class="active"><a href="{{ URL::route('paciente.index') }}">
                    <svg class="glyph stroked dashboard-dial">
                        <use xlink:href="#stroked-dashboard-dial"></use>
                    </svg>
                    Dashboard</a></li>
        @endif


        @yield('opciones')


        <li role="presentation" class="divider"></li>
        <li><a href="{{ url('/logout') }}">
                <svg class="glyph stroked male-user">
                    <use xlink:href="#stroked-male-user"></use>
                </svg>
                Salir</a></li>
    </ul>

</div><!--/.sidebar-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#">
                    <svg class="glyph stroked home">
                        <use xlink:href="#stroked-home"></use>
                    </svg>
                </a></li>
            <li class="active">Icons</li>
        </ol>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div><!--/.row-->
    @yield('includes')

    @yield('panel-tabla')



</div>    <!--/.main-->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/t/bs/dt-1.10.11/datatables.min.js"></script>

<script type="text/javascript" src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/messages_es.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/additional-methods.min.js') }}"></script>

@stack('script')
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });


    !function ($) {
        $(document).on("click", "ul.nav li.parent > a > span.icon", function () {
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })
</script>
</body>

</html>
